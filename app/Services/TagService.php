<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

class TagService
{
    public static function attach(array $tags = [], Taggable $taggable): void
    {
        $tags = collect($tags)->flatten()->filter(fn ($value) => is_int($value))->toArray();
        $taggable->tags()->sync($tags);
    }

    public static function detach():void
    {
    }
}
