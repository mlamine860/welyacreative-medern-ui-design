<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Taggable
{
    public function tags(): BelongsToMany;
}
