<?php

namespace App\Services;

use App\Models\Media;
use Illuminate\Http\File;
use App\Contract\Mediable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File as Filesystem;

class MediaService
{
    public static function attach(array $media, Mediable $mediable): void
    {
        $ids = collect($media)->map(function ($item) use ($mediable): int | null {
            ['path' => $filePath, 'name' => $fileName] = $item;
            if (!file_exists(Storage::path($filePath))) return null;
            $mediaRecord = self::getMediaRecord(self::getFileHash(Storage::path($filePath)), $fileName);
            if ($mediaRecord) {
                $attached = $mediable->media()->where('media_id', $mediaRecord->id)->first();
                if ($attached) return null;
                Storage::delete($filePath);
                return $mediaRecord->id;
            }
            $file = new File(Storage::path($filePath));
            $path = Storage::putFile('media', $file);
            $fileHash = self::getFileHash(Storage::path($path));
            $name = Filesystem::basename($path);
            $createdMedia =  self::create(
                $name,
                $fileName,
                $fileHash,
                $file->getSize(),
                config('media.disk'),
                $path,
                $file->getMimeType()
            );
            Storage::delete($filePath);
            (new ImageService)->generate(Storage::path($path));
            return $createdMedia->id;
        })->filter(fn ($value) => $value !== null)->toArray();
        $mediable->media()->attach($ids);
    }
    public static function create(
        string $name,
        string $fileName,
        string $fileHash,
        int $size,
        string $disk,
        string $path,
        string $mimeType
    ): Media {

        return Media::create([
            'file_name' => $fileName,
            'file_hash' => $fileHash,
            'name' => $name,
            'size' => $size,
            'disk' => $disk,
            'path' => $path,
            'mime_type' => $mimeType
        ]);
    }

    public static function getFileHash(string $filePath): string
    {
        return hash_file(config('media.hash'), $filePath);
    }

    public static function getMediaRecord(string $hash, string $fileName): Media | null
    {
        return Media::where('file_name',  $fileName)
            ->orWhere('file_hash', $hash)
            ->first();
    }
}
