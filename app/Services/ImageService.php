<?php


namespace App\Services;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;

class ImageService
{
    public  const IMAGE_FORMATS = [
        [
            'width' =>  1920,
            'height' => 1080,
            'prefix' => 'large'
        ],
        [
            'width' =>  640,
            'height' => 360,
            'prefix' => 'medium'
        ],
        [
            'width' =>  160,
            'height' => 90,
            'prefix' => 'small'
        ],
    ];

    public static function generate(string $imagePath): void
    {
        if (!file_exists($imagePath)) return;
        foreach (self::IMAGE_FORMATS as $format) {
            $image = Image::make($imagePath);
            $dest = "{$image->dirname}/{$format['prefix']}-{$image->basename}";
            if (!file_exists($dest)) {
                $image->resize($format['width'], $format['height'])
                    ->save($dest);
            }
        }
    }

    public static function getFormatFromPrefix(string $path, string $prefix = ''): string
    {
        $dirname =  File::dirname($path);
        $filename = File::basename($path);
        return "{$dirname}/{$prefix}-{$filename}";
    }
    public  static function formatFromPrefix(string $path, string $prefix = ''): string
    {
        $dirname =  File::dirname($path);
        $filename = File::basename($path);
        return "{$dirname}/{$prefix}-{$filename}";
    }

    public static  function imageHasAlreadyGenerated(string $path): bool
    {
        if (!file_exists($path)) return true;
        foreach (self::IMAGE_FORMATS as $format) {
            if (!file_exists(self::formatFromPrefix($path, $format['prefix']))) return false;
        }
        return true;
    }

    public static function orphans(Collection $collection, string $storagePath = 'tmp', callable $callback): array
    {
        $modelPathCollection = $collection->map($callback);
        $modelPathWithFormat = $modelPathCollection->map(function ($path) {
            $pathWithFormat = [];
            foreach (self::IMAGE_FORMATS as $format) {
                $pathWithFormat[] = self::getFormatFromPrefix($path, $format['prefix']);
            }
            return $pathWithFormat;
        });
        $storagePathCollection = collect(Storage::files($storagePath));
        $modelPathWithFormatCollection = $modelPathCollection->concat($modelPathWithFormat->flatten());
        return $storagePathCollection->diff($modelPathWithFormatCollection)->toArray();
    }

    public static function drop(array $files): void
    {
        collect($files)->each(fn ($file) => Storage::delete($file));
    }

    public static function dropFormat(string $path): void
    {
        foreach (ImageService::IMAGE_FORMATS as $format) {
            Storage::delete(ImageService::getFormatFromPrefix($path, $format['prefix']));
        }
    }
}
