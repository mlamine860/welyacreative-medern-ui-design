<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Utils\Role;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at',
        'last_seen',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_seen' => 'datetime'
    ];

    protected $appends = ['avatar'];


    public function projects(): HasMany
    {
        return $this->hasMany(Project::class, 'author');
    }
    public function teams(): BelongsToMany
    {
        return $this->belongsToMany(Team::class);
    }
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class);
    }

    public function services(): HasMany
    {
        return $this->hasMany(Service::class);
    }

    public function pages(): HasMany
    {
        return $this->hasMany(Page::class);
    }
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    public function isActive(): bool
    {
        return $this->email_verified_at !== null;
    }

    public function isAdmin(): bool
    {
        return $this->role === Role::admin->value;
    }

    public function isEditor(): bool
    {
        return $this->role === Role::editor->value;
    }

    public function isSimpleUser(): bool
    {
        return $this->role === Role::user->value;
    }
    public function isSiteManager()
    {
        return ($this->isAdmin() || $this->isEditor()) === true;
    }


    public function getAvatarAttribute()
    {
        if ($this->profile()->exists()) {
            return Storage::url($this->profile->photo_path);
        }
        return Gravatar::get($this->email);
    }

    public function getActiveAttribute(): bool
    {
        return $this->isActive();
    }


    // public function setPasswordAttribute($password)
    // {
    //     if (!$password) return;

    //     $this->attributes['password'] = Hash::make($password);
    // }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('name', 'like', '%' . $search . '%');
        })->when($filters['role'] ?? null, function ($query, $role) {
            if ($role === 'admin') {
                $query->whereRole('admin');
            } elseif ($role === 'user') {
                $query->whereRole('user');
            } elseif ($role === 'editor') {
                $query->whereRole('editor');
            }
        });
    }
}
