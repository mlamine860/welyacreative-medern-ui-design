<?php

namespace App\Models;

use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Testimony extends Model
{
    use HasFactory;

    protected $fillable = [
        'username', 'email', 'image_url', 'job', 'message',
    ];

    public string $imageStorageDir = 'testimonies/images';


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getAvatarAttribute()
    {
        if (Storage::disk()->exists($this->image_url)) {

            return Storage::url($this->image_url);
        }
        return Gravatar::get('contact@default.com');
    }


    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('username', 'like', '%' . $search . '%');
        });
    }
}
