<?php

namespace App\Models;

use Illuminate\Support\Str;
use App\Services\ImageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'content',
        'image_path', 'category_id',
        'slug', 'published',
        'excerpt'
    ];


    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }


    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('title', 'like', '%' . $search . '%');
        })
            ->when($filters['published'] ?? null, function ($query, $published) {
                if ($published === 'true') {
                    $query->wherePublished(true);
                } elseif ($published === 'false') {
                    $query->wherePublished(false);
                }
            });
    }


    public function setSlugAttribute(string $slug)
    {
        if (!$slug) return;
        $this->attributes['slug'] = Str::slug($slug);
    }

    public function getImageLargeAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/1920/1080?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_path, 'large'));
    }

    public function getImageAttribute(): string
    {

        if (!Storage::disk()->exists($this->image_path)) {
            return $this->image_path;
        }
        return Storage::url($this->image_path);
    }
    
    public function getImageMediumAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/640/360?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_path, 'medium'));
    }

    public function getImageSmallAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/160/90?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_path, 'small'));
    }
    private function imageDoesNotExist(): bool
    {
        return (!$this->image_path || !Storage::exists($this->image_path));
    }
}
