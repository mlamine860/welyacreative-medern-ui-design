<?php

namespace App\Models;

use App\Services\ImageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Media extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'file_name', 'mime_type',
        'path', 'disk', 'file_hash', 'size'
    ];

    public $timestamp = false;

    protected $appends = ['url', 'imageSmall', 'imageMedium', 'imageLarge'];


    public function projects(): MorphToMany
    {
        return $this->morphToMany(Project::class, 'mediable');
    }

    public function getUrlAttribute():string
    {
        return Storage::url($this->path);
    }

    public function getImageMediumAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/640/360?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->path, 'medium'));
    }
    public function getImageSmallAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/160/90?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->path, 'small'));
    }
    public function getImageLargeAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/160/90?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->path, 'large'));
    }


    private function imageDoesNotExist(): bool
    {
        return (!$this->path || !Storage::exists($this->path));
    }
}
