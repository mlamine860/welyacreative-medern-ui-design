<?php

namespace App\Models;

use App\Services\ImageService;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Service extends Model
{
    use HasFactory;


    protected $fillable = [
        'name', 'description', 'content', 'published', 'featured'
    ];

    protected $appends = ['image'];

    protected $casts = [
        'featured' => 'boolean',
        'published' => 'boolean',
    ];

    public string $imageStorageDir = 'services/images';
    public string $iconStorageDir = 'services/icons';

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function getimageAttribute()
    {
        if ($this->imageDoesNotExist()) {
            return  Gravatar::get(fake()->email());
        }
        return Storage::url($this->image_url);
    }
    public function getIconAttribute()
    {
        if (!$this->icon_url || !Storage::exists($this->icon_url)) {
            return  Gravatar::get(fake()->email());
        }
        return Storage::url($this->icon_url);
    }

    public function getImageLargeAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return  Gravatar::get(fake()->email());
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_url, 'large'));
    }
    public function getImageMediumAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return  Gravatar::get(fake()->email());
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_url, 'medium'));
    }

    public function getImageSmallAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return  Gravatar::get(fake()->email());
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_url, 'small'));
    }


    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('name', 'like', '%' . $search . '%');
        })->when($filters['featured'] ?? null, function ($query, $featured) {
            if ($featured === 'true') {
                $query->whereFeatured(true);
            } elseif ($featured === 'false') {
                $query->whereFeatured(false);
            }
        })
            ->when($filters['published'] ?? null, function ($query, $published) {
                if ($published === 'true') {
                    $query->wherePublished(true);
                } elseif ($published === 'false') {
                    $query->wherePublished(false);
                }
            });
    }

    public function setNameAttribute(string $value)
    {
        if (!$value) return;
        $this->attributes['name']  = $value;
        $this->attributes['slug']  = Str::slug($value);
    }

    private function imageDoesNotExist(): bool
    {
        return (!$this->image_url || !Storage::exists($this->image_url));
    }
}
