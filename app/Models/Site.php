<?php

namespace App\Models;

use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Site extends Model
{
    use HasFactory;

    protected $appends = ['logo'];
    public string $storageImageDir = 'sites/images';
    public string $storageLogoDir = 'sites/logo';

    protected $fillable = [
        'name', 'description',
        'email', 'phone',
        'address', 'tagline',
        'url'
    ];

    public function getLogoAttribute(): string
    {
        if (!Storage::exists(($this->logo_url)))
            return Gravatar::get($this->email);
        return Storage::url($this->logo_url);
    }
}
