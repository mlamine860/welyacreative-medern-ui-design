<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Profile extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name', 'last_name',
        'address', 'phone', 'department',
        'role', 'languages', 'organization',
        'education', 'work_history', 'birthday',
        'bio'
    ];

    public string $imageStorageDir = 'users/profile/images';


    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
