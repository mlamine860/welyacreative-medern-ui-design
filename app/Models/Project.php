<?php

namespace App\Models;

use App\Contract\Mediable;
use App\Services\Taggable;
use App\Services\ImageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Creativeorange\Gravatar\Facades\Gravatar;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Project extends Model implements Mediable, Taggable
{
    use HasFactory;

    protected $fillable = [
        'name', 'description', 'image_path', 'content',
    ];

    protected $appends = ['image'];

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author');
    }

    public function media(): MorphToMany
    {
        return $this->morphToMany(Media::class, 'mediable');
    }
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('name', 'like', '%' . $search . '%');
        });
    }

    public function getImageLargeAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/1920/1080?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_path, 'large'));
    }


    public function getImageAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/640/360?blur=10";;
        }
        return Storage::url($this->image_path);
    }

    public function getImageMediumAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/640/360?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_path, 'medium'));
    }
    public function getImageSmallAttribute(): string
    {
        if ($this->imageDoesNotExist()) {
            return "https://picsum.photos/seed/picsum/160/90?blur=10";
        }
        return Storage::url(ImageService::getFormatFromPrefix($this->image_path, 'small'));
    }


    private function imageDoesNotExist(): bool
    {
        return (!$this->image_path || !Storage::exists($this->image_path));
    }
}
