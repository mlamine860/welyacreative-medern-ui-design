<?php

namespace App\Utils;

enum Role: string
{
    case user = 'user';
    case editor = 'editor';
    case admin = 'admin';
}
