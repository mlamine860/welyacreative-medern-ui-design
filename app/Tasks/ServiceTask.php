<?php

namespace App\Tasks;

use App\Models\Service;
use App\Services\ImageService;
use Illuminate\Support\Facades\Storage;

class ServiceTask extends Task
{

    public static function generateImage(): void
    {
        Service::all()->each(function (Service $service) {
            ImageService::generate(Storage::path($service->image_url));
        });
    }

    public static function getUngeneratedImageCount(): int
    {
        $count = Service::all()->map(function (Service $service): bool {
            return !ImageService::imageHasAlreadyGenerated(Storage::path($service->image_url));
        })->filter(fn ($value) => $value !== false)->count();
        return 100 - $count;
    }


    public static function getOrphanImageCount(): int
    {
        return collect(self::getOrphanImage(Service::all(), 'services/images', fn (Service $media) => $media->image_url))->count();
    }

    public static function dropOrphans(): void
    {
        ImageService::drop(self::getOrphanImage(Service::all(), 'services/images', fn (Service $service) => $service->image_url));
    }
}
