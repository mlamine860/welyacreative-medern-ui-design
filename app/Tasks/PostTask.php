<?php

namespace App\Tasks;

use App\Models\Post;
use App\Services\ImageService;
use Illuminate\Support\Facades\Storage;

class PostTask extends Task
{

    public static function generateImage(): void
    {
        Post::all()->each(function (Post $post) {
            ImageService::generate(Storage::path($post->image_path));
        });
    }

    public static function getUngeneratedImageCount(): int
    {
        $count  = Post::all()->map(function (Post $post): bool {
            return !imageService::imageHasAlreadyGenerated(Storage::path($post->image_path));
        })->filter(fn ($value) => $value !== false)->count();
        return 100 - $count;
    }

    public static function getOrphanImageCount(): int
    {
        return collect(self::getOrphanImage(Post::all(), 'posts/images', fn (Post $media) => $media->image_path))->count();
    }

    public static function dropOrphans(): void
    {
        ImageService::drop(self::getOrphanImage(Post::all(), 'posts/images', fn (Post $post) => $post->image_path));
    }
}
