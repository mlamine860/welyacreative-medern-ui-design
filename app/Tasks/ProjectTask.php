<?php

namespace App\Tasks;

use App\Models\Project;
use App\Services\ImageService;
use Illuminate\Support\Facades\Storage;

class ProjectTask extends Task
{

    public static function generateImage(): void
    {
        Project::all()->each(function (Project $project) {
            ImageService::generate(Storage::path($project->image_path));
        });
    }

    public static function getUngeneratedImageCount(): int
    {
        $count = Project::all()->map(function (Project $project): bool {
            return !ImageService::imageHasAlreadyGenerated(Storage::path($project->image_path));
        })->filter(fn ($value) => $value !== false)->count();
        return 100 - $count;
    }


    public static function getOrphanImageCount(): int
    {
        return collect(self::getOrphanImage(Project::all(), 'projects/images', fn (Project $media) => $media->image_path))->count();
    }

    public static function dropOrphans(): void
    {
        ImageService::drop(self::getOrphanImage(Project::all(), 'projects/images', fn (Project $project) => $project->image_path));
    }
}
