<?php

namespace App\Tasks;

use App\Models\Media;
use App\Services\ImageService;
use Illuminate\Support\Facades\Storage;

class MediaTask extends Task
{

    public static function generateImage(): void
    {
        Media::all()->each(function (Media $media) {
            ImageService::generate(Storage::path($media->path));
        });
    }

    public static function getUngeneratedImageCount(): int
    {
        $mediaCollection = Media::all();
        $count = $mediaCollection->map(function (Media $media): bool {
            return !ImageService::imageHasAlreadyGenerated(Storage::path($media->path));
        })->filter(fn ($value) => $value !== false)->count();
        return - ($count / $mediaCollection->count() * 100) + 100;
    }

    public static function getOrphanImageCount(): int
    {
        return collect(self::getOrphanImage(Media::all(), 'media', fn (Media $media) => $media->path))->count();
    }

    public static function dropOrphans(): void
    {
        ImageService::drop(self::getOrphanImage(Media::all(), 'media', fn (Media $media) => $media->path));
    }
}
