<?php

namespace App\Tasks;

use App\Services\ImageService;
use Illuminate\Database\Eloquent\Collection;

abstract class Task
{

    abstract public static function generateImage(): void;

    abstract public static function getUngeneratedImageCount(): int;


    abstract public static function dropOrphans(): void;
    final public static function getOrphanImage(Collection $collection, string $storagePath, callable $callback): array
    {
        return ImageService::orphans($collection, $storagePath, $callback);
    }
}
