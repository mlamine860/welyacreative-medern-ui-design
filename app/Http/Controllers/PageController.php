<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function privacyPolicy()
    {
        $page = Page::whereTitle('Politique des données')->firstOrFail();
        return view('privacy-policy', compact('page'));
    }
}
