<?php

namespace App\Http\Controllers;

use App\Services\ImageService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;


    public function stroreUploadedFile(UploadedFile $uploadedFile, string $path, ?string $old = null): string
    {
        if (!$old) {
            return $uploadedFile->store($path);
        }

        Storage::delete($old);
        ImageService::dropFormat($old);
        return $uploadedFile->store($path);
    }
}
