<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request, int $catId = null)
    {
        $latestPost = Post::latest()->first();
        $posts = Post::where('id', '!=', $latestPost?->id)->latest();
        $categories = Category::all();
        if ($catId !== null) {
            $posts =   $posts->where('category_id', '=', $catId);
        }
        $posts = $posts->paginate(10);
        return view('blog', compact('posts', 'latestPost', 'categories', 'catId'));
    }

    public function detail(Post $post)
    {
        $title = $post->title;
        return view('blog.detail', compact('post', 'title'));
    }
}
