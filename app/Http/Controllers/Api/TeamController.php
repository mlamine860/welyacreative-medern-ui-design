<?php

namespace App\Http\Controllers\Api;

use App\Models\Team;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use App\Http\Resources\TeamCollection;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Redirect;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $teams = Team::query()
            ->with('users')
            ->filter($request->only('search'))
            ->paginate(5)
            ->appends($request->all('search'));
        return Inertia::render('pages/Teams/Index', [
            'teams' => new TeamCollection($teams),
            'filters' => $request->all('search'),
        ]);
    }

    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Teams/Create', []);
    }

    public function store(Request $request)
    {
        $this->authorize('manage-site');
        $request->validate([
            'name' => ['required', 'string', 'min:5', 'max:20', Rule::unique('teams')],
            'description' => ['nullable', 'string']
        ]);
        Team::create($request->only('name', 'description'));
        return Redirect::route('teams.index')->with('success', 'Le team a été crée.');
    }

    public function update(Team $team, Request $request)
    {
        $this->authorize('manage-site');
        $request->validate([
            'name' => [
                'required', 'string', 'min:5', 'max:20',
                Rule::unique('teams', 'name')->ignore($team->id)
            ],
            'description' => ['nullable', 'string']
        ]);
        $team->update($request->only('name', 'description'));
        $usersInput  = $request->input('users');
        $users = [];
        if (!empty($usersInput)) {
            foreach ($usersInput as $user) {
                $users[] = $user['value'];
            }
            $team->users()->sync($users);
        }
        return Redirect::back()->with('success', 'Le team a été mise à jour.');
    }

    public function edit(Team $team)
    {
        $this->authorize('manage-site');
        $team->load('users');
        return Inertia::render('pages/Teams/Edit', [
            'team' => new TeamResource($team),
            'users' => new UserCollection(User::all()),
        ]);
    }

    public function destroy(Team $team)
    {
        $this->authorize('admin-site');
        $team->delete();
        return Redirect::route('teams.index')->with('success', 'Le team a été supprimé.');
    }
}
