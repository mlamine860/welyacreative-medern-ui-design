<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;
use App\Http\Requests\StorePostRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Services\ImageService;
use Illuminate\Support\Facades\Redirect;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        return  Inertia::render('pages/Posts/Index', [
            'posts' => new PostCollection(
                Post::query()
                    ->with('author')
                    ->with('category')
                    ->orderBy('title')
                    ->filter($request->only('search', 'published'))
                    ->paginate(5)
                    ->appends($request->all())
            ),
            'filters' => []
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Posts/Create', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        $this->authorize('manage-site');
        $post = new Post;
        $post->fill($request->validated());
        $post->image_path = $this->stroreUploadedFile($request->file('image'), 'posts/images');
        ImageService::generate(Storage::path($post->image_path));
        $post->user_id = $request->user()->id;
        $post->save();
        return Redirect::route('posts.edit', $post->id)->with('success', 'L\'article a été crée.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Posts/Edit', [
            'post' => new PostResource($post),
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $this->authorize('manage-site');
        $post->fill($request->validated());

        if ($request->hasFile('image')) {
            $post->image_path = $this->stroreUploadedFile($request->file('image'), 'posts/images', $post->image_path);
            ImageService::generate(Storage::path($post->image_path));
        }

        $post->save();
        return Redirect::back()->with('success', 'L\'article a été mise à jour.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $this->authorize('admin-site');
        $post->delete();
        Storage::delete($post->image_path);
        ImageService::dropFormat($post->image_path);
        return Redirect::route('posts.index')->with('success', 'L\'article a été supprimé.');
    }
}
