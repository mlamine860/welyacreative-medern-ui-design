<?php

namespace App\Http\Controllers\Api;

use App\Models\Team;
use App\Models\User;
use Inertia\Inertia;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Validation\Rules\File;
use App\Http\Resources\TeamCollection;
use App\Http\Resources\UserCollection;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PasswordUpdateRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use App\Http\Resources\ProfileResource;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function profile(Request $request)
    {
        $this->authorize("manage-site");
        $profile = $request->user()->profile ?  new ProfileResource($request->user()->profile) : null;
        return Inertia::render('pages/Users/Profile', [
            'profile' => $profile,
        ]);
    }

    public function passwordUpdate(PasswordUpdateRequest $request)
    {

        $this->authorize("manage-site");
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);
        $request->user()->update($validated);
        return Redirect::back()->with('success', 'Votre mot de passe a été mise à jour.');
    }

    public function uploadAvatar(Request $request)
    {

        $this->authorize("manage-site");
        $request->validate([
            'avatar' => ['required', File::types(['png', 'jpg', 'svg'])]
        ]);
        $profile = $request->user()->profile;
        $profile->photo_path = $this->stroreUploadedFile(
            $request->file('avatar'),
            $profile->imageStorageDir,
            $profile->photo_path
        );
        $profile->save();
        return Redirect::back()->with('success', 'Votre avatar a été mise à jour.');
    }
    public function profileUpdate(UserProfileUpdateRequest $request)
    {

        $this->authorize("manage-site");
        $user = $request->user();
        if ($user->profile()->exists()) {
            $user->profile()->update($request->validated());
        } else {
            $user->profile()->create($request->validated());
        }
        return Redirect::back()->with('success', 'Votre profile a été mise à jour.');
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {

        $this->authorize("manage-site");
        $users = User::filter($request->only('search', 'role'))
            ->where('id', '!=', $request->user()->id)
            ->paginate(5)
            ->appends($request->all());
        return Inertia::render('pages/Users/Index', [
            'users' => new UserCollection($users),
            'filters' => $request->all('search', 'role')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $this->authorize("manage-site");
        return Inertia::render('pages/Users/Create', []);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserStoreRequest $request)
    {
        $this->authorize('admin-site');
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);
        $user = new User(attributes: $validated);
        if ($request->has('role')) {
            $user->role = $request->validated('role');
        }
        $user->email_verified_at = now();
        $user->save();
        return Redirect::route('users.index')->with('success', 'L\'utilisateur a été crée.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $user->load('teams');
        $this->authorize("manage-site");
        return Inertia::render('pages/Users/Edit', [
            'user' => new UserResource($user),
            'teams' => new TeamCollection(Team::all('name', 'id'))
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $this->authorize('admin-site');
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);
        $user->fill($validated);
        if ($request->has('role')) {
            $user->role = $request->validated('role');
        }
        $teamInput = $request->input('teams');
        $teams = [];
        if (!empty($teamInput)) {
            foreach ($teamInput as $team) {
                $teams[] = $team['value'];
            }
        }
        $user->teams()->sync($teams);
        if ($request->has('active')) {
            $user->email_verified_at = $request->validated('active') ? now() : null;
        }
        $user->save();
        return Redirect::back()->with('success', 'L\'utilisateur a été modifié.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $this->authorize('admin-site');
        $user->delete();
        return Redirect::route('users.index')->with('success', 'L\'utilisateur a été supprimé.');
    }
}
