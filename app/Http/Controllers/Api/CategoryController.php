<?php

namespace App\Http\Controllers\Api;

use Inertia\Inertia;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\CategoryCollection;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        return  Inertia::render('pages/Categories/Index', [
            'categories' => new CategoryCollection(
                Category::query()
                    ->orderBy('name')
                    ->filter($request->only('search'))
                    ->paginate(5)
                    ->appends($request->all())
            ),
            'filters' => []
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Categories/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:5', 'max:255'],
        ]);
        $categoryName = $request->input('name');
        $category = Category::query()->create(attributes: [
            'name' => $categoryName,
            'slug' => Str::slug($categoryName)
        ]);
        return Redirect::route('categories.edit', $category)->with('success', 'La category a été créée.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category, Request $request)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Categories/Edit', [
            'category' => new CategoryResource($category),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('manage-site');
        $request->validate([
            'name' => ['required', 'string', 'min:5', 'max:255'],
        ]);
        $categoryName = $request->input('name');
        $category->update(attributes: [
            'name' => $categoryName,
            'slug' => Str::slug($categoryName),
        ]);
        return Redirect::back()->with('success', 'La category a été modifié.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $this->authorize('admin-site');
        $category->delete();
        return Redirect::route('categories.index')->with('success', 'La category a été supprimé.');
    }
}
