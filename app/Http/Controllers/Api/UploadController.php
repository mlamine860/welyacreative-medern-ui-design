<?php

namespace App\Http\Controllers\Api;

use App\Models\Media;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Services\ImageService;
use App\Services\MediaService;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rules\File as FileRule;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function uploads(Request $request)
    {
        $this->authorize('manage-site');
        $request->validate([
            'image' => [
                'required',
                FileRule::types(['png', 'jpg', 'jpeg', 'webp'])
                    ->max(5 * 1024),
            ]
        ]);
        $image  = $request->file('image');
        $fileHash = MediaService::getFileHash($image->path());
        $media = MediaService::getMediaRecord($fileHash, $image->getClientOriginalName(),);
        if ($media) {
            return $media;
        }
        $name = $image->store('media');
        $createdMedia = MediaService::create(
            File::basename($name),
            $image->getClientOriginalName(),
            $fileHash,
            $image->getSize(),
            config('media.disk'),
            $name,
            $image->getMimeType()
        );
        ImageService::generate(Storage::path($createdMedia->path));
        return $createdMedia;
    }
    /**
     * Handle the incoming request.
     */

    public function filepondProcess(UploadRequest $request)
    {
        $this->authorize('manage-site');
        /** @var UploadedFile */
        $file = $request->file('files');
        $path = Storage::put("tmp", $file);
        return [
            'path' => $path,
            'name' => $file->getClientOriginalName(),
        ];
    }

    public function filepondRevert(Request $request)
    {

        $this->authorize('manage-site');
        Storage::delete($request->input('filePath'));
        return 'success';
    }

    public function filePondRemove(Request $request)
    {
        $this->authorize('manage-site');
        $media = Media::where('name', $request->media_name)->first();
        $project = Project::whereId($request->mediable_id)->first();
        if ($media && $project) {
            $project->media()->detach($media->id);
            return 'success';
        } else {
            throw new \Error('Filepond remove error');
        }
    }
}
