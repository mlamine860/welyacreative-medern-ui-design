<?php

namespace App\Http\Controllers\Api;

use App\Models\Tag;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Resources\TagResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\TagCollection;
use App\Http\Requests\StoreTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Http\Resources\ProjectCollection;
use App\Models\Project;
use Illuminate\Support\Facades\Redirect;

class TagController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $teams = Tag::query()
            ->filter($request->only('search'))
            ->paginate(5)
            ->appends($request->all('search'));
        return Inertia::render('pages/Tags/Index', [
            'teams' => new TagCollection($teams),
            'filters' => $request->all('search'),
        ]);
    }

    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Tags/Create', []);
    }

    public function store(StoreTagRequest $request)
    {
        $this->authorize('manage-site');
        Tag::create($request->validated());
        return Redirect::route('tags.index')->with('success', 'Le tag a été crée.');
    }

    public function update(Tag $tag, UpdateTagRequest $request)
    {
        $this->authorize('manage-site');
        $tag->update($request->validated());
        $projectsInput  = $request->input('projects');
        $projects = [];
        if (!empty($projectsInput)) {
            foreach ($projectsInput as $project) {
                $projects[] = $project['value'];
            }
            $tag->projects()->sync($projects);
        }
        return Redirect::back()->with('success', 'Le tag a été mise à jour.');
    }

    public function edit(Tag $tag)
    {
        $tag->load('projects');
        $this->authorize('manage-site');
        return Inertia::render('pages/Tags/Edit', [
            'tag' => new TagResource($tag),
            'projects' => new ProjectCollection(Project::all())
        ]);
    }

    public function destroy(Tag $tag)
    {
        $this->authorize('admin-site');
        $tag->delete();
        return Redirect::route('tags.index')->with('success', 'Le tag a été supprimé.');
    }
}
