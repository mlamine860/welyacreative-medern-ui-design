<?php

namespace App\Http\Controllers\Api;

use Inertia\Inertia;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\QuestionCollection;
use App\Http\Requests\QuestionStoreRequest;
use App\Http\Requests\QuestionUpdateRequest;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $questions = Question::query()
            ->filter(
                $request->only(
                    'search',
                )
            )->paginate(5)
            ->appends($request->all());
        return Inertia::render('pages/Questions/Index', [
            'filters' => $request->all('search'),
            'questions' => new QuestionCollection($questions)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Questions/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(QuestionStoreRequest $request)
    {
        $this->authorize('manage-site');
        Question::create($request->validated());
        return Redirect::route('questions.index')->with('success', 'La question à été créé.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Question $question)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Questions/Edit', [
            'question' => new QuestionResource($question),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(QuestionUpdateRequest $request, Question $question)
    {
        $this->authorize('manage-site');
        $question->update($request->validated());
        return Redirect::route('questions.index')->with('success', 'La question a été mise à jour.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question)
    {
        $this->authorize('admin-site');
        $question->delete();
        return Redirect::route('questions.index')->with('success', 'La question a été supprimé.');
    }
}
