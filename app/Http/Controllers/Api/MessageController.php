<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MessageResource;
use Inertia\Inertia;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MessageCollection;
use Illuminate\Support\Facades\Redirect;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        return  Inertia::render('pages/Messages/Index', [
            'messages' => new MessageCollection(
                Message::query()
                    ->orderBy('username')
                    ->filter($request->only('search'))
                    ->paginate(5)
                    ->appends($request->all())
            ),
            'filters' => []
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Message $message)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Messages/Show', [
            'message' => new MessageResource($message),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Message $message)
    {
        $this->authorize('admin-site');
        $message->delete();
        return Redirect::route('messages.index')->with('success', 'Le message a été supprimé.');
    }
}
