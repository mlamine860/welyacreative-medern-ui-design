<?php

namespace App\Http\Controllers\Api;

use App\Models\Tag;
use Inertia\Inertia;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TagCollection;
use App\Http\Resources\ProjectResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\ProjectCollection;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Services\ImageService;
use App\Services\MediaService;
use App\Services\TagService;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $projects = Project::with('creator')
            ->filter($request->only('search'))
            ->paginate(5)
            ->appends($request->all());
        return  Inertia::render('pages/Projects/Index', [
            'projects' => new ProjectCollection($projects),
            'filters' => $request->all('search')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Projects/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {
        $this->authorize('manage-site');
        $project = new Project(attributes: $request->validated());
        $project->image_path = $this->stroreUploadedFile($request->file('image'), 'projects/images');
        ImageService::generate(Storage::path($project->image_path));
        $project->author = $request->user()->id;
        $project->save();
        MediaService::attach($request->input('files') ?? [], $project);
        return Redirect::route('projects.index')->with('success', 'Le projet a été créé.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Project $project)
    {
        $project->load('tags', 'media');
        return Inertia::render('pages/Projects/Edit', [
            'project' => new ProjectResource($project),
            'tags' => new TagCollection(Tag::all())
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Project $project, UpdateProjectRequest $request)
    {
        $this->authorize('manage-site');
        $project->fill(attributes: $request->validated());
        if ($request->hasFile('image')) {
            $project->image_path = $this->stroreUploadedFile(
                $request->file('image'),
                'projects/images',
                $project->image_path
            );
            ImageService::generate(Storage::path($project->image_path));
        }
        $project->save();

        TagService::attach($request->input('tags') ?? [], $project);
        MediaService::attach($request->input('files') ?? [], $project);

        return Redirect::back()->with('success', 'Le projet a été mise à jour.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $this->authorize('admin-site');
        $project->delete();
        Storage::delete($project->image_path);
        ImageService::dropFormat($project->image_path);
        return Redirect::route('projects.index')->with('success', 'Le projet a été supprimé.');
    }
}
