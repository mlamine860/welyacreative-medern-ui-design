<?php

namespace App\Http\Controllers\Api;

use App\Models\Media;
use Illuminate\Http\Request;
use App\Services\ImageService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{


    public function index()
    {
        $this->authorize('manage-site');
        return view('uploads/media');
    }
    public function list(Request $request)
    {
        $this->authorize('manage-site');
        return Media::query()->select('id', 'path', 'name', 'file_name')
            ->orderBy('created_at', $request->query('sortBy') ?? 'asc')
            ->paginate(12);
    }

    public function destroy(Media $media)
    {
        $media->delete();
        Storage::delete($media->path);
        ImageService::dropFormat($media->path);
        return $media;
    }
}
