<?php

namespace App\Http\Controllers\Api;

use App\Models\Page;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PageCollection;
use App\Http\Requests\PageStoreRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Http\Resources\PageResource;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function uploadImage(Request $request)
    {
        $this->authorize('manage-site');
        $request->validate([
            'image' => [
                'required', 'image'
            ]
        ]);
        $location = $this->stroreUploadedFile($request->file('image'), 'pages/images');

        return response()->json([
            'location' => Storage::url($location),
        ]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $pages = Page::with('creator')
            ->filter($request->only('search'))
            ->paginate(5)
            ->appends($request->all());
        return  Inertia::render('pages/Pages/Index', [
            'pages' => new PageCollection($pages),
            'filters' => $request->all('search')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Pages/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PageStoreRequest $request)
    {
        $this->authorize('manage-site');
        $request->user()->pages()
            ->create(attributes: $request->validated());
        return Redirect::route('pages.index')->with('success', 'La page a été créée.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Page $page)
    {
        return Inertia::render('pages/Pages/Edit', [
            'page' => new PageResource($page),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Page $page, PageUpdateRequest $request)
    {
        $this->authorize('manage-site');
        $page->update(attributes: $request->validated());
        return Redirect::back()->with('success', 'La page a été mise à jour.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Page $page)
    {
        $this->authorize('admin-site');
        $page->delete();
        return Redirect::route('pages.index')->with('success', 'La page a été supprimé.');
    }
}
