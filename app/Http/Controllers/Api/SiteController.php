<?php

namespace App\Http\Controllers\Api;

use App\Models\Site;
use Inertia\Inertia;
use App\Http\Controllers\Controller;
use App\Http\Requests\SiteUpdateRequest;
use App\Http\Resources\SiteResource;
use Illuminate\Support\Facades\Redirect;

class SiteController extends Controller
{
    public function edit()
    {
        $site = Site::firstOrNew();
        return Inertia::render('pages/Sites/Edit', [
            'site' => new SiteResource($site)
        ]);
    }

    public function update(SiteUpdateRequest $request)
    {
        $site = Site::firstOrNew();
        $site->fill(attributes: $request->validated());
        if ($request->hasFile('logo')) {
            $site->logo_url = $this->stroreUploadedFile($request->file('logo'), $site->storageLogoDir, $site->logo_url);
        }
        if ($request->hasFile('image')) {
            $site->image_url = $this->stroreUploadedFile($request->file('image'), $site->storageImageDir, $site->image_url);
        }
        $site->save();
        return Redirect::back()->with('success', 'Les paramètres du site a été mise à jour.');
    }
}
