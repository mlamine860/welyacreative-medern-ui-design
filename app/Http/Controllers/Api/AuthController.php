<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            $user->tokens()->delete();
            $token = $user->createToken('auth-token');
            return response()->json([
                'user' => $user->load('profile'),
                'token' => $token->plainTextToken,
            ]);
        }
        return response()->json([
            'status' => 'failure',
            'message' => __('Ces identifiants ne correspondent pas à nos enregistrements.')

        ], 422);
    }
}
