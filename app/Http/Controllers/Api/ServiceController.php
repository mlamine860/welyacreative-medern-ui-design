<?php

namespace App\Http\Controllers\Api;

use Inertia\Inertia;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Events\ServiceCreated;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\ServiceCollection;
use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use App\Services\ImageService;

class ServiceController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $services = Service::query()
            ->filter(
                $request->only(
                    'search',
                    'featured',
                    'published'
                )
            )->paginate(5)
            ->appends($request->all());
        return Inertia::render('pages/Services/Index', [
            'filters' => $request->all('featured', 'published', 'search'),
            'services' => new ServiceCollection($services)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Services/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreServiceRequest $request)
    {
        $this->authorize('manage-site');

        $service = new Service;
        $service->fill($request->validated());
        $service->user_id = $request->user()->id;

        if ($request->hasFile('icon')) {
            $service->icon_url = $this->stroreUploadedFile($request->file('icon'), $service->iconStorageDir);
        }
        if ($request->hasFile('image')) {
            $service->image_url = $this->stroreUploadedFile($request->file('image'), $service->imageStorageDir);
            ImageService::generate(Storage::path($service->image_url));
        }
        $service->save();

        // Notifify subscriber if there more than 3 services in database
        if (Service::count() > 3) {
            ServiceCreated::dispatch($service);
        }

        return Redirect::route('services.index')->with('success', 'Le sevice a été mise créé .');
    }

    /**
     * Display the specified resource.
     */
    public function show(Service $service)
    {
        $this->authorize('manage-site');
        return new ServiceResource($service);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Service $service)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Services/Edit', [
            'service' => new ServiceResource($service)
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateServiceRequest $request, Service $service)
    {
        $this->authorize('manage-site');
        $service->fill($request->validated());

        if ($request->hasFile('icon')) {
            $service->icon_url = $this->stroreUploadedFile($request->file('icon'), $service->iconStorageDir, $service->icon_url);
        }
        if ($request->hasFile('image')) {
            $service->image_url = $this->stroreUploadedFile($request->file('image'), $service->imageStorageDir, $service->image_url);
            ImageService::generate(Storage::path($service->image_url));
        }
        $service->save();

        return Redirect::back()->with('success', 'Le sevice a été mise à jour.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Service $service)
    {
        $this->authorize('admin-site');
        $service->delete();
        Storage::delete($service->icon_url);
        Storage::delete($service->image_url);
        ImageService::dropFormat($service->image_url);
        return Redirect::route('services.index')->with('success', 'Le service a été supprimé.');
    }
}
