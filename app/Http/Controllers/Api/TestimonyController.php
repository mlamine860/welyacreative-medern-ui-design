<?php

namespace App\Http\Controllers\Api;

use Inertia\Inertia;
use App\Models\Testimony;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\TestimonyResource;
use App\Http\Resources\TestimonyCollection;
use App\Http\Requests\TestimonyStoreRequest;
use App\Http\Requests\TestimonyUpdateRequest;
use Illuminate\Support\Facades\Storage;

class TestimonyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $testimonies = Testimony::query()
            ->filter($request->only('search'))
            ->paginate(5)
            ->appends($request->all('search'));
        return Inertia::render('pages/Testimonies/Index', [
            'testimonies' => new TestimonyCollection($testimonies),
            'filters' => $request->all('search'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Testimonies/Create', []);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TestimonyStoreRequest $request)
    {
        $this->authorize('manage-site');
        $testimony = new Testimony;
        $testimony->fill($request->validated());
        if ($request->hasFile('image')) {
            $testimony->image_url = $this->stroreUploadedFile($request->file('image'), $testimony->imageStorageDir);
        }
        $testimony->save();
        return Redirect::route('testimonies.index')->with('success', 'Le témoignage a été créé.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Testimony $testimony)
    {
        $this->authorize('manage-site');
        return Inertia::render('pages/Testimonies/Edit', [
            'testimony' => new TestimonyResource($testimony),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TestimonyUpdateRequest $request, Testimony $testimony)
    {
        $this->authorize('manage-site');
        $testimony->update($request->validated());
        if ($request->hasFile('image')) {
            $testimony->image_url = $this->stroreUploadedFile($request->file('image'), $testimony->imageStorageDir, $testimony->image_url);
            $testimony->save();
        }
        return Redirect::back()->with('success', 'Le témoignage a été modifié.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Testimony $testimony)
    {
        $this->authorize('admin-site');
        $testimony->delete();
        Storage::disk()->delete($testimony->image_url);
        return Redirect::route('testimonies.index')->with('success', 'Le témoignage a été supprimé.');
    }
}
