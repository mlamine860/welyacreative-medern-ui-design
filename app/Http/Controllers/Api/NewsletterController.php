<?php

namespace App\Http\Controllers\Api;

use Inertia\Inertia;
use App\Models\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\NewsletterResource;
use App\Http\Resources\NewsletterCollection;

class NewsletterController extends Controller
{

    public function index(Request $request)
    {
        $this->authorize('manage-site');
        $newsletters = Newsletter::filter($request->only('search'))
            ->paginate(5)
            ->appends($request->all());
        return  Inertia::render('pages/Newsletters/Index', [
            'newsletters' => new NewsletterCollection($newsletters),
            'filters' => $request->all('search')
        ]);
    }

    public function edit(Request $request, string $email)
    {
        $newsletter = Newsletter::whereEmail($email)->first();
        return  Inertia::render('pages/Newsletters/Edit', [
            'pages' => new NewsletterResource($newsletter),
            'filters' => $request->all('search')
        ]);
    }


    public function destroy(string $email)
    {
        $newsletter = Newsletter::whereEmail($email)->first();
        $newsletter->delete();
        Redirect::back()->with('success', 'l\'adresse e-mail a été suppreimé.');
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email'],
        ]);
        $newsletter = Newsletter::whereEmail($request->email)->first();
        if (!$newsletter) {
            Newsletter::create($request->only(['email']));
        }

        return new JsonResponse([
            'status' => 'Success',
        ], 201);
    }
    public function unsubscribe(string $email)
    {

        $newsletter = Newsletter::whereEmail($email)->first();
        if ($newsletter) {
            $newsletter->update(['subscribe' => false]);
        }

        return new JsonResponse([
            'status' => 'Success',
        ], 200);
    }
}
