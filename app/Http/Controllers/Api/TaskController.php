<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Tasks\MediaTask;
use App\Tasks\PostTask;
use App\Tasks\ProjectTask;
use App\Tasks\ServiceTask;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TaskController extends Controller
{
    public function index()
    {

        return Inertia::render('pages/Tasks', [
            'tasks' => [
                [
                    'name' => 'Service', 'url' => route('tasks.process', ['name' => 'services']), 'icon' => 'services',
                    'count' => ServiceTask::getUngeneratedImageCount(),
                    'orphans' => ServiceTask::getOrphanImageCount()
                ],
                [
                    'name' => 'Post', 'url' => route('tasks.process', ['name' => 'posts']), 'icon' => 'blog',
                    'count' => PostTask::getUngeneratedImageCount(),
                    'orphans' => PostTask::getOrphanImageCount()
                ],
                [
                    'name' => 'Project', 'url' => route('tasks.process', ['name' => 'projects']),
                    'icon' => 'projects', 'count' => ProjectTask::getUngeneratedImageCount(),
                    'orphans' => ProjectTask::getOrphanImageCount()
                ],
                [
                    'name' => 'Media', 'url' => route('tasks.process', ['name' => 'media']), 'icon' => 'media',
                    'count' => MediaTask::getUngeneratedImageCount(),
                    'orphans' => MediaTask::getOrphanImageCount()
                ],
            ]
        ]);
    }

    public function process(Request $request)
    {
        switch ($request->query('name')) {
            case 'services':
                ServiceTask::generateImage();
                return response()->json([
                    'status' => 'Success',
                    'count' => ServiceTask::getUngeneratedImageCount(),
                ], 200);
            case 'media':
                MediaTask::generateImage();
                return response()->json([
                    'status' => 'Success',
                    'count' => MediaTask::getUngeneratedImageCount(),
                ], 200);
            case 'projects':
                ProjectTask::generateImage();
                return response()->json([
                    'status' => 'Success',
                    'count' => ProjectTask::getUngeneratedImageCount(),
                ], 200);
            case 'posts':
                PostTask::generateImage();
                return response()->json([
                    'status' => 'Success',
                    'count' => PostTask::getUngeneratedImageCount(),
                ], 200);
            case 'orphans':
                $this->handleDropOrphansTask($request->query('type'));
                return response()->json([
                    'status' => 'Success',
                    'message' => '',
                ], 200);

            default:
                return response()->json([
                    'status' => 'Failure',
                    'message' => 'Task not found'
                ], 404);
        }
    }
    private function handleDropOrphansTask(string $taskType): void
    {
        $taskName = "App\\Tasks\\{$taskType}Task";
        call_user_func([$taskName, 'dropOrphans']);
    }
}
