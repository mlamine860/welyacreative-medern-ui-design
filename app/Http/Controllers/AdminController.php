<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostCollection;
use App\Models\Category;
use App\Models\Page;
use App\Models\Post;
use App\Models\Site;
use Inertia\Inertia;
use App\Models\Message;
use App\Models\Newsletter;
use App\Models\Project;
use App\Models\Service;
use App\Models\Tag;
use App\Models\Team;
use App\Models\Testimony;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function  __invoke(Request $request)
    {
        $this->authorize('manage-site');
        $latestPosts = Post::with('author', 'category')
            ->filter($request->only('search', 'published'))
            ->orderBy('title')
            ->take(5)
            ->get();
        $data =  [
            'site' => Site::first(),
            'data' => [
                [
                    'label' => 'Services',
                    'count' => Service::count(),
                    'icon' => 'services',
                    'url' => route('services.index')
                ],
                [
                    'label' => 'Articles',
                    'count' => Post::count(),
                    'icon' => 'blog',
                    'url' => route('posts.index')
                ],
                [
                    'label' => 'Projets',
                    'count' => Project::count(),
                    'icon' => 'projects',
                    'url' => route('projects.index')
                ],
                [
                    'label' => 'Pages',
                    'count' => Page::count(),
                    'icon' => 'pages',
                    'url' => route('pages.index')
                ],
                [
                    'label' => 'Messages',
                    'count' => Message::count(),
                    'icon' => 'messages',
                    'url' => route('messages.index')
                ],
                [
                    'label' => 'Teams',
                    'count' => Team::count(),
                    'icon' => 'teams',
                    'url' => route('teams.index')
                ],
                [
                    'label' => 'Témoignages',
                    'count' => Testimony::count(),
                    'icon' => 'testimony',
                    'url' => route('testimonies.index')
                ],
                [
                    'label' => 'Catégories',
                    'count' => Category::count(),
                    'icon' => 'category',
                    'url' => route('categories.index')
                ],
                [
                    'label' => 'Tags',
                    'count' => Tag::count(),
                    'icon' => 'tags',
                    'url' => route('tags.index')
                ],
                [
                    'label' => 'Souscriptions',
                    'count' => Newsletter::count(),
                    'icon' => 'subscribe',
                    'url' => route('newsletters.index')
                ],
            ],
            'latestPosts' => new PostCollection($latestPosts)
        ];
        return Inertia::render('pages/Dashboard', [
            'dashboardData' => $data,
            'filters' => []
        ]);
    }


    public function logout()
    {
        Auth::logout();
        return Inertia::location(route('login'));
    }
}
