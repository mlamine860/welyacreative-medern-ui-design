<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Project;
use Illuminate\Database\Eloquent\Builder;

class ProjectController extends Controller
{
    public function index(string $slug = null)
    {

        if ($slug) {
            $projects = Project::whereHas('tags', function (Builder $query) use ($slug) {
                $query->where('slug', $slug);
            })->get();
        } else {
            $projects = Project::with('tags')->get();
        }

        $tags = Tag::all();
        return view('projects.index', compact('projects', 'tags'));
    }
    public function show(Project $project)
    {
        return view('projects.show', compact('project'));
    }
}
