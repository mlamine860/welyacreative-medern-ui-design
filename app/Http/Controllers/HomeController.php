<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __invoke()
    {
        $questions = Question::query()
            ->select('question', 'answer')
            ->limit(5)->get();
        return view('welcome', compact('questions'));
    }
}
