<?php

namespace App\Http\Controllers;

use App\Events\MessageCreated;
use App\Models\Site;
use App\Models\Message;
use App\Mail\MessageMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\StoreMessageRequest;

class MessageController extends Controller
{
    public function __invoke(StoreMessageRequest $request)
    {

        $response = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
            'response' => $request->get('recaptcha'),
            'secret' => env('RECAPTCHA_SECRET')
        ]);

        if (!$response['success']) {
            return response()->json([
                'success' => false,
            ]);
        }

        $message = Message::create($request->validated());

        try {
            MessageCreated::dispatch($message, ['site' => Site::first()]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return response()->json([
            'success' => true,
        ]);
    }
}
