<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:5', 'unique:services'],
            'description' => ['required', 'string',],
            'content' => ['required', 'string',],
            'icon' => ['mimes:svg'],
            'image' => ['image'],
            'published' => ['required', 'boolean'],
            'featured' => ['required', 'boolean']
        ];
    }
}
