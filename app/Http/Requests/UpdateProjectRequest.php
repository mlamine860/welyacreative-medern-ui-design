<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required', 'string', 'min:5', 'max:255',
                Rule::unique('projects')->ignore($this->route('project'))
            ],
            'description' => ['required', 'string', 'min:10'],
            'image' => ['nullable', 'image'],
            'content' => ['nullable', 'string', 'min:10'],
            'files' => ['nullable', 'array']
        ];
    }
}
