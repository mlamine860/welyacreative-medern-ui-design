<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'tagline' => ['required', 'string'],
            'address' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'url' => ['required', 'string', 'url'],
            'phone' => ['required', 'string'],
            'image' => ['nullable', 'image'],
            'logo' => ['nullable', 'image'],
        ];
    }
}
