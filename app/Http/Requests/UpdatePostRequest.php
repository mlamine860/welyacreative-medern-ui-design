<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:5', 'max:255'],
            'slug' => [
                'required', 'string', 'min:5', 'max:255',
                Rule::unique('posts')->ignore($this->route('post'))
            ],
            'content' => ['required', 'string', 'min:15'],
            'category_id' => ['required'],
            'image' => ['nullable', 'image'],
            'published' => ['required', 'boolean'],
            'excerpt' => ['required', 'string'],
        ];
    }
}
