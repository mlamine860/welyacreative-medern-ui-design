<?php

namespace App\Http\Requests;

use App\Utils\Role;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required', 'string', 'min:5', 'max:60',
                Rule::unique('users')->ignore($this->route('user')->id)
            ],
            'email' => ['required', 'string', 'email'],
            'password' => ['nullable', 'string', 'min:6'],
            'active' => ['nullable', 'boolean'],
            'role' => ['required', 'string', new Enum(Role::class)],
        ];
    }
}
