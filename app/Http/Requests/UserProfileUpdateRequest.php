<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => [
                'required', 'string', 'min:2', 'max:60'
            ],
            'last_name' => [
                'required', 'string', 'min:2', 'max:60'
            ],
            'address' => [
                'required', 'string',
            ],
            'phone' => [
                'required', 'string',
            ],
            'department' => [
                'required', 'string',
            ],
            'role' => [
                'required', 'string',
            ],
            'languages' => [
                'required', 'string',
            ],
            'organization' => [
                'required', 'string',
            ],
            'education'  => [
                'required', 'string',
            ],
            'work_history'  => [
                'required', 'string',
            ],
            'birthday'  => [
                'required', 'date',
            ],
            'bio'  => [
                'required', 'string',
            ]
        ];
    }
}
