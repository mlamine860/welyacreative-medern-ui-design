<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TestimonyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => [
                'required', 'string', 'min:2', 'max:60',
                Rule::unique('testimonies')
            ],
            'job' => ['required', 'string', 'min:5', 'max:60'],
            'image' => ['required', 'image'],
            'message' => ['required', 'string', 'min:10',],
        ];
    }
}
