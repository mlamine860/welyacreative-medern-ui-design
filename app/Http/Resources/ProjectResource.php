<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    public static $wrap = "";
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image,
            'author' => $this->whenLoaded('creator'),
            'content' => $this->content,
            'tags' => new TagCollection($this->whenLoaded('tags')),
            'media' => new MediaCollection($this->whenLoaded('media')),
        ];
    }
}
