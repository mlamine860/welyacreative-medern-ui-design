<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public static $wrap = '';
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'last_seen' => $this->last_seen,
            'avatar' => $this->avatar,
            'active' => $this->active,
            'role' => $this->role,
            'profile' => new ProfileResource($this->whenLoaded('profile')),
            'teams' => new TeamCollection($this->whenLoaded('teams')),
        ];
    }
}
