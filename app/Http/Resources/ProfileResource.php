<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * 
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'address' => $this->address,
            'phone' => $this->phone,
            'department' => $this->department,
            'role' => $this->role,
            'languages' => $this->languages,
            'organization' => $this->organization,
            'education' => $this->education,
            'work_history' => $this->work_history,
            'birthday' => $this->birthday,
            'bio' => $this->bio,
        ];
    }
}
