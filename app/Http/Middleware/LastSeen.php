<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\Response;

class LastSeen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = $request->user();
        if(!$user) return $next($request);

        $lastSeen = $user->last_seen;
        if(!$lastSeen){
            $user->last_seen = now();
            $user->save();
        }elseif(now()->diff(Carbon::parse($lastSeen))->format('%i') > 10){
            $user->last_seen = now();
            $user->save();
        }
        return $next($request);
    }
}
