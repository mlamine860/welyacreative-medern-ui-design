<?php

namespace App\Providers;

use App\Utils\Role;
use App\Models\Post;
use App\Models\Site;
use App\Models\Team;
use App\Models\Project;
use App\Models\Service;
use App\Models\Testimony;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if (!$this->app->runningInConsole()) {
            view()->share([
                'site' => Site::first(),
                'featuredServices' => Service::whereFeatured(true)->wherePublished(true)->get(),
                'teams' => Team::whereName('admin')->first()?->users ?? [],
                'posts' => Post::latest()->wherePublished(true)->limit(3)->get(),
                'projects' => Project::latest()->limit(5)->get(),
                'testimonials' => Testimony::query()->limit(6)->get(),
            ]);
        }
    }
}
