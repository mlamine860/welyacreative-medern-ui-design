<?php

namespace App\Listeners;

use App\Mail\MessageMail;
use App\Events\MessageCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessageNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(MessageCreated $event): void
    {
        try {
            Mail::to(env('MAIL_TO_CONTACT'))->send(new MessageMail($event->message, $event->params['site']));
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
