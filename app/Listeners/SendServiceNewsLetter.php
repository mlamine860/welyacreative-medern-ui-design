<?php

namespace App\Listeners;

use App\Events\ServiceCreated;
use App\Mail\ServiceMail;
use App\Models\Newsletter;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendServiceNewsLetter
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(ServiceCreated $event): void
    {
        Newsletter::whereSubscribe(true)->get()->each(function (Newsletter $newsletter) use ($event) {
            Mail::to($newsletter->email)->send(new ServiceMail($event->service, $event->params));
        });
    }
}
