<?php

namespace App\Mail;

use App\Models\Site;
use App\Models\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class ServiceMail extends Mailable
{
    use Queueable, SerializesModels;

    public Site $site;
    public array $params = [];

    /**
     * Create a new message instance.
     */
    public function __construct(public Service $service)
    {
        $this->site = Site::first();
        $latest = Service::latest()->where('id', '!=', $service->id)->take(3)->get();
        $this->params['first'] = $latest[0];
        $this->params['second'] = $latest[1];
        $this->params['third'] = $latest[2];
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Un nouveau service est ajouté',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.service',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
