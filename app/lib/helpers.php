<?php

if (!function_exists('app_route')) {
    function app_route(string $route, string $hash)
    {
        return request()->routeIs('welcome') ? "/{$hash}" : route($route);
    }
}

if (!function_exists('is_current_route')) {
    function set_active_class(string $route)
    {
        return request()->routeIs($route) ?  'active' : '';
    }
}
