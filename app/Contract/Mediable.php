<?php

namespace App\Contract;

use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface Mediable
{
    public function media(): MorphToMany;

}
