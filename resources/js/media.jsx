import { createRoot } from "react-dom/client";
import React from "react";
import MediaApp from "./src/MediaApp";

const rootElement = createRoot(document.getElementById("root"));


rootElement.render(<MediaApp />)