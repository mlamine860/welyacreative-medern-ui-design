import "./bootstrap"
import { object, string, setLocale } from 'yup';
import { fr } from 'yup-locales';
import "yup-phone-lite";
import {pick} from 'underscore'
setLocale(fr);


const navOpenBtn = document.querySelector('[data-nav-open-btn]')
const navCloseBtn = document.querySelector('[data-nav-close-btn]')
const navBar = document.querySelector('[data-navbar]')
const overlay = document.querySelector('[data-overlay]')
const dropdownToggler = document.querySelector('[data-toggle-dropdown]')
const navLinks = document.querySelectorAll('[data-navbar-link]')
const navDropdown = document.querySelector('[data-nav-dropdown]')


const elements = [navOpenBtn, navCloseBtn, overlay]
elements.forEach(element => {
    element?.addEventListener('click', function () {
        navBar.classList.toggle('active')
        overlay.classList.toggle('active')
    })
})



const gotop = document.querySelector('[data-go-top]')

window.addEventListener('scroll', () => {
    if (window.scrollY >= 400) {
        gotop.classList.add('active')
    } else {
        gotop.classList.remove('active')
    }

})


// Toggle menu dropdown

dropdownToggler?.addEventListener('click', function(){
    navDropdown.classList.toggle('hidden')
})

dropdownToggler?.addEventListener('mouseover', function(){
    if(window.innerWidth  > 768){
           if(!navDropdown.classList.contains('hidden')) return 
           navDropdown.classList.remove('hidden')
    }
})

navLinks.forEach(link => {
    link.addEventListener('click', () => {
        navDropdown.classList.add('hidden')
    })
})

document.addEventListener('click', ({target}) => {
    if(!navDropdown.classList.contains('hidden') && !navDropdown.parentElement.contains(target) ){
        navDropdown.classList.add('hidden')
    }
})


document.addEventListener('mouseover', ({target}) => {
    if(!navDropdown?.classList.contains('hidden') && !navDropdown?.parentElement.contains(target) ){
        navDropdown?.classList.add('hidden')
    }
})


// Alpinejs

document.addEventListener('alpine:init', () => {
    Alpine.data('contactForm', () => ({
        init(){
            this.$refs.alertmessagebox.classList.remove('hidden')
            this.$refs.alertmessagebox.classList.add('flex')

        },
        fields: {
            username: '',
            email: '',
            address: '',
            phone: '',
            subject: '',
            message: '',
        },
        errors: {
        },
        toast: false,
        timer: null,
        toastWidth: 0,
        loading: false,
        token: '',
        get toastWidthStyle(){
            return {width: `${this.toastWidth}%`}
            
        },
        get formData () {
            return pick(this.fields, ['username', 'email', 'phone', 'address', 'subject', 'message'])
        },
        async postForm(){
            try{
                this.loading = true
                const {data} = await axios.post('/message', {...this.formData, recaptcha: this.token})
                if(data.success === true){
                    this.toast = true
                    this.timer = setInterval(() => {
                        if(this.toastWidth >= 100){
                            this.toast = false
                            this.toastWidth = 0
    
                            clearInterval(this.timer)
                            return
                        }
                        this.toastWidth += 5
                    }, 150)
                }else{
                    console.log(data)

                }
                this.loading = false
                this.fields = { username: '', email: '', address: '', phone: '', subject: '',message: '',}

            }catch(e){
                if(e.response.status === 422){
                    const {errors} = e.response.data
                    this.errors = {...errors}
                    this.loading = false
                }else {

                }
            }
        }
        ,
        async submitHandler(event){
            event.preventDefault()
            grecaptcha.ready(async() => {
                try{
                    const token  = await grecaptcha.execute('6LdZ_kAlAAAAAJZGLocZYxHJY5J2y4F5j4iRxBfv', {action: 'submit'})
                    this.errors = {}
                const formSchema = object().shape({
                    message: string().required().min(15).label('Le message'),
                    subject: string().required().min(3).max(60).label('Le sujet'),
                    address: string().required().min(3).max(60).label('L\'adresse'),
                    phone: string().required().phone([], 'Veuillez saisir un numéro de téléphone valide').label('Le numéro de téléphone'),
                    email: string().required().email().label('Le email'),
                    username: string().required().min(2).max(60).label('Le nom'),
                })
                this.token = token

                try{
                    await formSchema.validate({...this.formData})
                    await this.postForm()

                }catch(err){
                    this.errors[err.path] = err.errors?.join()
                }
                }catch(e){
                    console.log(e)

                }
                
            })
            
        }
    }))


    Alpine.data('newsletter',() => ({
        email: '',
        submitHandler(e){
            e.preventDefault()
            object().shape(
                {
                    email: string()
                    .required().email()
                    .label('Le email'),})
                    .validate({email: this.email
                    }
            ).then(value => {
                axios.post('/api/v1/subscribe', value)
                .then(result => {
                    this.email = ""
                }).catch(err => {
                    console.log(err.response.data.message)
                })
            }).catch(err => {
                console.log(err)
            })
        }
    }))
})


Alpine.start()
