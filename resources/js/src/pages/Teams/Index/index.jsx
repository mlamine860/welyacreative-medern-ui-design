import { Link, usePage } from "@inertiajs/react";
import React from "react";
import Icon from "../../../shared/Icon";
import Layout from "../../../shared/Layout";
import Pagination from "../../../shared/Pagination";
import SearchFilter from "../../../shared/SearchFilter";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Index() {
    const {
        teams: {
            data,
            meta: { links },
        },
    } = usePage().props;

    return (
        <div>
            <TitleText>Les Teams</TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
                <Link
                    className="btn-indigo focus:outline-none"
                    href={route("teams.create")}
                >
                    <span>Créer</span>
                    <span className="hidden md:inline"> un team</span>
                </Link>
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">#ID</th>
                            <th className="px-6 pt-5 pb-4">NOM</th>
                            <th className="px-6 pt-5 pb-4">Membres</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(({ id, name, users }) => {
                            return (
                                <tr
                                    key={id}
                                    className="hover:bg-gray-100 focus-within:bg-gray-100"
                                >
                                    <td className="border-t">
                                        <Link
                                            href={route("teams.edit", id)}
                                            className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                        >
                                            {id}
                                        </Link>
                                    </td>
                                    <td className="border-t">
                                        <Link
                                            href={route("teams.edit", id)}
                                            className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                        >
                                            {name}
                                        </Link>
                                    </td>
                                    <td className="border-t">
                                        {/* <img
                                            src={avatar}
                                            alt={username}
                                            width="45"
                                            height="45"
                                            className="rounded-full"
                                        /> */}
                                        <div className="flex gap-1 flex-wrap">
                                            {users.data.map(
                                                ({ id, avatar, name }) => (
                                                    <Link
                                                        key={id}
                                                        href={route(
                                                            "users.edit",
                                                            id
                                                        )}
                                                    >
                                                        <img
                                                            src={avatar}
                                                            alt={name}
                                                            width="24"
                                                            height="24"
                                                            className="rounded-full"
                                                        />
                                                    </Link>
                                                )
                                            )}
                                        </div>
                                    </td>
                                    <td className="w-px border-t">
                                        <Link
                                            tabIndex="-1"
                                            href={route("teams.edit", id)}
                                            className="flex items-center px-4 focus:outline-none"
                                        >
                                            <Icon
                                                name="cheveron-right"
                                                className="block w-6 h-6 text-gray-400 fill-current"
                                            />
                                        </Link>
                                    </td>
                                </tr>
                            );
                        })}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="6">
                                    Aucun team pour le momment.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

Index.layout = (page) => <Layout title="Team" children={page} />;
export default Index;
