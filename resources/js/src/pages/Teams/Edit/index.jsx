import { Link, router, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import AlertConfirm from "../../../shared/AlertConfirm";
import DeleteButton from "../../../shared/DeleteButton";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route, toOptionSelect } from "../../../utils";
import Select from "react-select";

function Edit() {
    const [show, setShow] = useState();
    const { team, users } = usePage().props;
    const { data, setData, errors, put, processing } = useForm({
        name: team.name,
        description: team.description || "",
        users: team.users?.data.map(toOptionSelect) || [],
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        put(route("teams.update", team.id));
    };
    const destroy = () => {
        router.delete(route("teams.destroy", team.id));
    };
    return (
        <>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer ce team ?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("teams.index")}
                >
                    Teams /
                </Link>{" "}
                Modification
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <TextInput
                            className="w-full pb-8 pr-6"
                            label="Nom"
                            name="rname"
                            errors={errors.name}
                            value={data.name}
                            onChange={(e) => setData("name", e.target.value)}
                        />
                        <TextAreaInput
                            name="description"
                            label="Description"
                            className="w-full pb-8 pr-6"
                            errors={errors.description}
                            value={data.description}
                            onChange={(e) =>
                                setData("description", e.target.value)
                            }
                        />
                    </div>
                    <div className="flex flex-col gap-2 pb-12">
                        <label htmlFor="">Membres</label>
                        <Select
                            placeholder="Teams"
                            className="w-full"
                            isMulti
                            value={data.users}
                            onChange={(teams) => setData("users", teams)}
                            options={users.data.map(toOptionSelect)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}
Edit.layout = (page) => <Layout title="Team" children={page} />;
export default Edit;
