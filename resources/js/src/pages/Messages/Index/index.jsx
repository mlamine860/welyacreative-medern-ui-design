import { Link, usePage } from "@inertiajs/react";
import React from "react";
import SearchFilter from "../../../shared/SearchFilter";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";
import Icon from "../../../shared/Icon";
import Layout from "../../../shared/Layout";
import Pagination from "../../../shared/Pagination";

function Messages() {
    const {
        messages: { data, meta: {links} },
    } = usePage().props;
    return (
        <div>
            <TitleText>Messages</TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">NOM</th>
                            <th className="px-6 pt-5 pb-4">SUJET</th>
                            <th className="px-6 pt-5 pb-4">TELEPHONE</th>
                            <th className="px-6 pt-5 pb-4">EMAIL</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(
                            ({
                                id,
                                username,
                                subject,
                                email,
                                phone,
                                address,
                            }) => {
                                return (
                                    <tr
                                        key={id}
                                        className="hover:bg-gray-100 focus-within:bg-gray-100"
                                    >
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "messages.show",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {username}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "messages.show",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {subject}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "messages.show",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {phone}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "messages.show",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {address}
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route(
                                                    "messages.show",
                                                    id
                                                )}
                                                className="flex items-center px-4 focus:outline-none"
                                            >
                                                <Icon
                                                    name="cheveron-right"
                                                    className="block w-6 h-6 text-gray-400 fill-current"
                                                />
                                            </Link>
                                        </td>
                                    </tr>
                                );
                            }
                        )}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="6">
                                    Aucun message pour le momment.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

Messages.layout = (page) => <Layout children={page} title="Message" />;

export default Messages;
