import { Link, usePage, router } from "@inertiajs/react";
import React, { useState } from "react";
import { AiOutlineMail, AiOutlinePhone } from "react-icons/ai";
import { FiMap } from "react-icons/fi";
import AlertConfirm from "../../../shared/AlertConfirm";
import DeleteButton from "../../../shared/DeleteButton";
import Layout from "../../../shared/Layout";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Show() {
    const { message } = usePage().props;
    const [show, setShow] = useState();
    const destroy = () => {
        setShow(false);
        router.delete(route("messages.destroy", message.id));
    };
    return (
        <div>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer ce message?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("messages.index")}
                >
                    Messages/
                </Link>{" "}
                {message.subject}
            </TitleText>
            <div className="p-8 shadow rounded bg-white">
                <div className="flex flex-col">
                    <div className="font-bold text-2xl mb-4">
                        {message.username}
                    </div>
                    <div className="text-lg mb-4 flex gap-1 items-center text-primary">
                        <AiOutlineMail />
                        <a href={`mailto:{message.email}`}>{message.email}</a>
                    </div>
                    <div className="text-lg mb-4 flex gap-1 items-center text-primary">
                        <AiOutlinePhone />
                        <a href={`tel:{message.phone}`}>{message.phone}</a>
                    </div>
                    <div className="text-lg mb-4 flex gap-1 items-center">
                        <FiMap />
                        {message.address}
                    </div>
                    <p className="py-8 text-md">{message.message}</p>
                    <div className="flex justify-end">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                    </div>
                </div>
            </div>
        </div>
    );
}

Show.layout = (page) => <Layout children={page} title="Message" />;
export default Show;
