import { router, usePage } from "@inertiajs/react";
import React from "react";
import { AiFillCheckCircle, AiFillCloseCircle } from "react-icons/ai";
import Icon from "../../../shared/Icon";
import Layout from "../../../shared/Layout";
import Pagination from "../../../shared/Pagination";
import SearchFilter from "../../../shared/SearchFilter";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Index() {
    const {
        newsletters: {
            data,
            meta: { links },
        },
    } = usePage().props;
    const handleDelete = (email) => {
        router.delete(route("newsletters.destroy", email));
    };
    return (
        <div>
            <TitleText>Toutes les souscriptions </TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">#ID</th>
                            <th className="px-6 pt-5 pb-4">email</th>
                            <th className="px-6 pt-5 pb-4">SOUSCRIT</th>
                            <th className="px-6 pt-5 pb-4">ACTIONS</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(({ id, email, subscribe }) => {
                            return (
                                <tr
                                    key={id}
                                    className="hover:bg-gray-100 focus-within:bg-gray-100"
                                >
                                    <td className="border-t">
                                        <span className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none">
                                            {id}
                                        </span>
                                    </td>
                                    <td className="border-t">
                                        <span className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none">
                                            {email}
                                        </span>
                                    </td>
                                    <td className="border-t">
                                        <span className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none">
                                            {subscribe ? (
                                                <AiFillCheckCircle className="text-green-600" />
                                            ) : (
                                                <AiFillCloseCircle className="text-yellow-600" />
                                            )}
                                        </span>
                                    </td>
                                    <td className="w-px border-t">
                                        <button
                                            onClick={() => handleDelete(email)}
                                            className="flex items-center px-4 focus:outline-none"
                                        >
                                            <Icon
                                                name="delete"
                                                className="block w-6 h-6  fill-current text-red-600"
                                            />
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="6">
                                    Aucune subscription trouvé.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

Index.layout = (page) => (
    <Layout children={page} title="Toutes les souscriptions" />
);

export default Index;
