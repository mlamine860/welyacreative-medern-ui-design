import { Link, router, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import AlertConfirm from "../../../shared/AlertConfirm";
import DeleteButton from "../../../shared/DeleteButton";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Edit() {
    const [show, setShow] = useState();
    const { category } = usePage().props;
    const { data, setData, put, errors, processing } = useForm({
        name: category.name,
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        put(route("categories.update", category.id));
    };

    const destroy = () => {
        setShow(false);
        router.delete(route("categories.destroy", category.id));
    };

    return (
        <div>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer cette catégorie ?</p>
            </AlertConfirm>
            <TitleText></TitleText>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("categories.index")}
                >
                    Catégories /{" "}
                </Link>
                {category.name}
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <TextInput
                        name="name"
                        label="Nom"
                        value={data.name}
                        onChange={(e) => setData("name", e.target.value)}
                        errors={errors.name}
                        className="w-full pb-8 pr-6"
                    />
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Edit.layout = (page) => <Layout children={page} title="Category" />;
export default Edit;
