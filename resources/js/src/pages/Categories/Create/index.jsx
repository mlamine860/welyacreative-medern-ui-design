import { Link, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Create() {
    const { data, setData, errors, post, processing } = useForm({
        name: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("categories.store"));
    };
    return (
        <div>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("categories.index")}
                >
                    Catégories /{" "}
                </Link>
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <TextInput
                        name="name"
                        label="Nom"
                        value={data.name}
                        onChange={e => setData('name', e.target.value)}
                        errors={errors.name}
                        className="w-full pb-8 pr-6"
                    />
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Create.layout = (page) => <Layout children={page} title="Category" />;
export default Create;
