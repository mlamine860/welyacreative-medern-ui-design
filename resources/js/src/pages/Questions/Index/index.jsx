import { Link, usePage } from "@inertiajs/react";
import React from "react";
import Icon from "../../../shared/Icon";
import Layout from "../../../shared/Layout";
import Pagination from "../../../shared/Pagination";
import SearchFilter from "../../../shared/SearchFilter";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Question() {
    const {
        questions: {
            data,
            meta: { links },
        },
    } = usePage().props;
    return (
        <div>
            <TitleText>FAQ</TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
                <Link
                    className="btn-indigo focus:outline-none"
                    href={route("questions.create")}
                >
                    <span>Créer</span>
                    <span className="hidden md:inline"> une questions</span>
                </Link>
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">#ID</th>
                            <th className="px-6 pt-5 pb-4">Question</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(({ id, question }) => {
                            return (
                                <tr
                                    key={id}
                                    className="hover:bg-gray-100 focus-within:bg-gray-100"
                                >
                                    <td className="border-t">
                                        <Link
                                            href={route("questions.edit", id)}
                                            className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                        >
                                            {id}
                                        </Link>
                                    </td>
                                    <td className="border-t">
                                        <Link
                                            href={route("questions.edit", id)}
                                            className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                        >
                                            {question}
                                        </Link>
                                    </td>
                                    <td className="w-px border-t">
                                        <Link
                                            tabIndex="-1"
                                            href={route("questions.edit", id)}
                                            className="flex items-center px-4 focus:outline-none"
                                        >
                                            <Icon
                                                name="cheveron-right"
                                                className="block w-6 h-6 text-gray-400 fill-current"
                                            />
                                        </Link>
                                    </td>
                                </tr>
                            );
                        })}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="6">
                                    Aucune question pour le momment.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

Question.layout = (page) => <Layout children={page} title="Questions" />;
export default Question;
