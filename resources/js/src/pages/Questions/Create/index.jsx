import { Link, useForm, usePage} from "@inertiajs/react";
import React from "react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Create() {
    const {} = usePage().props;
    const { data, errors, setData, processing, post } = useForm({
        username: "",
        image: "",
        job: "",
        message: "",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("questions.store"));
    };
    return (
        <>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("questions.index")}
                >
                    FAQ /
                </Link>{" "}
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <TextInput
                            className="w-full pb-8 pr-6"
                            label="Question"
                            name="question"
                            errors={errors.question}
                            value={data.question}
                            onChange={(e) =>
                                setData("question", e.target.value)
                            }
                        />
                        <TextAreaInput
                            name="answer"
                            label="Réponse"
                            className="w-full pb-8 pr-6"
                            errors={errors.answer}
                            value={data.answer}
                            onChange={(e) => setData("answer", e.target.value)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}

Create.layout = (page) => <Layout children={page} title="Question" />;
export default Create;
