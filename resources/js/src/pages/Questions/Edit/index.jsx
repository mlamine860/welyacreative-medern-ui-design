import { Link, router, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import AlertConfirm from "../../../shared/AlertConfirm";
import DeleteButton from "../../../shared/DeleteButton";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Edit() {
    const { question } = usePage().props;
    const { data, errors, setData, processing, put } = useForm({
        question: question.question || "",
        answer: question.answer || "",
    });
    const [show, setShow] = useState();
    const destroy = () => {
        router.delete(route("questions.destroy", question.id));
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        put(route("questions.update", question.id));
    };
    return (
        <>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer cette question ?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("questions.index")}
                >
                    FAQ /
                </Link>{" "}
                {question.question}
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <TextInput
                            className="w-full pb-8 pr-6"
                            label="Question"
                            name="question"
                            errors={errors.question}
                            value={data.question}
                            onChange={(e) =>
                                setData("question", e.target.value)
                            }
                        />
                        <TextAreaInput
                            name="answer"
                            label="Réponse"
                            className="w-full pb-8 pr-6"
                            errors={errors.answer}
                            value={data.answer}
                            onChange={(e) => setData("answer", e.target.value)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}

Edit.layout = (page) => <Layout children={page} title="Question" />;
export default Edit;
