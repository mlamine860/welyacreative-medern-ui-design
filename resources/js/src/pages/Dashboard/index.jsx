import { Link, usePage } from "@inertiajs/react";
import moment from "moment";
import React from "react";
import Badge from "../../shared/Badge";
import Icon from "../../shared/Icon";
import Layout from "../../shared/Layout";
import Logo from "../../shared/Logo";
import SearchFilter from "../../shared/SearchFilter";
import TitleText from "../../shared/TitleText";
import { route } from "../../utils";

export function Dasboard() {
    const {
        auth: { user },
        dashboardData,
    } = usePage().props;
    const { site, data, latestPosts } = dashboardData;
    return (
        <div>
            <TitleText>Admistration</TitleText>
            <div className="flex flex-col md:flex-row gap-8">
                <div className="shadow-xl rounded-xl p-4 bg-white md:w-1/2">
                    <div className="flex item-center gap-4">
                        <img
                            src={user.avatar}
                            alt={user.name}
                            className="rounded-full"
                            width={80}
                            height={60}
                        />
                        <div className="pl-4">
                            {user.profile ? (
                                <h2 className="text-xl">
                                    Bienvenue, {user.profile.first_name}{" "}
                                    {user.profile.last_name}
                                </h2>
                            ) : (
                                <h2 className="text-xl">
                                    Bienvenue, {user.name}
                                </h2>
                            )}
                            <p className="text-xs">{user.profile?.role}</p>
                            <div className="mt-2">
                                <Link href="">Déconnexion</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="shadow-xl rounded-xl p-4 bg-white md:w-1/2">
                    <div className="flex flex-col md:flex-row gap-4">
                        <Link href={route("site.edit")}>
                            <Logo/>
                        </Link>
                        <div className="flex-1">
                            <h2 className="text-xl">{site.name}</h2>
                            <div className="flex flex-col gap-1">
                                <div>
                                    URL:{"  "}
                                    <a
                                        className="text-sm text-primary"
                                        href={site.url}
                                    >
                                        {site.url}
                                    </a>
                                </div>
                                <div>
                                    ADRESSE E-MAIL:{"  "}
                                    <a
                                        className="text-sm text-primary"
                                        href={`mailto:${site.email}`}
                                    >
                                        {site.email}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4 py-8">
                {data.map(({ label, count, icon, url }) => (
                    <Link
                        href={url}
                        className="shadow-xl rounded-xl bg-white p-4 flex justify-between items-center"
                        key={label}
                    >
                        <div className="text-xl text-gray-600">
                            <span className="flex gap-1 items-center">
                                <Icon name={icon} className="w-5 h-5" />
                                <span className="text-lg font-thin">
                                    {label}
                                </span>
                            </span>
                        </div>
                        <div
                            className="text-lg font-bold bg-primary 
                        w-8 h-8 p-1 text-white  rounded-full flex items-center justify-center"
                        >
                            {count}
                        </div>
                    </Link>
                ))}
            </div>
            <div className="p-4 bg-white rounded-xl shadow-lg">
                <h3 className="text-xl font-medium mb-4">
                    Les derniers articles
                </h3>
                <div className="flex justify-end bg-gray-100 py-6">
                    <SearchFilter />
                </div>
                <div className="overflow-x-auto bg-white rounded shadow">
                    <table className="w-full whitespace-nowrap">
                        <thead>
                            <tr className="font-bold text-left">
                                <th className="px-6 pt-5 pb-4">#ID</th>
                                <th className="px-6 pt-5 pb-4">TITRE</th>
                                <th className="px-6 pt-5 pb-4">AUTEUR</th>
                                <th className="px-6 pt-5 pb-4">CATEGORIE</th>
                                <th className="px-6 pt-5 pb-4">STATUS</th>
                                <th className="px-6 pt-5 pb-4">PUBLIE</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {latestPosts.data.map(
                                ({
                                    id,
                                    title,
                                    author,
                                    published,
                                    created_at,
                                    category,
                                }) => {
                                    return (
                                        <tr
                                            key={id}
                                            className="hover:bg-gray-100 focus-within:bg-gray-100"
                                        >
                                            <td className="border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                                >
                                                    {id}
                                                </Link>
                                            </td>
                                            <td className="border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                                >
                                                    {title}
                                                </Link>
                                            </td>
                                            <td className="border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                                >
                                                    <img
                                                        src={author.avatar}
                                                        alt={author.name}
                                                        width="32"
                                                        height="32"
                                                        className="rounded-full"
                                                    />
                                                </Link>
                                            </td>
                                            <td className="border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                                >
                                                    {category.name}
                                                </Link>
                                            </td>
                                            <td className="border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                                >
                                                    <Badge
                                                        type={
                                                            published
                                                                ? "success"
                                                                : "warning"
                                                        }
                                                        label={
                                                            published
                                                                ? "Publié"
                                                                : "Non publié"
                                                        }
                                                    />
                                                </Link>
                                            </td>
                                            <td className="border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                                >
                                                    {moment(
                                                        created_at
                                                    ).fromNow()}
                                                </Link>
                                            </td>
                                            <td className="w-px border-t">
                                                <Link
                                                    href={route(
                                                        "posts.edit",
                                                        id
                                                    )}
                                                    className="flex items-center px-4 focus:outline-none"
                                                >
                                                    <Icon
                                                        name="cheveron-right"
                                                        className="block w-6 h-6 text-gray-400 fill-current"
                                                    />
                                                </Link>
                                            </td>
                                        </tr>
                                    );
                                }
                            )}
                            {data.length === 0 && (
                                <tr>
                                    <td
                                        className="px-6 py-4 border-t"
                                        colSpan="6"
                                    >
                                        Aucune question trouvé.
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

Dasboard.layout = (page) => <Layout children={page} title="Dashboard" />;
export default Dasboard;
