import { Link, router, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import DeleteButton from "../../../shared/DeleteButton";
import AlertConfirm from "../../../shared/AlertConfirm";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route, toOptionSelect } from "../../../utils";
import ReactSelect from "react-select";

function Edit() {
    const { tag, projects } = usePage().props;
    const { data, setData, errors, put, processing } = useForm({
        name: tag.name,
        projects: tag.projects?.data.map(toOptionSelect),
    });
    const [show, setShow] = useState();
    const handleSubmit = (e) => {
        e.preventDefault();
        put(route("tags.update", tag.id));
    };
    const destroy = () => {
        router.delete(route("tags.destroy", tag.id));
    };
    return (
        <>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer ce tag ?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("teams.index")}
                >
                    Tags /
                </Link>{" "}
                {tag.name}
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <TextInput
                            className="w-full pb-8 pr-6"
                            label="Nom"
                            name="rname"
                            errors={errors.name}
                            value={data.name}
                            onChange={(e) => setData("name", e.target.value)}
                        />
                    </div>
                    <div className="flex flex-col gap-2 mb-8">
                        <label htmlFor="">Associer à des projets</label>
                        <ReactSelect
                            placeholder="Projects"
                            className="w-full"
                            isMulti
                            value={data.projects}
                            onChange={(projects) =>
                                setData("projects", projects)
                            }
                            options={projects.data.map(toOptionSelect)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}
Edit.layout = (page) => <Layout title="Tag Modification" children={page} />;
export default Edit;
