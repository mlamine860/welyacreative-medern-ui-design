import { Link, useForm } from "@inertiajs/react";
import React from "react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Create() {
    const { data, setData, errors, post, processing } = useForm({
        name: "",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("tags.store"));
    };
    return (
        <>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("teams.index")}
                >
                    Tags /
                </Link>{" "}
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <TextInput
                            className="w-full pb-8 pr-6"
                            label="Nom"
                            name="rname"
                            errors={errors.name}
                            value={data.name}
                            onChange={(e) => setData("name", e.target.value)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}
Create.layout = (page) => <Layout title="Team" children={page} />;
export default Create;
