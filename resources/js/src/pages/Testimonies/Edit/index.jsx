import { Link, useForm, usePage, router } from "@inertiajs/react";
import React, { useState } from "react";
import { Helmet } from "react-helmet";
import AlertConfirm from "../../../shared/AlertConfirm";
import DeleteButton from "../../../shared/DeleteButton";
import FileInput from "../../../shared/FileInput";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Edit() {
    const { testimony } = usePage().props;
    const { data, errors, setData, processing, post } = useForm({
        username: testimony.username || "",
        image: "",
        job: testimony.job || "",
        message: testimony.message || "",
        _method: "PUT",
    });
    const [show, setShow] = useState();
    const destroy = () => {
        router.delete(route("testimonies.destroy", testimony.id));
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("testimonies.update", testimony.id));
    };
    return (
        <>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer ce témoignage ?</p>
            </AlertConfirm>
            <Helmet title={`${testimony.username}`} />
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("testimonies.index")}
                >
                    Témoignages /
                </Link>{" "}
                {testimony.username}
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <div className="flex flex-col md:flex-row justify-between items-center">
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Nom"
                                name="username"
                                errors={errors.username}
                                value={data.username}
                                onChange={(e) =>
                                    setData("username", e.target.value)
                                }
                            />
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Job"
                                name="job"
                                errors={errors.job}
                                value={data.job}
                                onChange={(e) => setData("job", e.target.value)}
                            />
                        </div>
                        <FileInput
                            label="Image"
                            accept="image/*"
                            name="image"
                            errors={errors.image}
                            value={data.image}
                            onChange={(image) => setData("image", image)}
                            className="pb-8"
                        />
                        <TextAreaInput
                            name="message"
                            label="Message"
                            className="w-full pb-8 pr-6"
                            errors={errors.message}
                            value={data.message}
                            onChange={(e) => setData("message", e.target.value)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}

Edit.layout = (page) => <Layout children={page} title="Témoignage" />;
export default Edit;
