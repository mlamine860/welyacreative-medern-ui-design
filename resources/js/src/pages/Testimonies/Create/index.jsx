import { Link, useForm, usePage } from "@inertiajs/react";
import FileInput from "../../../shared/FileInput";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Create() {
    const {} = usePage().props;
    const { data, errors, setData, processing, post } = useForm({
        username: "",
        image: "",
        job: "",
        message: "",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("testimonies.store"));
    };
    return (
        <>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("testimonies.index")}
                >
                    Témoignages /
                </Link>{" "}
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <div className="flex flex-col md:flex-row justift-beween items-center">
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Nom"
                                name="username"
                                errors={errors.username}
                                value={data.username}
                                onChange={(e) =>
                                    setData("username", e.target.value)
                                }
                            />
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Job"
                                name="job"
                                errors={errors.job}
                                value={data.job}
                                onChange={(e) => setData("job", e.target.value)}
                            />
                        </div>
                        <FileInput
                            label="Image"
                            accept="image/*"
                            name="image"
                            errors={errors.image}
                            value={data.image}
                            onChange={(image) => setData("image", image)}
                            className="pb-8"
                        />
                        <TextAreaInput
                            name="message"
                            label="Message"
                            className="w-full pb-8 pr-6"
                            errors={errors.message}
                            value={data.message}
                            onChange={(e) => setData("message", e.target.value)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}

Create.layout = (page) => <Layout children={page} title="Témoignage" />;
export default Create;
