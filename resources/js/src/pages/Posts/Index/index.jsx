import { Link, usePage } from "@inertiajs/react";
import React from "react";
import Layout from "../../../shared/Layout";
import TitleText from "../../../shared/TitleText";
import SearchFilter from "../../../shared/SearchFilter";
import { route, moment } from "../../../utils";
import Icon from "../../../shared/Icon";
import Badge from "../../../shared/Badge";
import Pagination from "../../../shared/Pagination";

function Index() {
    const {
        posts: {
            data,
            meta: { links },
        },
    } = usePage().props;
    return (
        <div>
            <TitleText>Tous les articles</TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
                <Link
                    className="btn-indigo focus:outline-none"
                    href={route("posts.create")}
                >
                    <span>Créer</span>
                    <span className="hidden md:inline"> un article</span>
                </Link>
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">#ID</th>
                            <th className="px-6 pt-5 pb-4">TITRE</th>
                            <th className="px-6 pt-5 pb-4">AUTEUR</th>
                            <th className="px-6 pt-5 pb-4">CATEGORIE</th>
                            <th className="px-6 pt-5 pb-4">STATUS</th>
                            <th className="px-6 pt-5 pb-4">PUBLIE</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(
                            ({
                                id,
                                title,
                                author,
                                published,
                                created_at,
                                category,
                            }) => {
                                return (
                                    <tr
                                        key={id}
                                        className="hover:bg-gray-100 focus-within:bg-gray-100"
                                    >
                                        <td className="border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {id}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {title}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                <img
                                                    src={author.avatar}
                                                    alt={author.name}
                                                    width="32"
                                                    height="32"
                                                    className="rounded-full"
                                                />
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {category.name}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                <Badge
                                                    type={
                                                        published
                                                            ? "success"
                                                            : "warning"
                                                    }
                                                    label={
                                                        published
                                                            ? "Publié"
                                                            : "Non publié"
                                                    }
                                                />
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {moment(created_at).fromNow()}
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                href={route("posts.edit", id)}
                                                className="flex items-center px-4 focus:outline-none"
                                            >
                                                <Icon
                                                    name="cheveron-right"
                                                    className="block w-6 h-6 text-gray-400 fill-current"
                                                />
                                            </Link>
                                        </td>
                                    </tr>
                                );
                            }
                        )}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="6">
                                    Aucune question trouvé.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

Index.layout = (page) => <Layout children={page} title="Articles" />;
export default Index;
