import { Link, useForm, usePage } from "@inertiajs/react";
import React from "react";
import Layout from "../../../shared/Layout";
import TitleText from "../../../shared/TitleText";
import TextInput from "../../../shared/TextInput";
import LoadingButton from "../../../shared/LoadingButton";
import { imageUploadHandler, route } from "../../../utils";
import FileInput from "../../../shared/FileInput";
import TinyMCE from "../../../shared/TinyMCE";
import SelectInput from "../../../shared/SelectInput";
import TextAreaInput from "../../../shared/TextAreaInput";

function Edit() {
    const { categories } = usePage().props;
    const { data, setData, errors, post, processing } = useForm({
        title: "",
        image: "",
        content: "",
        category_id: "",
        excerpt: "",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("posts.store"));
    };
    return (
        <div>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("posts.index")}
                >
                    Articles /{" "}
                </Link>
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-4 md:px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-col md:flex-row justify-center">
                        <TextInput
                            name="title"
                            label="Titre"
                            value={data.title}
                            onChange={(e) => setData("title", e.target.value)}
                            errors={errors.title}
                            className="w-full pb-4 md:pr-6"
                        />
                        <TextInput
                            name="slug"
                            label="Slug"
                            value={data.slug}
                            onChange={(e) => setData("slug", e.target.value)}
                            errors={errors.slug}
                            className="w-full pb-4 md:pr-6"
                        />
                    </div>
                    <TextAreaInput
                        label="Extrait"
                        value={data.excerpt}
                        name="excerpt"
                        errors={errors.excerpt}
                        onChange={(e) => setData("excerpt", e.target.value)}
                        className="w-full pb-4 md:pr-6"
                    />
                    <TinyMCE
                        errors={errors.content}
                        value={data.content}
                        label="Contenu"
                        onChange={(content) => setData("content", content)}
                        className="w-full pb-6 md:pr-6"
                        images_upload_handler={imageUploadHandler}
                    />
                    <div className="flex flex-col md:flex-row justify-between mb-6">
                        <SelectInput
                            errors={errors.category_id}
                            name="category_id"
                            label="Category"
                            value={data.category_id}
                            onChange={(e) =>
                                setData("category_id", e.target.value)
                            }
                        >
                            <option>Selectionner une catégorie</option>
                            {categories.map((cat) => (
                                <option key={cat.id} value={cat.id}>
                                    {cat.name}
                                </option>
                            ))}
                        </SelectInput>
                        <FileInput
                            label="Image"
                            accept="image/*"
                            name="icon"
                            className="pb-6 md pr-6"
                            errors={errors.image}
                            value={data.image}
                            onChange={(image) => setData("image", image)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Edit.layout = (page) => <Layout children={page} title="Article" />;
export default Edit;
