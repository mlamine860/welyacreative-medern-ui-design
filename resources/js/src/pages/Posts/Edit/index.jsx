import { Link, router, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import AlertConfirm from "../../../shared/AlertConfirm";
import DeleteButton from "../../../shared/DeleteButton";
import FileInput from "../../../shared/FileInput";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import SelectInput from "../../../shared/SelectInput";
import TextInput from "../../../shared/TextInput";
import TinyMCE from "../../../shared/TinyMCE";
import TitleText from "../../../shared/TitleText";
import CheckboxInput from "../../../shared/CheckboxInput";
import { imageUploadHandler, route } from "../../../utils";
import TextAreaInput from "../../../shared/TextAreaInput";

function Edit() {
    const [show, setShow] = useState();
    const { post, categories } = usePage().props;
    const {
        data,
        setData,
        errors,
        post: postMethod,
        processing,
    } = useForm({
        title: post.title || "",
        slug: post.slug || "",
        content: post.content || "",
        category_id: post.category_id || "",
        image: "",
        published: post.published,
        excerpt: post.excerpt,
        _method: "PUT",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        postMethod(route("posts.update", post.id));
    };
    const destroy = () => {
        router.delete(route("posts.destroy", post.id));
    };
    return (
        <div>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer cet artcle ?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("posts.index")}
                >
                    Articles /{" "}
                </Link>
                Modification
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-4 md:px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-col md:flex-row justify-center">
                        <TextInput
                            name="title"
                            label="Titre"
                            value={data.title}
                            onChange={(e) => setData("title", e.target.value)}
                            errors={errors.title}
                            className="w-full pb-4 md:pr-6"
                        />
                        <TextInput
                            name="slug"
                            label="Slug"
                            value={data.slug}
                            onChange={(e) => setData("slug", e.target.value)}
                            errors={errors.slug}
                            className="w-full pb-4 md:pr-6"
                        />
                    </div>
                    <TextAreaInput
                        label="Extrait"
                        value={data.excerpt}
                        name="excerpt"
                        errors={errors.excerpt}
                        onChange={(e) => setData("excerpt", e.target.value)}
                        className="w-full pb-4 md:pr-6"
                    />

                    <TinyMCE
                        value={data.content}
                        errors={errors.content}
                        label="Contenu"
                        onChange={(content) => setData("content", content)}
                        className="w-full pb-6 md:pr-6"
                        images_upload_handler={imageUploadHandler}
                    />
                    <div className="flex flex-col md:flex-row justify-between mb-6">
                        <SelectInput
                            errors={errors.category_id}
                            name="category_id"
                            label="Category"
                            value={data.category_id}
                            onChange={(e) =>
                                setData("category_id", e.target.value)
                            }
                        >
                            <option>Selectionner une catégorie</option>
                            {categories.map((cat) => (
                                <option key={cat.id} value={cat.id}>
                                    {cat.name}
                                </option>
                            ))}
                        </SelectInput>

                        <FileInput
                            label="Image"
                            accept="image/*"
                            name="icon"
                            errors={errors.image}
                            value={data.image}
                            onChange={(image) => setData("image", image)}
                        />
                    </div>
                    <div className="mb-4">
                        <CheckboxInput
                            label="Publié"
                            name="published"
                            value={data.published}
                            onChange={(published) =>
                                setData("published", published)
                            }
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Edit.layout = (page) => <Layout children={page} title="Article" />;
export default Edit;
