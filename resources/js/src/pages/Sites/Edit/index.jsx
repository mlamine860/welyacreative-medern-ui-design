import { useForm, usePage } from "@inertiajs/react";
import React from "react";
import FileInput from "../../../shared/FileInput";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

function Site() {
    const { site } = usePage().props;
    const { data, errors, setData, processing, post } = useForm({
        name: site.name || "",
        email: site.email || "",
        description: site.description || "",
        tagline: site.tagline || "",
        address: site.address || "",
        phone: site.phone || "",
        url: site.url || "",
        image: "",
        logo: "",
        _method: "PUT",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("site.update"));
    };
    return (
        <>
            <TitleText>Paramètres du site</TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <div className="-mb-8 -mr-6 pb-8">
                        <div className="flex flex-col md:flex-row justify-between items-center">
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Nom du site"
                                name="name"
                                errors={errors.name}
                                value={data.name}
                                onChange={(e) =>
                                    setData("name", e.target.value)
                                }
                            />
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Email"
                                name="email"
                                errors={errors.email}
                                type="email"
                                value={data.email}
                                onChange={(e) =>
                                    setData("email", e.target.value)
                                }
                            />
                        </div>
                       
                        <div className="flex flex-col md:flex-row justify-between items-center">
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="URL"
                                name="url"
                                type="url"
                                errors={errors.url}
                                value={data.url}
                                onChange={(e) => setData("url", e.target.value)}
                            />
                            <TextInput
                                className="w-full pb-8 pr-6"
                                label="Adresse"
                                name="address"
                                errors={errors.address}
                                value={data.address}
                                onChange={(e) =>
                                    setData("address", e.target.value)
                                }
                            />
                        </div>
                        <div>
                        <TextInput
                                className="w-full pb-8 pr-6"
                                label="Téléphones"
                                name="phone"
                                errors={errors.phone}
                                value={data.phone}
                                onChange={(e) =>
                                    setData("phone", e.target.value)
                                }
                            />
                        </div>
                        <div className="flex flex-col md:flex-row justify-between pb-8 pr-8">
                            <FileInput
                                label="Logo"
                                accept="image/svg"
                                name="logo"
                                errors={errors.logo}
                                value={data.logo}
                                onChange={(logo) => setData("logo", logo)}
                                className="pb-8"
                            />
                            <FileInput
                                label="Image"
                                accept="image/*"
                                name="image"
                                errors={errors.image}
                                value={data.image}
                                onChange={(image) => setData("image", image)}
                            />
                        </div>
                        <TextAreaInput
                            name="tagline"
                            label="Tagline"
                            className="w-full pb-8 pr-6"
                            errors={errors.tagline}
                            value={data.tagline}
                            onChange={(e) => setData("tagline", e.target.value)}
                        />
                        <TextAreaInput
                            name="description"
                            label="Description"
                            className="w-full pb-8 pr-6"
                            errors={errors.description}
                            value={data.description}
                            onChange={(e) =>
                                setData("description", e.target.value)
                            }
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}

Site.layout = (page) => <Layout children={page} title="Site" />;
export default Site;
