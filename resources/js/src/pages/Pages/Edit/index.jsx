import { Link, useForm, usePage, router } from "@inertiajs/react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { imageUploadHandler, route } from "../../../utils";
import React, { useState } from "react";
import TinyMCE from "../../../shared/TinyMCE";
import DeleteButton from "../../../shared/DeleteButton";
import AlertConfirm from "../../../shared/AlertConfirm";

function Edit() {
    const { page } = usePage().props;
    const { data, setData, errors, put, processing } = useForm({
        title: page.title || "",
        content: page.content || "",
    });
    const [show, setShow] = useState();

    const handleSubmit = (e) => {
        e.preventDefault();
        put(route("pages.update", page.id));
    };
    const destroy = () => {
        setShow(false);
        router.delete(route("pages.destroy", page.id));
    };

    return (
        <div>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sur de vouloir supprimer cette question ?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("pages.index")}
                >
                    Pages /
                </Link>{" "}
                {page.title}
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <TextInput
                        name="title"
                        label="Le titre"
                        value={data.title}
                        onChange={(e) => setData("title", e.target.value)}
                        errors={errors.title}
                        className="w-full pb-8 pr-6"
                    />
                    <TinyMCE
                        label="Contenu"
                        errors={errors.content}
                        value={data.content}
                        onChange={(content) => setData("content", content)}
                        className="w-full pb-8 pr-6"
                        images_upload_handler={imageUploadHandler}
                    />
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jours
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Edit.layout = (page) => <Layout children={page} title="Page" />;
export default Edit;
