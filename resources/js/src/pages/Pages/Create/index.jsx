import { Link, useForm } from "@inertiajs/react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import { imageUploadHandler, route } from "../../../utils";
import React from "react";
import TinyMCE from "../../../shared/TinyMCE";

function Create() {
    const { data, setData, errors, post, processing } = useForm({
        title: "",
        content: "",
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("pages.store"));
    };
    return (
        <div>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("pages.index")}
                >
                    Pages /
                </Link>
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <TextInput
                        name="title"
                        label="Le titre"
                        value={data.title}
                        onChange={(e) => setData("title", e.target.value)}
                        errors={errors.title}
                        className="w-full pb-8 pr-6"
                    />
                    <TinyMCE
                        value={data.content}
                        onChange={(content) => setData("content", content)}
                        className="w-full pb-8 pr-6"
                        images_upload_handler={imageUploadHandler}
                    />
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Create.layout = (page) => <Layout children={page} title="Page" />;
export default Create;
