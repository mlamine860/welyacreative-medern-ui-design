import { usePage } from "@inertiajs/react";
import ProgressBar from "@ramonak/react-progress-bar";
import React, { useState } from "react";
import Layout from "../../shared/Layout";
import TitleText from "../../shared/TitleText";
import Icon from "../../shared/Icon";
import axios from "axios";
import { route } from "../../utils";

const Task = ({ name, url, icon, count, orphans }) => {
    const [currentCount, setCurrentCount] = useState(count);
    const [currentOrphans, setCurrentOrphans] = useState(orphans);
    const [spin, setSpin] = useState(false);
    const [orphansSpin, setOrphansSpin] = useState(false);
    const processTask = () => {
        setSpin(true);
        axios
            .post(url, {})
            .then(({ data }) => {
                setCurrentCount(data.count);
                setSpin(false);
            })
            .catch((err) => {
                setSpin(false);
            });
    };
    const dropOrphans = () => {
        setOrphansSpin(true);
        axios
            .post(route("tasks.process", { name: "orphans", type: name }))
            .then(({ data }) => {
                setCurrentOrphans(0)
                setOrphansSpin(false);
            })
            .catch((err) => {
                console.log(err.response);
                setOrphansSpin(false);
            });
    };
    return (
        <div className="bg-white shadow-xl rounded p-4">
            <div className="flex justify-between items-center py-4">
                <h3 className="text-xl fount-medium mb-4 flex gap-2 items-center">
                    <Icon name={icon} className="h-8 w-8" />
                    {`Vignette pour les ${name}`}
                </h3>
                {currentCount < 100 && (
                    <Icon
                        onClick={processTask}
                        className={`cursor-pointer h-8 w-8 text-yellow-600 ${
                            spin ? "animate-spin" : ""
                        }`}
                        name="refresh"
                    />
                )}
            </div>
            <ProgressBar completed={currentCount} />
            <div className="flex justify-between items-center gap-4 pt-8">
                {currentOrphans > 0 && (
                    <>
                        <span className="text-md">
                            <span className="text-primary mr-1">{currentOrphans}</span>
                            Images Orphelines
                        </span>
                        {!orphansSpin ? (
                            <button
                                onClick={dropOrphans}
                                className="flex justify-center items-center bg-primary w-6 h-6 rounded-full relative"
                            >
                                <Icon className="text-white" name="delete" />
                            </button>
                        ) : (
                            <Icon
                                className="cursor-pointer h-8 w-8 text-yellow-600 animate-spin"
                                name="refresh"
                            />
                        )}
                    </>
                )}
            </div>
        </div>
    );
};
function Tasks() {
    const { tasks } = usePage().props;
    return (
        <div>
            <TitleText>Géstion des tâches</TitleText>
            <div className="grid grid-cols-1 gap-8">
                {tasks.map((task) => (
                    <Task
                        name={task.name}
                        icon={task.icon}
                        url={task.url}
                        count={task.count}
                        orphans={task.orphans}
                        key={task.name}
                    />
                ))}
            </div>
            {/* <div className="bg-white shadow-xl rounded p-4 relative">
                <h3 className="text-md fount-medium mb-4">
                    Supprimer les fiechies temporeur
                </h3>
            </div> */}
        </div>
    );
}

Tasks.layout = (page) => <Layout title="Géestion des tâches" children={page} />;
export default Tasks;
