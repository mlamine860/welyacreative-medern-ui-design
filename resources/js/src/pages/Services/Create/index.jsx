import Layout from "../../../shared/Layout";
import TitleText from "../../../shared/TitleText";
import { Link, useForm } from "@inertiajs/react";
import { imageUploadHandler, route } from "../../../utils";
import TextInput from "../../../shared/TextInput";
import TextAreaInput from "../../../shared/TextAreaInput";
import LoadingButton from "../../../shared/LoadingButton";
import FileInput from "../../../shared/FileInput";
import SelectInput from "../../../shared/SelectInput";
import TinyMCE from "../../../shared/TinyMCE";

function Create() {
    const { data, setData, errors, processing, post } = useForm({
        name: "",
        description: "",
        content: "",
        icon: "",
        image: "",
        published: false,
        featured: false,
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("services.store"));
    };
    return (
        <>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("services.index")}
                >
                    Services/
                </Link>{" "}
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-4">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap  -mb-8 -mr-6 pb-8">
                        <TextInput
                            className="w-full pb-8 pr-6"
                            label="Nom"
                            name="name"
                            errors={errors.name}
                            value={data.name}
                            onChange={(e) => setData("name", e.target.value)}
                        />
                        <TextAreaInput
                            name="description"
                            label="Description"
                            className="w-full pb-8 pr-6"
                            errors={errors.description}
                            value={data.description}
                            onChange={(e) =>
                                setData("description", e.target.value)
                            }
                        />

                        <TinyMCE
                            errors={errors.content}
                            label="Contenu"
                            value={data.content}
                            onChange={(content) => setData("content", content)}
                            className="w-full pb-6 md:pr-6"
                            images_upload_handler={imageUploadHandler}
                        />
                    </div>
                    <div className="flex flex-col md:flex-row justify-between pb-8">
                        <FileInput
                            label="Icon"
                            accept="image/svg"
                            name="icon"
                            errors={errors.icon}
                            value={data.icon}
                            onChange={(icon) => setData("icon", icon)}
                        />
                        <FileInput
                            label="Image"
                            accept="image/*"
                            name="image"
                            errors={errors.image}
                            value={data.image}
                            onChange={(image) => setData("image", image)}
                        />
                    </div>
                    <div className="flex flex-col md:flex-row justify-between pb-8">
                        <SelectInput
                            name="featured"
                            label="Mise en avant"
                            value={Number(data.featured)}
                            onChange={({ target }) =>
                                setData("featured", target.value)
                            }
                        >
                            <option></option>
                            <option value={1}>Oui</option>
                            <option value={0}>Non</option>
                        </SelectInput>
                        <SelectInput
                            name="published"
                            label="Publier"
                            value={Number(data.published)}
                            onChange={({ target }) =>
                                setData("published", target.value)
                            }
                        >
                            <option></option>
                            <option value={1}>Oui</option>
                            <option value={0}>Non</option>
                        </SelectInput>
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer un service
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </>
    );
}

Create.layout = (page) => <Layout children={page} title="Creation service" />;
export default Create;
