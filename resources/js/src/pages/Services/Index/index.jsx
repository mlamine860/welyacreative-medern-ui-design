import { Link, usePage } from "@inertiajs/react";
import React from "react";
import { route } from "../../../utils";
import Layout from "../../../shared/Layout";
import SearchFilter from "../../../shared/SearchFilter";
import TitleText from "../../../shared/TitleText";
import Icon from "../../../shared/Icon";
import Pagination from "../../../shared/Pagination";

function ServiceList() {
    const {
        services: {
            data,
            meta: { links },
        },
    } = usePage().props;

    return (
        <div>
            <TitleText>Services</TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
                <Link
                    className="btn-indigo focus:outline-none"
                    href={route("services.create")}
                >
                    <span>Créer</span>
                    <span className="hidden md:inline"> un service</span>
                </Link>
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">#ID</th>
                            <th className="px-6 pt-5 pb-4">NOM</th>
                            <th className="px-6 pt-5 pb-4">IMAGE</th>
                            <th className="px-6 pt-5 pb-4">MISE EN AVANT</th>
                            <th className="px-6 pt-5 pb-4">PUBLIER</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(
                            ({ id, name, icon_url, featured, published }) => {
                                return (
                                    <tr
                                        key={id}
                                        className="hover:bg-gray-100 focus-within:bg-gray-100"
                                    >
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "services.edit",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {id}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "services.edit",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {name}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "services.edit",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                <img
                                                    src={icon_url}
                                                    alt=""
                                                    width={32}
                                                    height={32}
                                                />
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                href={route(
                                                    "services.edit",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {featured ? (
                                                    <Icon
                                                        name="checked"
                                                        className="stroke-green-500"
                                                    />
                                                ) : (
                                                    <Icon
                                                        name="unchecked"
                                                        className="stroke-red-500"
                                                    />
                                                )}
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                href={route(
                                                    "services.edit",
                                                    id
                                                )}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {published ? (
                                                    <Icon
                                                        name="checked"
                                                        className="stroke-green-500"
                                                    />
                                                ) : (
                                                    <Icon
                                                        name="unchecked"
                                                        className="stroke-red-500"
                                                    />
                                                )}
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route(
                                                    "services.edit",
                                                    id
                                                )}
                                                className="flex items-center px-4 focus:outline-none"
                                            >
                                                <Icon
                                                    name="cheveron-right"
                                                    className="block w-6 h-6 text-gray-400 fill-current"
                                                />
                                            </Link>
                                        </td>
                                    </tr>
                                );
                            }
                        )}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="6">
                                    Aucun service trouvé.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

ServiceList.layout = (page) => <Layout children={page} title="Services" />;
export default ServiceList;
