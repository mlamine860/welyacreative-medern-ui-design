import { Link, usePage } from "@inertiajs/react";
import React from "react";
import Icon from "../../../shared/Icon";
import Layout from "../../../shared/Layout";
import Pagination from "../../../shared/Pagination";
import SearchFilter from "../../../shared/SearchFilter";
import TitleText from "../../../shared/TitleText";
import { route } from "../../../utils";

const UserRole = ({ role }) => {
    switch (role) {
        case "admin":
            return <Icon name="admin" className="text-green-600" />;
        case "user":
            return <Icon name="user" className="text-red-600" />;
        case "editor":
            return <Icon name="editor" className="text-yellow-600" />;
    }
};
const UserActive = ({ active }) => {
    switch (active) {
        case true:
            return <Icon name="checked" className="text-green-600 w-6 h-6" />;
        case false:
            return <Icon name="unchecked" className="text-red-600 w-6 h-6" />;
    }
};
function Users() {
    const {
        users: {
            data,
            meta: { links },
        },
    } = usePage().props;
    return (
        <div>
            <TitleText>Les Utilisateurs</TitleText>
            <div className="flex items-center justify-between mb-6">
                <SearchFilter />
                <Link
                    className="btn-indigo focus:outline-none"
                    href={route("users.create")}
                >
                    <span>Créer</span>
                    <span className="hidden md:inline"> un utilisateur</span>
                </Link>
            </div>
            <div className="overflow-x-auto bg-white rounded shadow">
                <table className="w-full whitespace-nowrap">
                    <thead>
                        <tr className="font-bold text-left">
                            <th className="px-6 pt-5 pb-4">NOM</th>
                            <th className="px-6 pt-5 pb-4">IMAGE</th>
                            <th className="px-6 pt-5 pb-4">ACTIVE</th>
                            <th className="px-6 pt-5 pb-4">Role</th>
                            <th className="px-6 pt-5 pb-4">DERNIERE VISITE</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(
                            ({ id, name, avatar, role, active, last_seen }) => {
                                return (
                                    <tr
                                        key={id}
                                        className="hover:bg-gray-100 focus-within:bg-gray-100"
                                    >
                                        <td className="border-t">
                                            <Link
                                                href={route("users.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo-700 focus:outline-none"
                                            >
                                                {name}
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route("users.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                                            >
                                                <img
                                                    src={avatar}
                                                    alt={name}
                                                    width="32"
                                                    height="32"
                                                    className="rounded-full"
                                                />
                                            </Link>
                                        </td>
                                        <td className="border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route("users.edit", id)}
                                                className="flex items-center px-6 py-4 focus:text-indigo focus:outline-none"
                                            >
                                                <UserActive active={active} />
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route("users.edit", id)}
                                                className="flex items-center px-4 focus:outline-none"
                                            >
                                                <UserRole role={role} />
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route("users.edit", id)}
                                                className="flex items-center px-4 focus:outline-none"
                                            >
                                                {last_seen?.substring(0, 10)}
                                            </Link>
                                        </td>
                                        <td className="w-px border-t">
                                            <Link
                                                tabIndex="-1"
                                                href={route("users.edit", id)}
                                                className="flex items-center px-4 focus:outline-none"
                                            >
                                                <Icon
                                                    name="cheveron-right"
                                                    className="block w-6 h-6 text-gray-400 fill-current"
                                                />
                                            </Link>
                                        </td>
                                    </tr>
                                );
                            }
                        )}
                        {data.length === 0 && (
                            <tr>
                                <td className="px-6 py-4 border-t" colSpan="4">
                                    No users found.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <Pagination links={links} />
        </div>
    );
}

Users.layout = (page) => (
    <Layout children={page} title="Tous les utilisateurs" />
);
export default Users;
