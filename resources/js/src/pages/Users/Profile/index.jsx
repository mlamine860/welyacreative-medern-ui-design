import { useForm, usePage, router } from "@inertiajs/react";
import React from "react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import TextAreaInput from "../../../shared/TextAreaInput";
import FileInput from "../../../shared/FileInput";
import { route } from "../../../utils";

function Profile() {
    const {
        auth: { user },
        errors: serverErrors,
        profile,
    } = usePage().props;
    const { data, errors, setData, post, processing } = useForm({
        first_name: user.profile?.first_name || "",
        last_name: user.profile?.last_name || "",
        address: user.profile?.address || "",
        phone: user.profile?.phone || "",
        department: user.profile?.department || "",
        role: user.profile?.role || "",
        languages: user.profile?.languages || "",
        organization: user.profile?.organization || "",
        education: user.profile?.education || "",
        work_history: user.profile?.work_history || "",
        birthday: user.profile?.birthday || "",
        bio: user.profile?.bio || "",
        _method: "PUT",
    });
    const {
        data: passwordData,
        setData: setPasswordData,
        put,
        errors: passwordErrors,
    } = useForm({
        current_password: "",
        password: "",
        password_confirmation: "",
    });

    const handleImageChange = (avatar) => {
        router.post(route("users.upload.avatar"), { avatar, _method: "PUT" });
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("users.profile.update"));
    };
    const handlePasswordSubmit = (e) => {
        e.preventDefault();
        put(route("users.password.update"));
    };
    return (
        <div>
            <TitleText>Profile@{user.name}</TitleText>
            <div className="overflow-hiddenrounded md:p-8 mb-4">
                <div className="flex flex-col md:flex-row gap-4 mb-8 md:items-start">
                    <div className="card bg-white rounded shadow">
                        <div className="flex flex-col p-8">
                            <img
                                src={user.avatar}
                                alt={user.name}
                                width={100}
                                height={100}
                                className="rounded-full"
                            />
                            <p className="text-xl font-bold py-2">
                                {user.name}
                            </p>
                            <p className="font-[500] text-sm text-gray-600 mb-8">
                                Développeur fullstack
                            </p>
                            {profile && (
                                <FileInput
                                    name="image"
                                    buttonText="Changer la photo"
                                    accept="image/*"
                                    errors={
                                        serverErrors.avatar
                                            ? [serverErrors?.avatar]
                                            : []
                                    }
                                    onChange={(avatar) =>
                                        handleImageChange(avatar)
                                    }
                                />
                            )}
                        </div>
                    </div>
                    <div className="card md:flex-1 bg-white rounded shadow">
                        <div className="md:p-8">
                            <div className="text-xl font-bold pb-4 px-8 py-4">
                                Information général
                            </div>
                            <form onSubmit={handleSubmit}>
                                <div className="flex flex-wrap p-8 -mb-8 -mr-6">
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Prénom"
                                        name="first_name"
                                        placeholder="John"
                                        errors={errors.first_name}
                                        value={data.first_name}
                                        onChange={(e) =>
                                            setData(
                                                "first_name",
                                                e.target.value
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Nom"
                                        type="text"
                                        name="last_name"
                                        placeholder="Doe"
                                        errors={errors.last_name}
                                        value={data.last_name}
                                        onChange={(e) =>
                                            setData("last_name", e.target.value)
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Adresse"
                                        type="text"
                                        name="address"
                                        placeholder="Conakry"
                                        errors={errors.address}
                                        value={data.address}
                                        onChange={(e) =>
                                            setData("address", e.target.value)
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Téléphone"
                                        type="text"
                                        name="phone"
                                        placeholder="+224 622 00 00 00"
                                        errors={errors.phone}
                                        value={data.phone}
                                        onChange={(e) =>
                                            setData("phone", e.target.value)
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Département"
                                        type="text"
                                        name="department"
                                        errors={errors.department}
                                        value={data.department}
                                        placeholder="Développemnt"
                                        onChange={(e) =>
                                            setData(
                                                "department",
                                                e.target.value
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Role"
                                        type="text"
                                        name="role"
                                        errors={errors.role}
                                        value={data.role}
                                        placeholder="Developpeur frontend"
                                        onChange={(e) =>
                                            setData("role", e.target.value)
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Langages"
                                        type="text"
                                        name="languages"
                                        placeholder="Francais, Englais"
                                        errors={errors.languages}
                                        value={data.languages}
                                        onChange={(e) =>
                                            setData("languages", e.target.value)
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Organisation"
                                        type="text"
                                        name="organization"
                                        errors={errors.organization}
                                        value={data.organization}
                                        placeholder="Nom compagnie"
                                        onChange={(e) =>
                                            setData(
                                                "organization",
                                                e.target.value
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Education"
                                        type="text"
                                        name="education"
                                        errors={errors.education}
                                        value={data.education}
                                        placeholder="Développeur senior"
                                        onChange={(e) =>
                                            setData("education", e.target.value)
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Experience professionnel"
                                        type="text"
                                        name="work_history"
                                        errors={errors.work_history}
                                        value={data.work_history}
                                        onChange={(e) =>
                                            setData(
                                                "work_history",
                                                e.target.value
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Date d'anniversaire"
                                        type="date"
                                        name="birthday"
                                        errors={errors.birthday}
                                        value={data.birthday}
                                        onChange={(e) =>
                                            setData("birthday", e.target.value)
                                        }
                                    />
                                    <TextAreaInput
                                        label="Biographie"
                                        name="bio"
                                        value={data.bio}
                                        errors={errors.bio}
                                        onChange={(e) =>
                                            setData("bio", e.target.value)
                                        }
                                    />
                                </div>
                                <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                                    <LoadingButton
                                        loading={processing}
                                        type="submit"
                                        className="ml-auto btn-indigo"
                                    >
                                        Mettre àjour
                                    </LoadingButton>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="flex flex-col md:flex-row gap-4 items-start">
                    <div className="flex-1 card bg-white rounded shadow">
                        <div className="p-8">
                            <div className="text-xl font-bold pb-4">
                                Information de mot de passe
                            </div>
                            <form onSubmit={handlePasswordSubmit}>
                                <div className="flex flex-wrap -mb-8 -mr-6">
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Mot de passe actuel"
                                        type="password"
                                        name="current_password"
                                        errors={passwordErrors.current_password}
                                        value={passwordData.current_password}
                                        onChange={(e) =>
                                            setPasswordData(
                                                "current_password",
                                                e.target.value
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="nouveau mot de passe"
                                        type="password"
                                        name="password"
                                        errors={passwordErrors.password}
                                        value={passwordData.password}
                                        onChange={(e) =>
                                            setPasswordData(
                                                "password",
                                                e.target.value
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="w-full pb-2 pr-6 lg:w-1/2"
                                        label="Confirmer le mot de passe"
                                        type="password"
                                        name="password_confirmation"
                                        errors={
                                            passwordErrors.password_confirmation
                                        }
                                        value={
                                            passwordData.password_confirmation
                                        }
                                        onChange={(e) =>
                                            setPasswordData(
                                                "password_confirmation",
                                                e.target.value
                                            )
                                        }
                                    />
                                </div>
                                <div className="py-8">
                                    <div className="text-xl font-medium">
                                        Conseils relatives aux mots de passe :
                                    </div>
                                    <div className="text-sm text-gray-600">
                                        S'assurer que ces exigences sont
                                        respectées :
                                    </div>
                                    <p className="text-sm text-gray-600">
                                        Au moins 10 caractères (et jusqu'à 100
                                        caractères)
                                    </p>
                                    <p className="text-sm text-gray-600">
                                        Au moins un caractère minuscule
                                        Inclusion d'au moins un caractère
                                        spécial, par exemple, ! @ #?
                                    </p>
                                </div>
                                <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                                    <LoadingButton
                                        loading={processing}
                                        type="submit"
                                        className="ml-auto btn-indigo"
                                    >
                                        Modifiez
                                    </LoadingButton>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

Profile.layout = (page) => <Layout children={page} title="Profile" />;
export default Profile;
