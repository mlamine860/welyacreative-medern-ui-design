import { Link, router, useForm, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import Layout from "../../../shared/Layout";
import TitleText from "../../../shared/TitleText";
import TextInput from "../../../shared/TextInput";
import SelectInput from "../../../shared/SelectInput";
import DeleteButton from "../../../shared/DeleteButton";
import LoadingButton from "../../../shared/LoadingButton";
import AlertConfirm from "../../../shared/AlertConfirm";
import { route } from "../../../utils";
import Select from "react-select";
import CheckboxInput from "../../../shared/CheckboxInput";

const toOptionSelect = (item) => ({ value: item.id, label: item.name });

function Edit() {
    const { user, teams } = usePage().props;
    const { data, setData, errors, put, processing } = useForm({
        name: user.name || "",
        email: user.email || "",
        password: user.password || "",
        role: user.role || "user",
        teams: user.teams?.data.map(toOptionSelect) || [],
        active: Number(user.active),
    });
    const [show, setShow] = useState();
    const handleSubmit = (e) => {
        e.preventDefault();
        put(route("users.update", user.id));
    };
    const destroy = () => {
        setShow(false);
        router.delete(route("users.destroy", user.id));
    };
    return (
        <div>
            <AlertConfirm
                show={show}
                onClose={() => setShow(false)}
                onConfirm={destroy}
            >
                Vous etes sur de vouloir supprimer cet utilisateur ?
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary hover:text-primary"
                    href={route("users.index")}
                >
                    Utiliateurs
                </Link>{" "}
                /{user.name}
            </TitleText>
            <div className="max-w-3xl overflow-hidden bg-white rounded shadow">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap p-8 -mb-8 -mr-6">
                        <TextInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Nom"
                            name="name"
                            errors={errors.name}
                            value={data.name}
                            onChange={(e) => setData("name", e.target.value)}
                        />
                        <TextInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Email"
                            type="email"
                            name="email"
                            errors={errors.email}
                            value={data.email}
                            onChange={(e) => setData("email", e.target.value)}
                        />
                        <TextInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Mot de passe"
                            name="password"
                            type="password"
                            errors={errors.password}
                            value={data.password}
                            onChange={(e) =>
                                setData("password", e.target.value)
                            }
                        />
                        <SelectInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Role"
                            name="role"
                            errors={errors.role}
                            value={data.role}
                            onChange={(e) => setData("role", e.target.value)}
                        >
                            <option value="admin">Administrateur</option>
                            <option value="editor">Editeur</option>
                            <option value="user">Simple utilisateur</option>
                        </SelectInput>
                    </div>
                    <div className="flex  justify-between items-center p-8">
                        <CheckboxInput
                            label="Active"
                            name="active"
                            value={data.active}
                            onChange={(active) => setData("active", active)}
                        />
                        <Select
                            placeholder="Teams"
                            className="w-full md:w-1/2"
                            isMulti
                            value={data.teams}
                            onChange={(teams) => setData("teams", teams)}
                            options={teams.data.map(toOptionSelect)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimé
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre àjour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Edit.layout = (page) => <Layout children={page} title="Utilisateur" />;
export default Edit;
