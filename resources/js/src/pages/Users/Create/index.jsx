import { Link, router, useForm, usePage } from "@inertiajs/react";
import Layout from "../../../shared/Layout";
import TitleText from "../../../shared/TitleText";
import TextInput from "../../../shared/TextInput";
import SelectInput from "../../../shared/SelectInput";
import LoadingButton from "../../../shared/LoadingButton";
import { route } from "../../../utils";

function Create() {
    const { data, setData, errors, post, processing } = useForm({
        name: "",
        email: "",
        password: "",
        role: "user",
    });
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("users.store"));
    };

    return (
        <div>
            <TitleText>
                <Link
                    className="text-primary hover:text-primary"
                    href={route("users.index")}
                >
                    Utiliateurs
                </Link>{" "}
                / créer
            </TitleText>
            <div className="max-w-3xl overflow-hidden bg-white rounded shadow">
                <form onSubmit={handleSubmit}>
                    <div className="flex flex-wrap p-8 -mb-8 -mr-6">
                        <TextInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Nom"
                            name="name"
                            errors={errors.name}
                            value={data.name}
                            onChange={(e) => setData("name", e.target.value)}
                        />
                        <TextInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Email"
                            type="email"
                            name="email"
                            errors={errors.email}
                            value={data.email}
                            onChange={(e) => setData("email", e.target.value)}
                        />
                        <TextInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Mot de passe"
                            name="password"
                            type="password"
                            errors={errors.password}
                            value={data.password}
                            onChange={(e) =>
                                setData("password", e.target.value)
                            }
                        />
                        <SelectInput
                            className="w-full pb-8 pr-6 lg:w-1/2"
                            label="Role"
                            name="role"
                            errors={errors.role}
                            value={data.role}
                            onChange={(e) => setData("role", e.target.value)}
                        >
                            <option value=""></option>
                            <option value="admin">Administrateur</option>
                            <option value="editor">Editeur</option>
                            <option value="user">Simple utilisateur</option>
                        </SelectInput>
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Create.layout = (page) => <Layout children={page} title="Utilisateur" />;
export default Create;
