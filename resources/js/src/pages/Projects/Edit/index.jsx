import { Link, router, useForm, usePage } from "@inertiajs/react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import DeleteButton from "../../../shared/DeleteButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import FileInput from "../../../shared/FileInput";
import TinyMCE from "../../../shared/TinyMCE";
import { imageUploadHandler, route, toOptionSelect } from "../../../utils";
import React, { useState } from "react";
import AlertConfirm from "../../../shared/AlertConfirm";
import ReactSelect from "react-select";

import "filepond/dist/filepond.min.css";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";

import axios from "axios";
import "filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css";
import AppFilepond from "../../../shared/AppFilepond";

function Edit() {
    const { project, tags, errors: er } = usePage().props;
    const { data, setData, errors, post, processing } = useForm({
        name: project.name,
        description: project.description,
        image: "",
        content: project.content || "",
        tags: project.tags?.data?.map(toOptionSelect) || [],
        files: [],
        _method: "PUT",
    });
    const [show, setShow] = useState(false);
    const [files, setFiles] = useState(
        project.media?.data.map((m) => ({
            source: m.url,
            options: {
                type: "local",
                file: {
                    name: m.name,
                    size: m.size,
                    type: m.mime_type,
                },
            },
            metadata: {
                poster: m.url,
            },
        }))
    );
    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("projects.update", project.id));
    };
    const destroy = () => {
        router.delete(route("projects.destroy", project.id));
    };
    const process = (v) => ({
        url: route("filepond-process"),
        method: "POST",
        onload(file) {
            file = JSON.parse(file);
            setData((data) => ({ ...data, files: [...data.files, file] }));
            return file.path;
        },
    });
    const revert = (file, load, error) => {
        axios
            .delete(route("filepond-revert"), {
                data: { filePath: file },
            })
            .then(() => {
                setData(
                    "files",
                    data.files.filter((f) => f.path !== file)
                );
            })
            .catch((err) => error(err));
    };

    const remove = (source, load, err) => {
        const parts = source.split("/");
        const file = parts[parts.length - 1];
        axios
            .delete(
                route("filepond-remove", {
                    mediable_id: project.id,
                    media_name: file,
                })
            )
            .then((success) => {
                console.log(success);
                setFiles(files.filter((f) => f.source !== source));
                load();
            })
            .catch((err) => {});
    };


    return (
        <div>
            <AlertConfirm show={show} onClose={setShow} onConfirm={destroy}>
                <p>Vous etes sûr de vouloir supprimer ce projet ?</p>
            </AlertConfirm>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("projects.index")}
                >
                    Projects /
                </Link>{" "}
                {project.name}
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <TextInput
                        name="name"
                        label="Le Nom"
                        value={data.name}
                        onChange={(e) => setData("name", e.target.value)}
                        errors={errors.title}
                        className="w-full pb-8 pr-6"
                    />
                    <TextAreaInput
                        label="Description"
                        name="description"
                        value={data.description}
                        errors={errors.description}
                        onChange={(e) => setData("description", e.target.value)}
                        className="w-full pb-8 pr-6"
                    />
                    <TinyMCE
                        errors={errors.content}
                        label="Contenu"
                        value={data.content}
                        onChange={(content) => setData("content", content)}
                        className="w-full pb-6 md:pr-6"
                        images_upload_handler={imageUploadHandler}
                    />
                    <FileInput
                        label="Image de mise en avant"
                        name="image"
                        errors={errors.image}
                        value={data.image}
                        onChange={(image) => setData("image", image)}
                    />
                    <div className="pt-8">
                        <p className="mb-2">Les images de slide</p>
                        <AppFilepond
                            name="files"
                            files={files}
                            setFiles={setFiles}
                            process={process}
                            revert={revert}
                            remove={remove}
                            maxFiles={5}
                        />
                    </div>
                    <div className="flex flex-col gap-2 py-12">
                        <label htmlFor="">Associer à des tags</label>
                        <ReactSelect
                            placeholder="Tags"
                            className="w-full md:w-1/2"
                            isMulti
                            value={data.tags}
                            onChange={(tags) => setData("tags", tags)}
                            options={tags.data.map(toOptionSelect)}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <DeleteButton onDelete={() => setShow(true)}>
                            Supprimer
                        </DeleteButton>
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Mettre à jour
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Edit.layout = (page) => <Layout children={page} title="Noveau Projet" />;
export default Edit;
