import { Link, useForm } from "@inertiajs/react";
import Layout from "../../../shared/Layout";
import LoadingButton from "../../../shared/LoadingButton";
import TextAreaInput from "../../../shared/TextAreaInput";
import TextInput from "../../../shared/TextInput";
import TitleText from "../../../shared/TitleText";
import FileInput from "../../../shared/FileInput";
import { imageUploadHandler, route } from "../../../utils";
import React, { useState } from "react";
import TinyMCE from "../../../shared/TinyMCE";
import AppFilepond from "../../../shared/AppFilepond";
import axios from "axios";

function Create() {
    const { data, setData, errors, post, processing } = useForm({
        name: "",
        description: "",
        image: "",
        content: "",
        files: [],
    });
    const [files, setFiles] = useState([]);

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route("projects.store"));
    };
    const process = () => ({
        url: route("filepond-process"),
        method: "POST",
        onload(file) {
            file = JSON.parse(file);
            setData((data) => ({ ...data, files: [...data.files, file] }));
            return file.path;
        },
    });
    const revert = (file, load, error) => {
        axios
            .delete(route("filepond-revert"), {
                data: { filePath: file },
            })
            .then(() => {
                setData(
                    "files",
                    data.files.filter((f) => f.path !== file)
                );
            })
            .catch((err) => error(err));
    };
    return (
        <div>
            <TitleText>
                <Link
                    className="text-primary/70 hover:text-primary"
                    href={route("projects.index")}
                >
                    Projects /
                </Link>
                Creation
            </TitleText>
            <div className="max-w-7xl overflow-hidden bg-white rounded shadow px-8 py-6">
                <form onSubmit={handleSubmit}>
                    <TextInput
                        name="name"
                        label="Le Nom"
                        value={data.name}
                        onChange={(e) => setData("name", e.target.value)}
                        errors={errors.title}
                        className="w-full pb-8 pr-6"
                    />
                    <TextAreaInput
                        label="Description"
                        name="description"
                        value={data.description}
                        errors={errors.description}
                        onChange={(e) => setData("description", e.target.value)}
                        className="w-full pb-8 pr-6"
                    />
                    <TinyMCE
                        errors={errors.content}
                        label="Contenu"
                        value={data.content}
                        onChange={(content) => setData("content", content)}
                        className="w-full pb-6 md:pr-6"
                        images_upload_handler={imageUploadHandler}
                    />
                    <FileInput
                        label="Image"
                        name="image"
                        errors={errors.image}
                        value={data.image}
                        onChange={(image) => setData("image", image)}
                    />
                    <div className="pt-8">
                        <p className="mb-2">Les images de slide</p>
                        <AppFilepond
                            name="files"
                            files={files}
                            setFiles={setFiles}
                            process={process}
                            revert={revert}
                        />
                    </div>
                    <div className="flex items-center px-8 py-4 bg-gray-100 border-t border-gray-200">
                        <LoadingButton
                            loading={processing}
                            type="submit"
                            className="ml-auto btn-indigo"
                        >
                            Créer
                        </LoadingButton>
                    </div>
                </form>
            </div>
        </div>
    );
}

Create.layout = (page) => <Layout children={page} title="Noveau Projet" />;
export default Create;
