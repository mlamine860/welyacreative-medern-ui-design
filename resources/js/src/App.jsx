import React, { useEffect, useState } from 'react';
import AppNavbar from './components/AppNavbar'
import AppSidebar from './components/AppSidebar'
import { Outlet, useRouteError } from "react-router-dom";
import { Flowbite } from 'flowbite-react';
import { AuthContext } from './auth/context'
import storage from './auth/storage';
import Auth from './pages/Auth';

import {store} from './app'
import {Provider} from 'react-redux'


const them = {
}
export default function App() {
    const [userInfo, setUserInfo] = useState()
    const [collapsed, setCollapsed] = useState(false)
    const collapseSideBar = () => setCollapsed(!collapsed)
    const error = useRouteError();

    useEffect(() => {
        setUserInfo(storage.getUserInfo())
    }, [])

    return (
     <Provider store={store}>
           <AuthContext.Provider value={ { userInfo, setUserInfo } }>
            {
                userInfo ? (
                    <Flowbite theme={ them }>
                        <AppNavbar collapseSideBar={ collapseSideBar } />
                        <main className='flex h-screen'>
                            <div className="w-fit">
                                <AppSidebar collapsed={ collapsed } />
                            </div>
                            <div className="w-full p-2">
                                <Outlet />
                            </div>
                        </main>
                    </Flowbite>
                ) :
                    <Auth />
            }
        </AuthContext.Provider>
     </Provider>
    )
}
