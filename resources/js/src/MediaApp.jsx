import axios from "axios";
import { useEffect, useState } from "react";
import { route } from "./utils";
import { TfiViewGrid } from "react-icons/tfi";
import { TiArrowUnsorted } from "react-icons/ti";
import {
    AiOutlineUnorderedList,
    AiFillFolderOpen,
} from "react-icons/ai";
import FileInput from "./shared/FileInput";
import LoadingButton from "./shared/LoadingButton";
import Icon from "./shared/Icon";

const Media = ({ url, file_name, setSelectedMedia, selectedMedia }) => {
    const handleSelectedMedia = (url) => {
        setSelectedMedia(url);
        if (window !== window.top)
            window.parent.postMessage(
                {
                    mceAction: "customAction",
                    data: {
                        location: url,
                        alt: file_name,
                    },
                },
                "*"
            );
    };

    return (
        <div
            className={`relative cursor-pointer w-40 h-40 md:w-32 md:h-32`}
            onClick={() => handleSelectedMedia(url)}
        >
            <img
                data-url={url}
                src={url}
                alt=""
                className={`w-full h-full object-cover shadow rounded`}
            />
            {selectedMedia === url && (
                <div className="absolute inset-0 bg-gray-800 opacity-50 flex justify-center items-center">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-8 h-8 text-white"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                    </svg>
                </div>
            )}
        </div>
    );
};

const Spiner = () => {
    return (
        <div className="fixed inset-0 bg-black/30 flex justify-center items-center">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="text-white animate-spin w-16 h-16"
                fill="currentColor"
                viewBox="0 0 24 24"
            >
                <path d="M11.9995 2C12.5518 2 12.9995 2.44772 12.9995 3V6C12.9995 6.55228 12.5518 7 11.9995 7C11.4472 7 10.9995 6.55228 10.9995 6V3C10.9995 2.44772 11.4472 2 11.9995 2ZM11.9995 17C12.5518 17 12.9995 17.4477 12.9995 18V21C12.9995 21.5523 12.5518 22 11.9995 22C11.4472 22 10.9995 21.5523 10.9995 21V18C10.9995 17.4477 11.4472 17 11.9995 17ZM20.6597 7C20.9359 7.47829 20.772 8.08988 20.2937 8.36602L17.6956 9.86602C17.2173 10.1422 16.6057 9.97829 16.3296 9.5C16.0535 9.02171 16.2173 8.41012 16.6956 8.13398L19.2937 6.63397C19.772 6.35783 20.3836 6.52171 20.6597 7ZM7.66935 14.5C7.94549 14.9783 7.78161 15.5899 7.30332 15.866L4.70525 17.366C4.22695 17.6422 3.61536 17.4783 3.33922 17C3.06308 16.5217 3.22695 15.9101 3.70525 15.634L6.30332 14.134C6.78161 13.8578 7.3932 14.0217 7.66935 14.5ZM20.6597 17C20.3836 17.4783 19.772 17.6422 19.2937 17.366L16.6956 15.866C16.2173 15.5899 16.0535 14.9783 16.3296 14.5C16.6057 14.0217 17.2173 13.8578 17.6956 14.134L20.2937 15.634C20.772 15.9101 20.9359 16.5217 20.6597 17ZM7.66935 9.5C7.3932 9.97829 6.78161 10.1422 6.30332 9.86602L3.70525 8.36602C3.22695 8.08988 3.06308 7.47829 3.33922 7C3.61536 6.52171 4.22695 6.35783 4.70525 6.63397L7.30332 8.13398C7.78161 8.41012 7.94549 9.02171 7.66935 9.5Z"></path>
            </svg>
        </div>
    );
};
const folders = ["Media", "Dossier Partager"];
const Error = ({ error }) => {
    return <div className="text-lg bg-red-100 w-full  p-4">{error}</div>;
};
export default function MediaApp() {
    const [display, setDisplay] = useState("row");
    const [sort, setSort] = useState("asc");
    const [data, setData] = useState([]);
    const [error, setError] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [selectedMedia, setSelectedMedia] = useState();
    const loadData = async (url = route("media.list")) => {
        setIsLoading(true);
        axios
            .get(url)
            .then(({ data }) => {
                setIsLoading(false);
                setData(data);
            })
            .catch((err) => {
                setIsLoading(false);
                setError(err.response.data);
            });
    };

    const toggleSort = () => {
        setSort(sort === "asc" ? "desc" : "asc");
        loadData(route("media.list", { sortBy: sort }));
    };
    const [image, setImage] = useState("");
    const [uploading, setUploading] = useState();
    const [uploadError, setUploadError] = useState();
    const [formOpen, setFormOpen] = useState();
    const handleUpload = async () => {
        setUploading(true);
        const formData = new FormData();
        setUploadError(false);
        formData.append("image", image);
        axios
            .post(route("uploads.image"), formData)
            .then((res) => {
                setUploading(false);
                setFormOpen(false);
                setData({ ...data, data: [res.data, ...data.data] });
            })
            .catch((err) => {
                setUploading(false);
                setUploadError(err.response.data.message);
            });
    };

    const handleDeleteMedia = async () => {
        const mediaId = data.data.find((m) => m.url === selectedMedia).id;
        setIsLoading(true);
        axios
            .delete(route("media.destroy", { id: mediaId }))
            .then((res) => {
                setIsLoading(false);
                setData({
                    ...data,
                    data: data.data.filter((m) => m.id !== mediaId),
                });
            })
            .catch((err) => {
                console.log(err);
                setIsLoading(false);
            });
    };
    const gotToDashboard = () => {
        history.pushState({}, location.href);
        location.replace(route("admin"));
    };
    useEffect(() => {
        loadData();
    }, []);

    return (
        <div>
            {formOpen && (
                <div className="fixed inset-0 bg-black/40 z-10 flex flex-col items-center justify-center">
                    <div className="bg-white p-4 w-96 shadow-lg rounded relative">
                        <button
                            onClick={() => setFormOpen(false)}
                            className="absoulte top-0 right-0 max-w-max"
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M6 18L18 6M6 6l12 12"
                                />
                            </svg>
                        </button>
                        <h3 className="text-xl text-secondary text-center font-bold">
                            Envoyer une image
                        </h3>
                        {uploadError && (
                            <div className="text-center bg-red-200 py-2 mt-4 text-red-900">
                                {uploadError}
                            </div>
                        )}
                        <div className="py-8">
                            <FileInput
                                name="image"
                                label=""
                                onChange={(image) => setImage(image)}
                            />
                            <LoadingButton
                                onClick={handleUpload}
                                type="submit"
                                loading={uploading}
                                className="btn-indigo mt-6 w-full justify-center"
                            >
                                Envoyer
                            </LoadingButton>
                        </div>
                    </div>
                </div>
            )}
            <header className="h-[13vh] bg-secondary mb-8 flex items-center justify-between px-8">
                {window === window.parent && (
                    <button
                        className="flex justify-center items-center gap-1 text-white"
                        onClick={gotToDashboard}
                    >
                        <Icon name="dashboard" className="text-white w-6 h-6" />
                        Dashboard
                    </button>
                )}
                <ul className="flex items-center  justify-end gap-4">
                    {selectedMedia && window === window.parent && (
                        <li>
                            <button
                                onClick={handleDeleteMedia}
                                className="flex items-center text-white gap-1"
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="w-6 h-6"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                    />
                                </svg>
                                Supprimer
                            </button>
                        </li>
                    )}
                    <li>
                        <button
                            onClick={() => setDisplay("row")}
                            className="flex items-center text-white gap-1"
                        >
                            <TfiViewGrid className="text-white" />
                            Vignettes
                        </button>
                    </li>
                    <li>
                        <button
                            onClick={() => setDisplay("col")}
                            className="flex items-center text-white gap-1"
                        >
                            <AiOutlineUnorderedList className="text-white" />
                            Liste
                        </button>
                    </li>
                    <li>
                        <button
                            onClick={toggleSort}
                            className="flex items-center text-white gap-1"
                        >
                            Trier
                            <TiArrowUnsorted className="text-white" />
                        </button>
                    </li>
                </ul>
            </header>
            <main className="max-w-7xl m-auto md:flex">
                <aside className="md:w-1/3 hidden md:block">
                    <ul>
                        {folders.map((folder) => (
                            <li
                                key={folder}
                                className="bg-secondary text-white cursor-pointer border px-2 py-4 mb-2 flex items-center gap-2"
                            >
                                <AiFillFolderOpen className="w-6 h-6" />{" "}
                                {folder}
                            </li>
                        ))}
                    </ul>
                </aside>
                <div className="p-4 md:px-8 w-full">
                    {isLoading ? (
                        <Spiner />
                    ) : error ? (
                        <Error error={error?.message} />
                    ) : (
                        <div className="w-full">
                            <div className="flex flex-wrap justify-between md:justify-start gap-4 w-full">
                                {data.data?.map(({ url, file_name, id }) => (
                                    <Media
                                        url={url}
                                        file_name={file_name}
                                        key={id}
                                        setSelectedMedia={setSelectedMedia}
                                        selectedMedia={selectedMedia}
                                    />
                                ))}
                            </div>
                            <div className="py-8 flex justify-center items-center gap-8">
                                {data.links?.map(
                                    ({ url, label, active }) =>
                                        url && (
                                            <button
                                                onClick={() => loadData(url)}
                                                key={label}
                                                disabled={active}
                                                className={`bg-primary px-2 py-1 text-white rounded hover:bg-primary/50 ${
                                                    active &&
                                                    "cursor-not-allowed bg-primary/30  hover:bg-primary/30"
                                                }`}
                                            >
                                                {Number.isNaN(Number(label)) ? (
                                                    <span
                                                        dangerouslySetInnerHTML={{
                                                            __html: label,
                                                        }}
                                                    />
                                                ) : (
                                                    label
                                                )}
                                            </button>
                                        )
                                )}
                            </div>
                        </div>
                    )}
                </div>
            </main>
            <button
                onClick={() => setFormOpen(true)}
                className="fixed bottom-10 right-12 w-16 h-16 rounded-full text-white text-xl shadow-2xl bg-primary flex items-center justify-center"
            >
                +
            </button>
        </div>
    );
}
