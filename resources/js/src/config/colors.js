export default {
    primary: '#FF5F9E',
    secondary: '#060047',
    pink: '#E90064',
    darkRed: '#B3005E',
    body: '#4f4b79',
    success: '#09ac09',
    danger: '#d90202',
    warning: '#d9d902',
}
