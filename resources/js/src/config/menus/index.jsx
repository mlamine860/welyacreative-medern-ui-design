import { AiFillDashboard } from "react-icons/ai";
import { FaUser } from "react-icons/fa";
import { RiServiceFill } from "react-icons/ri";

export default [
    {
        path: "/",
        label: "Dashboard",
        icon: AiFillDashboard,
    },
    {
        label: "Services",
        icon: RiServiceFill,
        path: "/services",
        sub: [
            {
                path: "/services",
                label: "Tous les services",
            },
            {
                path: "/services/create",
                label: "Ajouter",
            },
        ],
    },
    {
        label: "Utlisateurs",
        icon: FaUser,
        path: "/users",
        sub: [
            {
                path: "/users",
                label: "Tous les utilisateurs",
            },
            {
                path: "/users/create",
                label: "Ajouter",
            },
        ],
    },
];
