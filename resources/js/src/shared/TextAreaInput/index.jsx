import React from "react";

function TextAreaInput({ label, name, errors = [], className, ...props }) {
    return (
        <div className={className}>
            {label && (
                <label className="form-label" htmlFor={name}>
                    {label}:
                </label>
            )}
            <textarea
                id={name}
                name={name}
                {...props}
                className={`form-textarea ${errors.length ? "error" : ""}`}
            ></textarea>
            {errors && <div className="form-error">{errors}</div>}
        </div>
    );
}

export default TextAreaInput;
