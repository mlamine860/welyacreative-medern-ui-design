import React from "react";
import {
    AiFillDashboard,
    AiFillDelete,
    AiFillQuestionCircle,
    AiFillSetting,
    AiFillTag,
    AiOutlineClose,
    AiOutlineTeam,
} from "react-icons/ai";
import { MdOutlineMiscellaneousServices, MdPages, MdUnsubscribe } from "react-icons/md";
import { FaBlogger } from "react-icons/fa";
import { FiChevronDown, FiChevronRight, FiMessageSquare } from "react-icons/fi";
import { BiCategoryAlt, BiRefresh } from "react-icons/bi";
import { GoFileMedia } from "react-icons/go";
import {
    BsFillChatLeftQuoteFill,
    BsShieldFillCheck,
    BsShieldLockFill,
    BsShieldX,
    BsCheck,
    BsListTask,
} from "react-icons/bs";

import ProjectsIcon from "../ProjectsIcon";

function Icon({ name, ...otherProps }) {
    switch (name) {
        case "dashboard":
            return <AiFillDashboard {...otherProps} />;
        case "services":
            return <MdOutlineMiscellaneousServices {...otherProps} />;
        case "cheveron-down":
            return <FiChevronDown {...otherProps} />;
        case "cheveron-right":
            return <FiChevronRight {...otherProps} />;
        case "checked":
            return <BsCheck {...otherProps} />;
        case "unchecked":
            return <AiOutlineClose {...otherProps} />;
        case "messages":
            return <FiMessageSquare {...otherProps} />;
        case "blog":
            return <FaBlogger {...otherProps} />;
        case "testimony":
            return <BsFillChatLeftQuoteFill {...otherProps} />;
        case "site":
            return <AiFillSetting {...otherProps} />;
        case "questions":
            return <AiFillQuestionCircle {...otherProps} />;
        case "admin":
            return <BsShieldFillCheck {...otherProps} />;
        case "user":
            return <BsShieldX {...otherProps} />;
        case "editor":
            return <BsShieldLockFill {...otherProps} />;
        case "pages":
            return <MdPages {...otherProps} />;
        case "category":
            return <BiCategoryAlt {...otherProps} />;
        case "teams":
            return <AiOutlineTeam {...otherProps} />;
        case "projects":
            return <ProjectsIcon {...otherProps} />;
        case "tags":
            return <AiFillTag {...otherProps} />;
        case "delete":
            return <AiFillDelete {...otherProps} />;
        case "subscribe":
            return <MdUnsubscribe {...otherProps} />;
        case "tasks":
            return <BsListTask {...otherProps} />;
        case "refresh":
            return <BiRefresh {...otherProps} />;
        case "media":
            return <GoFileMedia {...otherProps} />;
        default:
            return null;
    }
}

export default Icon;
