import React from "react";

function Logo({ white, ...otherProps }) {
    if (white) {
        return (
            <svg
                width={160}
                height={40}
                viewBox="0 0 42.334 10.583"
                id="svg5"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                {...otherProps}
            >
                <defs id="defs2">
                    <style id="style1254">
                        {
                            ".cls-1{isolation:isolate}.cls-2{font-size:74.5px;font-family:ChubGothic,Chub Gothic}.cls-3{fill:#8d4ee9}.cls-4{opacity:.49;mix-blend-mode:multiply}.cls-5{fill:#e6e6e6}"
                        }
                    </style>
                </defs>
                <g id="layer1" transform="matrix(1 0 0 1.00002 -5.292 -2.646)">
                    <g
                        id="g7563"
                        transform="translate(-16.75 -6.423) scale(.25151)"
                    >
                        <g id="g1">
                            <g id="g2" transform="translate(6.695 -1.994)">
                                <g
                                    id="g4"
                                    style={{
                                        fill: "#fff",
                                        fillOpacity: 1,
                                    }}
                                    transform="translate(19.617 -3.427) scale(.89811)"
                                >
                                    <path
                                        d="M121.955 63.677h2.524l1.204 3.795 1.203-3.795h2.541l-1.989 5.517 1.204 3.193 2.975-8.726h2.992l-4.747 11.868h-2.29l-1.89-4.647-1.872 4.647h-2.307l-4.73-11.868h2.975l2.993 8.726 1.17-3.193z"
                                        id="path7489"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M143.988 73.122v2.407h-8.342V63.661h8.191v2.407h-5.45v2.307h4.681v2.223h-4.68v2.524z"
                                        id="path7491"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M145.66 75.53V63.66h2.74v9.462h5.751v2.407z"
                                        id="path7493"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="m155.255 63.66 2.557 5.45 2.591-5.45h2.993l-4.23 7.824v4.045h-2.724v-4.078l-4.163-7.79z"
                                        id="path7495"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M166.355 63.66h2.474l4.33 11.87h-2.809l-.92-2.658h-3.694l-.903 2.657h-2.808zm2.624 7.323-1.387-4.196-1.421 4.196z"
                                        id="path7497"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M177.02 69.495q0-1.07.401-2.123.401-1.07 1.17-1.906.77-.836 1.873-1.354 1.103-.518 2.507-.518 1.672 0 2.892.718 1.237.72 1.839 1.873l-2.106 1.47q-.2-.467-.519-.768-.3-.318-.668-.502-.368-.2-.752-.267-.385-.084-.753-.084-.785 0-1.37.318-.586.318-.97.82-.385.5-.568 1.136-.184.635-.184 1.287 0 .702.217 1.354.217.652.618 1.154.418.501.987.802.585.284 1.304.284.367 0 .752-.083.401-.1.752-.285.368-.2.669-.501.3-.318.485-.77l2.24 1.322q-.268.651-.803 1.17-.518.518-1.203.869-.686.351-1.455.535-.769.184-1.504.184-1.287 0-2.374-.518-1.07-.535-1.856-1.405-.769-.869-1.203-1.972-.418-1.104-.418-2.24z"
                                        id="path7499"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M189.207 75.53V63.66h5.349q.836 0 1.538.352.719.35 1.237.919.518.568.802 1.287.301.719.301 1.455 0 .551-.134 1.07-.133.501-.384.952-.25.452-.619.82-.35.35-.802.601l2.608 4.413h-3.093l-2.273-3.828h-1.789v3.828zm2.741-6.22h2.508q.484 0 .835-.45.351-.469.351-1.187 0-.736-.4-1.17-.402-.435-.87-.435h-2.424z"
                                        id="path7501"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M208.748 73.122v2.407h-8.341V63.661h8.191v2.407h-5.45v2.307h4.681v2.223h-4.68v2.524z"
                                        id="path7503"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M213.646 63.66h2.475l4.33 11.87h-2.81l-.919-2.658h-3.694l-.903 2.657h-2.808zm2.625 7.323-1.387-4.196-1.421 4.196z"
                                        id="path7505"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M229.494 66.068h-3.61v9.461h-2.742v-9.461h-3.628V63.66h9.98z"
                                        id="path7507"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M230.831 75.53V63.66h2.742v11.87z"
                                        id="path7509"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="m237.568 63.66 2.792 8.426 2.758-8.425h2.892l-4.497 11.868h-2.307l-4.547-11.868z"
                                        id="path7511"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                    <path
                                        d="M255.455 73.122v2.407h-8.342V63.661h8.192v2.407h-5.45v2.307h4.68v2.223h-4.68v2.524z"
                                        id="path7513"
                                        style={{
                                            fontWeight: 800,
                                            fontSize: "21.6278px",
                                            fontFamily: "Raleway",
                                            InkscapeFontSpecification:
                                                "'Raleway Ultra-Bold'",
                                            opacity: 0.875593,
                                            fill: "#fff",
                                            fillOpacity: 1,
                                            strokeWidth: 2.32178,
                                        }}
                                    />
                                </g>
                                <path
                                    style={{
                                        fill: "#fff",
                                        fillOpacity: 1,
                                        strokeWidth: 5.29167,
                                    }}
                                    id="path1"
                                    d="m79.65 68.062-56.655-32.71v-65.42L79.65-62.776l56.655 32.71v65.42z"
                                    transform="translate(73.996 58.269) scale(.31158)"
                                />
                                <g
                                    id="g5"
                                    style={{
                                        fill: "#8d4ee9",
                                        fillOpacity: 1,
                                    }}
                                >
                                    <path
                                        style={{
                                            color: "#000",
                                            fill: "#8d4ee9",
                                            fillOpacity: 1,
                                            stroke: "none",
                                            strokeWidth: 0.266094,
                                            strokeOpacity: 1,
                                            InkscapeStroke: "none",
                                        }}
                                        d="M112.607 50.15c-.691.377-12.188 6.646-13.268 7.509-1.113.889-1.08 3.023-1.08 3.023l-.02 14.862a4.563 4.563 0 0 0 2.856-.536l10.362-5.982a4.563 4.563 0 0 0 2.281-3.952v-3.286l-9.89 5.71a.946.946 0 0 1-1.418-.82v-3.724c0-.915.487-1.76 1.279-2.216l10.03-5.79V53.11a4.563 4.563 0 0 0-1.132-2.96z"
                                        id="path3"
                                    />
                                    <path
                                        style={{
                                            color: "#000",
                                            fill: "#8d4ee9",
                                            fillOpacity: 1,
                                            stroke: "none",
                                            strokeWidth: 0.266094,
                                            strokeOpacity: 1,
                                            InkscapeStroke: "none",
                                        }}
                                        d="M98.814 42.565a4.57 4.57 0 0 0-2.282.61l-10.361 5.983a4.563 4.563 0 0 0-2.282 3.952v11.964c0 1.63.87 3.137 2.282 3.952l10.361 5.982c.148.07.3.134.454.188L97 60.237s.142-1.304.663-2.369c.522-1.064 2.094-1.963 2.094-1.963l10.928-6.305.747-.43.01.005.02-.013a.022.022 0 0 1-.004-.004l-2.795-1.613c-.062.034-12.369 6.737-13.479 7.624-1.113.889-1.08 3.023-1.08 3.023l-.02 9.939-.807-.9a1.742 1.742 0 0 1-.446-1.168l.013-8.316s.142-1.305.663-2.369c.522-1.064 2.094-1.963 2.094-1.963l11.618-6.703-2.851-1.647-1.64.872c-3.23 1.77-11.026 6.06-11.913 6.768-1.113.888-1.08 3.023-1.08 3.023l-.02 9.939-.735-.82a2.028 2.028 0 0 1-.517-1.356l.013-8.208s.142-1.305.663-2.37c.522-1.063 2.094-1.962 2.094-1.962l10.573-6.1 1.095-.632-1.806-1.043a4.563 4.563 0 0 0-2.281-.611z"
                                        id="path11"
                                    />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        );
    }
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100"
            height="100"
            viewBox="0 0 158.75 158.75"
        >
            <path
                fill="#8d4ee9"
                d="M79.374 158.75l-68.74-39.688V39.687L79.373.002l68.74 39.687v79.376z"
            ></path>
            <path
                style={{ InkscapeStroke: "none" }}
                fill="#fff"
                d="M133.089 44.557c-2.693 1.467-47.461 25.88-51.666 29.238-4.334 3.461-4.21 11.774-4.21 11.774l-.078 57.871a17.769 17.769 0 0011.124-2.085l40.35-23.296a17.768 17.768 0 008.884-15.388V89.873l-38.514 22.236c-2.454 1.416-5.523-.355-5.523-3.19V94.413a9.964 9.964 0 014.982-8.629l39.055-22.548V56.08a17.768 17.768 0 00-4.404-11.522z"
                color="#000"
            ></path>
            <path
                style={{ InkscapeStroke: "none" }}
                fill="#fff"
                d="M79.375 15.016a17.77 17.77 0 00-8.884 2.381L30.142 40.692a17.768 17.768 0 00-8.884 15.387v46.592a17.768 17.768 0 008.884 15.388l40.35 23.296c.576.276 1.167.52 1.769.732l.053-58.252s.551-5.08 2.583-9.224c2.03-4.145 8.153-7.644 8.153-7.644l42.556-24.554 2.907-1.678.033.021.085-.048-.02-.016-10.883-6.282c-.241.132-48.165 26.237-52.488 29.689-4.335 3.46-4.208 11.773-4.208 11.773l-.08 38.702-3.14-3.507a6.795 6.795 0 01-1.735-4.543l.052-32.386s.553-5.08 2.584-9.224c2.03-4.145 8.152-7.644 8.152-7.644l45.244-26.104-11.104-6.41-6.383 3.393C82.039 35.037 51.684 51.743 48.23 54.502c-4.334 3.461-4.208 11.774-4.208 11.774l-.08 38.702-2.858-3.192a7.896 7.896 0 01-2.015-5.281l.052-31.964s.551-5.08 2.582-9.224c2.031-4.145 8.153-7.643 8.153-7.643l41.175-23.757 4.263-2.46-7.034-4.061a17.768 17.768 0 00-8.884-2.38z"
                color="#000"
            ></path>
        </svg>
    );
}

export default Logo;
