import React, { Children } from "react";

const AlertConfirm = ({ show, onClose, onConfirm, children }) => {
    if (!show) return null;
    return (
        <div className="fixed inset-0  z-20 flex">
            <div className="fixed inset-0 bg-black opacity-25"></div>
            <div className="m-auto inline-block p-6 shadow-md rounded bg-white relative z-30 opacity-100">
                <div className="text-medium text-2xl mb-4">Confirmer</div>
                {children}
                <div className="flex justify-between pt-6">
                    <button
                        onClick={onConfirm}
                        className="px-4 py-2 bg-green-600 text-white"
                    >
                        Oui
                    </button>
                    <button
                        onClick={() => onClose(false)}
                        className="px-4 py-2 bg-red-600 text-white"
                    >
                        Non
                    </button>
                </div>
            </div>
        </div>
    );
};
export default AlertConfirm;
