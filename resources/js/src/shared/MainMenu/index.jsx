import React from "react";
import { route } from "../../utils";
import MenuItem from "../MenuItem";

function MainMenu({ className }) {
    return (
        <div className={className}>
            <MenuItem text="Dasboard" icon="dashboard" link={route("admin")} />
            <MenuItem text="Paramètres" icon="site" link={route("site.edit")} />
            <MenuItem
                text="Services"
                icon="services"
                link={route("services.index")}
            />
            <MenuItem
                text="Projets"
                icon="projects"
                link={route("projects.index")}
            />
            <MenuItem
                text="Messages"
                icon="messages"
                link={route("messages.index")}
            />
            <MenuItem
                text="Temoignages"
                icon="testimony"
                link={route("testimonies.index")}
            />
            <MenuItem text="Pages" icon="pages" link={route("pages.index")} />
            <MenuItem
                text="FAQ"
                icon="questions"
                link={route("questions.index")}
            />
            <MenuItem text="Teams" icon="teams" link={route("teams.index")} />
            <MenuItem text="Tags" icon="tags" link={route("tags.index")} />
            <MenuItem
                text="Souscriptions"
                icon="subscribe"
                link={route("newsletters.index")}
            />
            <MenuItem text="Tâches" icon="tasks" link={route("tasks.index")} />
            <MenuItem
                text="Media"
                icon="media"
                link={route("media.index")}
                nativeLink={true}
            />

            <MenuItem
                className="border-t border-white border-opacity-30 pt-2 mt-4"
                text="Blog"
                icon="blog"
                link={route("posts.index")}
            />
            <MenuItem
                className=""
                text="Categories"
                icon="category"
                link={route("categories.index")}
            />
        </div>
    );
}

export default MainMenu;
