import { Editor } from "@tinymce/tinymce-react";
import React, { useRef, useState } from "react";
import { route } from "../../utils";

// import { fileBrowser } from "../../utils";

function TinyMCE({
    className,
    onChange,
    images_upload_handler,
    errors,
    value,
}) {
    const config = {
        height: 800,
        menubar: true,
        plugins: [
            "advlist",
            "autolink",
            "autosave",
            "lists",
            "link",
            "image",
            "preview",
            "anchor",
            "searchreplace",
            "visualblocks",
            "fullscreen",
            "insertdatetime",
            "table",
            "code",
        ],
        toolbar:
            "undo redo | blocks | " +
            "bold italic forecolor | alignleft aligncenter " +
            "alignright alignjustify | bullist numlist outdent indent | " +
            "removeformat |  image | restoredraft",
        content_style:
            "body { font-family:Source Sans Pro, sans-serif; font-size:16px }",
        file_picker_callback: function fileBrowser(callback, value, meta) {
            const x =
                window.innerWidth ||
                document.documentElement.clientWidth ||
                document.getElementsByTagName("body")[0].clientWidth;
            const y =
                window.innerHeight ||
                document.documentElement.clientHeight ||
                document.getElementsByTagName("body")[0].clientHeight;

            var cmsURL = "/admin/media";
            if (meta.filetype == "image") {
                cmsURL = cmsURL + "?&type=Images";
            } else {
                cmsURL = cmsURL + "?&type=Files";
            }

            editorRef.current.windowManager.openUrl({
                url: cmsURL,
                title: "Media Manager",
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no",
                onMessage: (api, message) => {
                    callback(message.data.location, message.data);
                    api.close()
                   
                },
            });
        },
    };
    const editorRef = useRef(null);
    const [tinyEditor, setTinyEditor] = useState();
    return (
        <div className={className}>
            <Editor
                apiKey="0n13csvs7aj9um6u9erjpltovwmm1aka160vob4yepex9dst"
                onInit={(evt, editor) => {
                    editorRef.current = editor;
                    setTinyEditor(editor);
                }}
                init={config}
                onEditorChange={() => onChange(editorRef.current.getContent())}
                value={value}
            />
            {errors && <div className="form-error">{errors}</div>}
        </div>
    );
}

export default TinyMCE;
