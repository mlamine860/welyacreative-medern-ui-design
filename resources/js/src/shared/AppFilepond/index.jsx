import React from "react";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";

import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";

import Cookies from "js-cookie";

registerPlugin(
    FilePondPluginImageExifOrientation,
    FilePondPluginImagePreview,
);

function AppFilepond({
    process,
    revert,
    files,
    setFiles,
    name,
    maxFiles = 5,
    allowMultiple = true,
    remove
}) {
    return (
        <FilePond
            files={files}
            onupdatefiles={setFiles}
            allowMultiple={allowMultiple}
            maxFiles={maxFiles}
            server={{
                headers: {
                    "X-XSRF-TOKEN": Cookies.get("XSRF-TOKEN"),
                    "X-Requested-With": "XMLHttpRequest",
                },
                process: process(),
                revert: revert,
                remove:remove
            }}
            name={name}
            labelIdle='Faites glisser et déposez vos fichiers ou <span class="filepond--label-action">Parcourez</span>'
        />
    );
}

export default AppFilepond;
