import { usePage, router } from "@inertiajs/react";
import React, { useEffect, useState } from "react";
import { usePrevious } from "react-use";
import pickBy from "lodash/pickBy";

import SelectInput from "../SelectInput";

function SearchFilter() {
    const { props, url } = usePage();
    const { filters } = props;
    const [opened, setOpened] = useState(false);
    const [values, setValues] = useState({
        search: filters.search || "",
        featured: filters.featured || "",
        published: filters.published || "",
    });
    const prevValues = usePrevious(values);

    function handleChange(e) {
        const key = e.target.name;
        const value = e.target.value;

        setValues((values) => ({
            ...values,
            [key]: value,
        }));

        if (opened) setOpened(false);
    }

    function reset() {
        setValues({
            search: "",
            featured: "",
            published: "",
        });
    }

    useEffect(() => {
        // https://reactjs.org/docs/hooks-faq.html#how-to-get-the-previous-props-or-state
        if (prevValues) {
            const query = Object.keys(pickBy(values)).length
                ? pickBy(values)
                : { remember: "forget" };
            router.get(url, query, {
                replace: true,
                preserveState: true,
            });
        }
    }, [values]);
    return (
        <div className="flex items-center w-full max-w-md mr-4">
            <div className="relative flex w-full bg-white rounded shadow">
                <div
                    style={{ top: "100%" }}
                    className={`absolute ${opened ? "" : "hidden"}`}
                >
                    <div
                        onClick={() => setOpened(false)}
                        className="fixed inset-0 z-20 bg-black opacity-25"
                    ></div>
                    <div className="relative z-30 w-64 px-4 py-6 mt-2 bg-white rounded shadow-lg">
                        {filters.featured !== undefined && (
                            <SelectInput
                                label="Mise en avant"
                                name="featured"
                                value={values.featured}
                                onChange={handleChange}
                            >
                                <option value="all">Tous</option>
                                <option value="false">Non mise en avant</option>
                                <option value="true">
                                    Que les mise en avant
                                </option>
                            </SelectInput>
                        )}
                        {filters.published !== undefined && (
                            <SelectInput
                                label="Publié"
                                name="published"
                                value={values.published}
                                onChange={handleChange}
                            >
                                <option value="all">Tous</option>
                                <option value="false">Non Publié</option>
                                <option value="true">Que les publiés</option>
                            </SelectInput>
                        )}
                        {filters.role !== undefined && (
                            <SelectInput
                                label="Role"
                                name="role"
                                value={values.role}
                                onChange={handleChange}
                            >
                                <option value="all">Tous</option>
                                <option value="admin">Admistrateur</option>
                                <option value="user">Simple utilisateur</option>
                                <option value="editor">Editteur</option>
                            </SelectInput>
                        )}
                    </div>
                </div>
                {Object.keys(filters).length > 0 &&
                    (filters.featured !== undefined ||
                        filters.published !== undefined ||
                        filters.role !== undefined) && (
                        <button
                            onClick={() => setOpened(true)}
                            className="px-4 border-r rounded-l md:px-6 hover:bg-gray-100 focus:outline-none focus:border-white focus:ring-2 focus:ring-indigo-400 focus:z-10"
                        >
                            <div className="flex items-baseline">
                                <span className="hidden text-gray-700 md:inline">
                                    Filtrer
                                </span>
                                <svg
                                    className="w-2 h-2 text-gray-700 fill-current md:ml-2"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 961.243 599.998"
                                >
                                    <path d="M239.998 239.999L0 0h961.243L721.246 240c-131.999 132-240.28 240-240.624 239.999-.345-.001-108.625-108.001-240.624-240z" />
                                </svg>
                            </div>
                        </button>
                    )}
                <input
                    className="form-input relative w-full px-6 py-2 border-0 rounded-r focus:outline-none focus:ring-2 focus:ring-secondary/40"
                    autoComplete="off"
                    type="text"
                    name="search"
                    value={values.search}
                    onChange={handleChange}
                    placeholder="Rechercher..."
                />
            </div>
            <button
                onClick={reset}
                className="ml-3 text-sm text-gray-600 hover:text-gray-700 focus:text-secondary/70 focus:outline-none"
                type="button"
            >
                Reset
            </button>
        </div>
    );
}

export default SearchFilter;
