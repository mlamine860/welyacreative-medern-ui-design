import React from "react";

function Badge({ type, className, label }) {
    const badgeTypes = {
        success: "bg-green-200 text-green-600 ",
        danger: "bg-red-200 text-red-600 ",
        warning: "bg-yellow-200 text-yellow-600 ",
    };
    const classes = type
        ? (badgeTypes[type] += className)
        : (badgeTypes.success += className);
    return <div className={`px-2 py-1 rounded-full ${classes}`}>{label}</div>;
}

export default Badge;
