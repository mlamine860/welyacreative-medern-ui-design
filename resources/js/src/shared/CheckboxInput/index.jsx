import React from "react";

function CheckboxInput({ label, onChange, name, value, disabled }) {
    return (
        <div className="flex items-center ml-2">
            <input
                onChange={(e) => onChange(Number(e.target.checked))}
                disabled={disabled}
                checked={value}
                id={name}
                type="checkbox"
                value={value}
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
            />
            <label
                htmlFor={name}
                className="ml-2  font-medium"
            >
                {label}
            </label>
        </div>
    );
}

export default CheckboxInput;
