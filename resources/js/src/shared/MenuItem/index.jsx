import { Link, usePage } from "@inertiajs/react";
import classNames from "classnames";
import React from "react";
import Icon from "../Icon";

function MenuItem({ icon, link, text, className, nativeLink }) {
    console.log(nativeLink);
    const { url } = usePage();
    const base = location.pathname.split("/").filter((part) => part !== "")[1];
    const isActive = link.includes(base) || link.endsWith(url);
    const iconClasses = classNames("w-4 h-4 mr-2", {
        "text-white fill-current": isActive,
        "text-indigo-400 group-hover:text-white fill-current stroke-current":
            !isActive,
    });

    const textClasses = classNames({
        "text-white": isActive,
        "text-indigo-200 group-hover:text-white": !isActive,
    });
    return (
        <div className={`mb-1 ${className}`}>
            {nativeLink ? (
                <a href={link} className="flex items-center group py-2">
                    <Icon className={iconClasses} name={icon} />
                    <div className={textClasses}>{text}</div>
                </a>
            ) : (
                <Link href={link} className="flex items-center group py-2">
                    <Icon className={iconClasses} name={icon} />
                    <div className={textClasses}>{text}</div>
                </Link>
            )}
        </div>
    );
}

export default MenuItem;
