import React from "react";

function TeamIcon(props) {
    return (
        <svg
            {...props}
            viewBox="0 0 64 64"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            stroke="#000000"
        >
            <polygon points="54 48 10 48 8 22 56 22 54 48" />
            <polyline points="12 22 12 12 22 12 26 16 52 16 52 22" />
        </svg>
    );
}
export default TeamIcon;
