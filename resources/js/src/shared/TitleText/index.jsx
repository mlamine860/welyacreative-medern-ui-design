import React from "react";

function TitleText({ children, className, ...otherProps }) {
    return <h1 className={`mb-6 text-3xl font-bold text-secondary/60 ${className} form-error`}>{children}</h1>;
}

export default TitleText;
