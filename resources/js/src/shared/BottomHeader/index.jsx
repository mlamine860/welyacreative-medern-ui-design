import { Link, usePage } from "@inertiajs/react";
import React, { useState } from "react";
import { route } from "../../utils";
import Icon from "../Icon";

function BottomHeader() {
    const { auth } = usePage().props;
    const [menuOpened, setMenuOpened] = useState(false);

    return (
        <div className="flex items-center justify-between w-full p-4 text-sm bg-white border-b md:py-0 md:px-12 d:text-md">
            <div className="mt-1 mr-4"></div>
            <div className="relative">
                <div
                    className="flex items-center cursor-pointer select-none group"
                    onClick={() => setMenuOpened(true)}
                >
                    <div className="mr-1 text-gray-800 whitespace-nowrap group-hover:text-secondary focus:text-secondary">
                        <span>{auth.user.name}</span>
                        <span className="hidden ml-1 md:inline">
                            {/* {auth.user.last_name} */}
                        </span>
                    </div>
                    <Icon
                        className="w-5 h-5 text-gray-800 fill-current group-hover:text-secondary focus:text-secondary"
                        name="cheveron-down"
                    />
                </div>
                <div className={menuOpened ? "" : "hidden"}>
                    <div className="absolute top-0 right-0 left-auto z-20 py-2 mt-8 text-sm whitespace-nowrap bg-white rounded shadow-xl">
                        <Link
                            href={route("users.profile")}
                            className="block px-6 py-2 hover:bg-secondary hover:text-white"
                            onClick={() => setMenuOpened(false)}
                        >
                            Mon profile
                        </Link>
                        <Link
                            href={route("users.index")}
                            className="block px-6 py-2 hover:bg-secondary hover:text-white"
                            onClick={() => setMenuOpened(false)}
                        >
                            Gérer les utililisateurs
                        </Link>
                        <Link
                            as="button"
                            type="button"
                            href={route("admin.logout")}
                            className="block w-full px-6 py-2 text-left focus:outline-none hover:bg-secondary hover:text-white"
                            method="post"
                        >
                            Se déconnecter
                        </Link>
                    </div>
                    <div
                        onClick={() => {
                            setMenuOpened(false);
                        }}
                        className="fixed inset-0 z-10 bg-black opacity-25"
                    ></div>
                </div>
            </div>
        </div>
    );
}

export default BottomHeader;
