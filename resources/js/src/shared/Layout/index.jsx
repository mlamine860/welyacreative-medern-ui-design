import React from "react";
import Helmet from "react-helmet";
import BottomHeader from "../BottomHeader";
import MainMenu from "../MainMenu";
import TopHeader from "../TopHeader";
import FlashMessage from "../FlashMessage";

function Layout({ title, children }) {
    return (
        <div>
            <Helmet titleTemplate="%s | WelyaCreative" title={title} />
            <div className="flex flex-col">
                <div className="flex flex-col h-screen">
                    <div className="md:flex">
                        <TopHeader />
                        <BottomHeader />
                    </div>
                    <div className="flex flex-grow overflow-hidden">
                        <MainMenu className="flex-shrink-0 hidden w-56 p-12 overflow-y-auto bg-secondary/75 md:block" />
                        <div className="w-full px-4 py-4 overflow-hidden overflow-y-auto md:p-8 bg-secondary/5">
                            <FlashMessage />
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Layout;
