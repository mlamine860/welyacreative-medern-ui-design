import React from "react";

function SelectInput({
    name,
    label,
    errors,
    children,
    className,
    ...otherProps
}) {
    return (
        <div className={className}>
            {label && (
                <label className="form-label" htmlFor={name}>
                    {label}:
                </label>
            )}
            <select
                id={name}
                name={name}
                {...otherProps}
                className={`form-select ${errors?.length ? "error" : ""}`}
            >
                {children}
            </select>
            {errors && <div className="form-error">{errors}</div>}
        </div>
    );
}

export default SelectInput;
