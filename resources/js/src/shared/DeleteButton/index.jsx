import React from "react";

function DeleteButton({ children, onDelete }) {
    return (
        <button
            className="text-red-600 focus:outline-none hover:underline"
            tabIndex="-1"
            type="button"
            onClick={onDelete}
        >
            {children}
        </button>
    );
}

export default DeleteButton;
