import { createInertiaApp } from "@inertiajs/react";
import { createRoot } from "react-dom/client";


createInertiaApp({
    resolve: (name) => {
        const pages = import.meta.glob("./src/pages/**/*.jsx", { eager: true });
        return pages[`./src/${name}/index.jsx`];
    },
    setup({ el, App, props }) {
        createRoot(el).render(<App {...props} />);
    },
});
