<x-app-layout>
    @section('title')
        | Politique de confidentialité
    @endsection
    <section class="policy py-32">
        <div class="container">
            <div class="py-6">
                <h1>{{ $page->title }}</h1>
            </div>
            <div>
                {!! $page->content !!}
            </div>
        </div>
    </section>
</x-app-layout>
