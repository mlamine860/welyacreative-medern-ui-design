<x-app-layout>
    @section('title')
        | {{ $service->name }}
    @endsection
    <header
        class="py-[150px] md:h-4/5 grid place-items-center bg-no-repeat bg-cover relative
     after:absolute after:content-[''] after:inset-0 after:bg-black after:opacity-50 z-0"
        style="background-image: url('{{ $service->imageLarge }}')">
        <div class="container relative z-10 grid  place-items-center">
            <div class="md:w-4/6">
                <h1 class="text-6xl md:text-8xl text-white leading-tight mb-8">{{ $service->name }}</h1>
                <p class="text-[18px]  text-slate-200 leading-relaxed mb-8">
                    {{ $service->description }}
                </p>
                <a href="{{ route('pages.contact') }}" class="btn-primary inline-block">Obtenez une estimation
                    gratuite</a>
            </div>
        </div>
    </header>
    <section class="content py-16 md:py-32" id="content">
        <div class="container">
            <div class="raw-content max-w-7xl m-auto">
                {!! $service->content !!}
            </div>
        </div>
    </section>

    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>

    @include('partials.newsletter')
</x-app-layout>
