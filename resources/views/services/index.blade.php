<x-app-layout>
    @section('title')
        | Services
    @endsection

    <header class="py-[150px]  md:h-4/5 grid place-content-center bg-no-repeat bg-cover"
        style="background-image: url('{{ asset('images/service-image-banner-01.jpeg') }}')">
        <div class="container">
            <h1 class="text-6xl md:text-8xl text-white leading-tight mb-8">AUGMENTER LES REVENUS <br> ET AUGMENTER LE
                TRAFIC.</h1>
            <p class="text-[18px]  text-slate-200 leading-relaxed mb-8">
                Augmentez la rentabilité, la disponibilité et l'efficacité de votre entreprise <br>
                grâce aux solutions de développement Web pertinentes <br>
                grâce à l'architecture évolutive utilisant les dernières technologies et
                tendances.
            </p>
            <a href="" class="btn-primary inline-block">Voire notre portefolio</a>
        </div>
    </header>

    <!-- Services -->
    <section class="section services">
        <div class="container">
            <div class="grid  md:grid-cols-2 lg:grid-cols-4 gap-8">
                @foreach ($services as $service)
                    <div class="card group">
                        <div class="card-icon">
                            <a href="{{ route('services.detail', $service) }}">
                                <img src="{{ $service->icon }}" width="64" height="64"
                                    title="{{ $service->name }}" alt="{{ $service->name }}">
                            </a>
                        </div>
                        <a href="{{ route('services.detail', $service) }}">
                            <h3 class="text-ssp text-fs-5 font-500 text-center mb-[15px] text-secondary">
                                {{ $service->name }}</h3>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>

    @include('partials.newsletter')
</x-app-layout>
