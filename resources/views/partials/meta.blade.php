<!-- Primary Meta Tags -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<meta name="title" content="WelyaCreative">
<meta name="author" content="WelyaCreative">
<meta name="description"
    content="Nous aidons votre entreprise à croître! Nous sommes une agence à service complet qui fournit des services tels que le marketing digital, la conception et le développement Web, la conception et le développement d'applications, la conception graphique!">
<meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large" />
<meta name="keywords"
    content="Société de conception Web en Guinée à Conakry à Fria, Meilleur cabinet de conception Web, Services de conception Web en Guinée, Société de développement Web en Guinée Conakry, NY Web Design Company, Agence Web Marketing en Guinée Conakry, Web designer Guinée Conakry, Développeurs Web en Guinée à Conakry, Agence marketing digital en Guinée Conakry, Agence de graphique design en Guinée Conakry, Agence Social médias manager en Guinée Conakry ">
<link rel="canonical" href="https://www.welyacreative.com" />

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://www.welyacreative.com">
<meta property="twitter:title" content="Fournisseur de services web et de marketing numérique">
<meta property="twitter:description"
    content="Nous aidons votre entreprise à croître! Nous sommes une agence à service complet qui fournit des services tels que le marketing digital, la conception et le développement Web, la conception et le développement d'applications, la conception graphique!">
<meta property="twitter:image" content="{{ asset('images/welyacrative.png') }}">
<meta property="og:image" content="{{ asset('images/welyacrative.png') }}">
<meta name="og:site_name" property="og:site_name" content="WelyaCreative">
<meta property="og:locale" content="fr_FR">
@section('meta_og')
    <meta property="og:type" content="website">
    <meta property="og:title" content="Fournisseur de services web et de marketing numérique">
    <meta property="og:description"
        content="Nous aidons votre entreprise à croître! Nous sommes une agence à service complet qui fournit des services tels que le marketing digital, la conception et le développement Web, la conception et le développement d'applications, la conception graphique!" />
    <meta property="og:url" content="https://www.welyacreative.com">
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}">
