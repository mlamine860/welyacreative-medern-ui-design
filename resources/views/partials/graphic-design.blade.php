<section class="content py-16" id="content">
    <div class="container pb-8 md:pb-32">
        <h2 class="text-fs-2 text-center">Nous concevons l'expérience</h2>
        <p>WelyaCreative accompagne les marques à véhiculer une image forte, rassurante et incitative à l’achat afin que
            votre cible vous fasse confiance. Notre agence conseil en communication mets en place les actions de
            communication liées à votre stratégie marketing : branding, identité graphique. Une communication efficace
            montre votre expertise et votre positionnement de spécialiste de votre secteur économique.</p>
        <p>En tant qu'agence de Com, Nous créons des identités graphiques adaptées à 100% à vos objectifs. Notre équipe
            de graphiste réalise votre identité de A à Z en partant de l’analyse de vos besoins, jusqu’à la production
            des supports de communications. A partir de vos couleurs, de votre logo actuel ou encore de votre identité,
            nous nous adaptons à la demande afin de fournir un travail de qualité. Le but est de refléter du sérieux et
            de la cohérence dans votre marque.</p>

        <div class="py-16">
            <h3 class="text-fs-5">Notre expertise </h3>
            <ul>
                <li>Création d’une identité visuelle adapté</li>
                <li>Choix des couleurs lié au secteur d’activité</li>
                <li>Définitions des typographies de la marque</li>
                <li>Uniformisation des supports</li>
                <li>Création d’une charte graphique donnant le guide de bonne utilisation de l’identité</li>
            </ul>

            <h3 class="text-fs-5 mt-6">Identité visuelle</h3>
            <ul>
                <li>Une identité visuelle doit être simple, forte et unique</li>
                <li>WelyaCreative crée pour vous l’expérience utilisateur (UX design) qui ancrera votre marque dans
                    l’esprit de vos clients</li>
                <li>Définitions des typographies de la marque</li>
                <li>De la création graphique de votre logo à la conception du web design de votre site pour être
                    compatible responsive design, nos graphistes vous proposent la meilleure image de marque qu’il soit
                </li>
            </ul>

            <h3 class="text-fs-5 mt-6">Charte graphique</h3>
            <p>A partir de vos objectifs et des valeurs de votre entreprise, notre agence de communication travaille à
                établir une charte graphique précise et l'utiliser pour créer des visuels adaptés à tous les supports :
            </p>
            <ul class="py-4">
                <li>
                    Création d'un logo
                </li>
                <li>Conception de maquettes graphiques pour le web design de votre site ou de votre application mobile
                </li>
                <li>Création de visuels pour les publicités et les posts sur les réseaux sociaux</li>
            </ul>
            <p>Notre équipe est polyvalente et nous nous concentrons sur la recherche de solutions. Nous apprécions les
                mémoires complexes et nuancés et utilisons une approche intégrée et interdisciplinaire. Cela crée des
                solutions de marque innovantes qui se connecteront avec les clients et livreront commercialement.</p>
        </div>

    </div>
    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>
</section>
