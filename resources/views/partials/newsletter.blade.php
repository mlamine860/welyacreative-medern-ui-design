  <!-- Newsletter -->
  <div class="container pt-[60px] mb-4">
    <div class="card shadow-4xl">
        <h3 class="text-ssp text-fs-4 text-secondary font-700">Suscrivez au newsletter</h3>
        <p class="text-fs-8 mb-[15px]">Vous voulez que nous vous envoyions occasionnellement des e-mails
            avec des nouvelles de WelyaCreative ?</p>
        <form x-data="newsletter" @submit="submitHandler">
            <div class="mb-[10px]">
                <div class="flex bg-gray-200 px-[10px] py-[1px] rounded-xl">
                    <input x-model="email" type="text" name="email" id="email" placeholder="exemple@domain.com"
                        class="self-center w-full bg-transparent text-fs-8 border-0 placeholder:text-fs-8 focus:outline-none focus:ring-0">
                    <button class="btn-primary">Souscrire</button>
                </div>
            </div>
        </form>
    </div>
</div>