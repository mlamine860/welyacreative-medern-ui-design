<!-- Header -->
<header class="bg-white py-4 fixed top-0 left-0 w-full z-40 transitidarnei-kah-mein.png.5 header" data-header>
    <div class="container flex justify-between items-center gap-[30px]">
        <div class="fixed inset-0 z-1 bg-black_70 transition-2 opacity-0 pointer-events-none overlay md:hidden"
            data-overlay>
        </div>
            <a href="{{ route('welcome') }}">
                <img src="{{ asset('images/logo.svg') }}" class="w-[200px]" alt="Logo">
            </a>
        <nav class="bg-white fixed left-[-280px]  top-0 w-full max-w-[280px] p-12 h-full transtion-2 navbar lg:static lg:visible"
            data-navbar>
            <div class="lg:hidden flex justify-between items-center pt-[10px] pb-[30px]">
                <a href="{{ route('welcome') }}"><img src="{{ asset('images/logo.svg') }}" class="w-[160px]"
                        alt="Logo"></a>
                <button class="p-1" aria-label="Close Menu" data-nav-close-btn>
                    <svg class="text-4xl" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                        height="24">
                        <path fill="none" d="M0 0h24v24H0z" />
                        <path
                            d="M12 10.586l4.95-4.95 1.414 1.414-4.95 4.95 4.95 4.95-1.414 1.414-4.95-4.95-4.95 4.95-1.414-1.414 4.95-4.95-4.95-4.95L7.05 5.636z" />
                    </svg>
                </button>
            </div>
            <ul class="nav-list">
                <li class="nav-list-item">
                    <a href="{{ route('welcome') }}" class="nav-list-item-link {{ set_active_class('welcome') }}"
                        data-navbar-link>Accueil</a>
                </li>

                <li class="nav-list-item flex flex-wrap  gap-1 items-center relative">
                    <a href="{{ app_route('services.list', '#services') }}"
                        class="nav-list-item-link {{ set_active_class('services.list') }}" data-navbar-link>
                        Services
                    </a>
                    <span class="cursor-pointer" data-toggle-dropdown>
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                            width="24" height="24">
                            <path fill="none" d="M0 0h24v24H0z" />
                            <path d="M12 13.172l4.95-4.95 1.414 1.414L12 16 5.636 9.636 7.05 8.222z" />
                        </svg>
                    </span>
                    <ul class="md:absolute md:top-full md:shadow-2xl md:rounded-lg bg-white p-2 md:min-w-[700px] md:p-8 hidden"
                        data-nav-dropdown>
                        <div class="md:grid grid-cols-2 gap-4 pt-2 mb-8">
                            @foreach ($featuredServices as $service)
                                <li class="hidden card md:flex">

                                    <a href="{{ route('services.detail', $service) }}"
                                        class="flex items-center gap-4 hover:text-primary">
                                        <img src="{{ $service->icon }}" alt="" width="24" height="24">
                                        {{ $service->name }}
                                    </a>
                                </li>
                                <li class="py-2 md:hidden border-b last:border-0">
                                    <a href="{{route('services.detail', $service)}}"
                                        class="text-body hover:text-primary transition">{{ $service->name }}</a>
                                </li>
                            @endforeach
                        </div>
                    </ul>
                </li>

                <li class="nav-list-item">
                    <a href="{{ app_route('portfolio', '#portfolio') }}"
                        class="nav-list-item-link {{ set_active_class('portfolio') }}" data-navbar-link>Portfolio</a>
                </li>

                <li class="nav-list-item">
                    <a href="{{ app_route('blog', '#blog') }}"
                        class="nav-list-item-link {{ set_active_class('blog') }}" data-navbar-link>Blog</a>
                </li>

                <li class="nav-list-item">
                    <a href="{{ app_route('pages.contact', '#contact') }}"
                        class="nav-list-item-link {{ set_active_class('pages.contact') }}" data-navbar-link>Contact</a>
                </li>

                <li class="navbar-item">
                    <a href="{{ app_route('about-us', '#about') }}"
                        class="nav-list-item-link {{ set_active_class('about-us') }}" data-navbar-link>Apropos</a>
                </li>

            </ul>

        </nav>
        <a href="{{ route('pages.contact') }}" class="btn-primary hidden md:flex">
            <svg class="fill-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                height="24">
                <path fill="none" d="M0 0h24v24H0z" />
                <path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z" />
            </svg>
            <span>Obtenir un devis</span>
        </a>
        <button class="lg:hidden" aria-label="Open Menu" data-nav-open-btn>
            <svg class="fill-st-patricks-blue" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="32"
                height="32">
                <path fill="none" d="M0 0h24v24H0z" />
                <path d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z" />
            </svg>
        </button>

    </div>
</header>
