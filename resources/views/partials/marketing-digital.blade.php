<section class="content py-16" id="content">
    <div class="container pb-8 md:pb-32">
        <h2 class="text-fs-3 mb-8 text-center">Tirez parti de la puissance du marketing digital
            <br> pour faire grandir votre entreprise
        </h2>
        <div class="flex flex-col lg:flex-row gap-4 py-8 md:py-16">
            <div>
                <p>La quasi-totalité des entreprises possèdent aujourd’hui un site Internet, outil
                    indispensable pour être
                    visible et pour se conformer aux attentes des consommateurs.</p>
                <p>Très vite, le développement des algorithmes des moteurs de recherche a révélé qu’un site
                    Internet seul
                    n’allait pas se suffire à lui-même. Pour se faire connaître sur la Toile et cibler le
                    consommateur, des
                    stratégies se sont développées au fur et à mesure, englobées dans ce que l’on nomme ‘’marketing
                    digital’’.</p>
            </div>
            <img src="{{ asset('images/marketing.jpg') }}" width="640" height="1520" alt="Marketing">

        </div>
        <p>
            À l’aide d’une stratégie webmarketing basée sur l’étude de vos personae, welyaCreative
            s’appuiera sur l’ensemble des canaux de diffusions marketing et sur les différents supports ;
            ordinateur, tablette, mobile ; pour développer vos objectifs.
        </p>
        <p>
            Soucieux de votre projet et de votre développement, nos experts webmarketing vous fourniront un
            accompagnement haut de gamme pour répondre à l’ensemble de vos demandes et anticiper vos
            besoins.
        </p>
        <p>Dès la conception de votre site internet à forte valeur ajoutée, nous établissons une stratégie
            webmarketing afin d’optimiser les résultats et la visibilité de votre futur support web. Une
            étude approfondie de la concurrence sur les différents canaux d’acquisition et une étude de
            votre cible nous permettent d’anticiper les facteurs bloquants à la conversion de vos objectifs
            et d’imiter le comportement de vos utilisateurs.</p>
        <p>Nous installerons tous les outils adéquats (Google Tag Manager, Google Search Console, Google
            Analytics, Hodjar, etc.) pour étudier votre taux de conversion et les autres indicateurs de
            performance pertinents. Ainsi nous serons en mesure de calculer votre retour sur investissement
            – ROI. Vous recevrez chaque mois un reporting complet de nos actions ainsi pour celles à venir.
        </p>

        <div class="flex flex-col lg:flex-row gap-4 py-8 md:py-16">
            <div>
                <h3 class="text-fs-5">Référencement seo/sea</h3>
                <p>Le référencement web est indispensable pour augmenter le trafic vers votre site : un bon
                    référencement permet de se positionner dans les premiers résultats des moteurs de recherche.</p>
                <p>Cependant la notion élargie englobe également toutes les actions menées pour attirer du trafic et
                    influer sur le nombre de visiteurs.

                    Nous connaissons les leviers pour développer la meilleure stratégie de référencement naturel
                    possible en cohérence avec vos objectifs de ciblage.</p>

            </div>
            <img src="{{ asset('images/seo.jpg') }}" width="640" height="1520" alt="SEO">
        </div>
        <div class="py-8 md:py-16">
            <h3 class="text-fs-4 mb-6 text-center">Nos meilleures pratiques de marketing numérique</h3>
            <p class="text-center text-fs-7">Comment nous développons la notoriété de votre marque et votre clientèle
            </p>
            <div class="py-8 md:py-16" x-data="serviceTab">
                <ul class="flex flex-col md:flex-row gap-4 items-center justify-between">
                    <li  x-init="activeTab = $el" class="text-fs-7 text-secondary fontbold p-4  border-b-2 border-primary cursor-pointer"
                        @click="setTab('google-add')">
                        Utilisation de Google Add
                    </li>
                    <li class="text-fs-7 text-secondary fontbold cursor-pointer border-primary p-4" @click="setTab('inbound-marketing')">
                        Inbound Marketing
                    </li>
                    <li class="text-fs-7 text-secondary fontbold cursor-pointer border-primary p-4" @click="setTab('landing')">
                        Attirer
                    </li>
                    <li class="text-fs-7 text-secondary fontbold cursor-pointer border-primary p-4" @click="setTab('convert')">
                        Convertir
                    </li>
                    <li class="text-fs-7 text-secondary fontbold cursor-pointer border-primary p-4" @click="setTab('sell')">
                        Vendre
                    </li>
                    <li class="text-fs-7 text-secondary fontbold cursor-pointer border-primary p-4" @click="setTab('retain')">
                        Fideliser
                    </li>
                </ul>
                <div class="flex no-wrap p-8" x-ref="tabContent">
                    <div x-init="current = $el" x-ref="google-add">
                        <p>
                            Les bonnes pratiques et la mise en place de google Add sont d’excellents processus pour
                            amener
                            du
                            monde sur votre site internet et augmenter le trafic.
                        </p>
                        <p>
                            Le référencement payant est un excellent moyen d’être présent partout, de gagner en
                            visibilité
                            et
                            surtout une place sur le marché.
                        </p>
                    </div>
                    <div class="hidden" x-ref="inbound-marketing">
                        <p>
                            L'inbound marketing repose sur une stratégie de création de contenu qui permet d'attirer des
                            visiteurs afin de les convertir en leads puis en clients, grâce à des techniques telles que
                            le marketing automation, et la création de contenu.
                        </p>
                        <p>L'outbound marketing repose sur l'utilisation de publicités non-ciblées et intrusives.
                            L'inbound marketing a pour objectif de générer des prospects en leur proposant du contenu
                            intéressant, adapté à leur position dans le processus d'achat.</p>
                    </div>
                    <div class="hidden" x-ref="landing">
                        <p>
                            La première étape d'une bonne stratégie inbound marketing est d'être capable d'attirer des
                            visiteurs (et des clients potentiels) sur votre site web.
                        </p>
                    </div>
                    <div class="hidden" x-ref="convert">
                        <p>
                            Une fois que les prospects qui vous intéressent ont visité votre site, il est temps de les
                            conduire vers l'étape suivante du processus d'achat et de les convertir en leads.
                        </p>
                    </div>
                    <div class="hidden" x-ref="sell">
                        <p>
                            Maintenant que les leads sont identifiés, il faut passer à l'étape suivante : les convertir
                            en clients. C'est l'étape où le marketing automation, le lead scoring et les rapports en
                            boucle fermée entrent en jeu.
                        </p>
                    </div>
                    <div class="hidden" x-ref="retain">
                        <p>
                            La fidélisation est la phase finale de la méthodologie inbound marketing et cette phase est
                            bien souvent ignorée ou sous-évaluée. Pourtant, acquérir de nouveaux clients coûterait près
                            de 7 fois plus cher que de les retenir.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>
</section>
