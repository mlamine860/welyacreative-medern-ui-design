<section class="content py-16 md:py-32" id="content">
    <div class="container pb-8 md:pb-32">
        <div class="grid place-items-center pb-16">
            <h2 class="text-fs-3 md:text-fs-1">Votre application web sur-mesure</h2>
            <p class="md:w-4/6 md:text-center text-fs-7">Nous sommes passionnés par les challenges tech posés par les
                applications web et SaaS, que nous développons 100% sur-mesure selon vos besoins.</p>
            <a href="{{ route('contact') }}" class="btn-primary my-8">Parlez-nous de votre projet</a>
        </div>
        <h2 class="text-fs-5 text-center mt-8 md:mt-32">Notre expertise est centrée sur 4 pôles</h2>
        <div class="flex flex-col gap-4 md:flex-row justify-between py-8">
            <div>
                <h4 class="text-fs-6 text-secondary">Software-as-a-Service</h4>
                <ul>
                    <li>Extraction et consolidation de données</li>
                    <li>Schémas de base de données personnalisés</li>
                    <li>Fonctionnalités métier sur-mesure</li>
                </ul>
            </div>

            <div>
                <h4 class="text-fs-6 text-secondary">Back office</h4>
                <ul>
                    <li>Administration des processus métiers</li>
                    <li>Tableaux de bord statistiques</li>
                    <li>Intégration aux outils métier : ERP, CRM, comptabilité...</li>
                </ul>
            </div>
        </div>
        <div class="flex flex-col gap-4 md:flex-row justify-between py-8 md:py-32">
            <div class="">
                <h4 class="text-fs-6 text-secondary">Marketplaces</h4>
                <ul>
                    <li>Interface sur-mesure bi-partite</li>
                    <li>Gestion des commandes et des devis</li>
                    <li>Intégration des modes de paiements</li>
                </ul>
            </div>
            <div class="">
                <h4 class="text-fs-6 text-secondary">Réseaux sociaux</h4>
                <ul>
                    <li>Interface utilisateur personnalisée</li>
                    <li>Fontionnalités de partage (ex. Article)</li>
                    <li>Intégration de messageries</li>
                    <li>Systèmes de notifications</li>
                </ul>
            </div>

        </div>
        <h3 class="text-fs-5 text-secondary md:text-center mt-16">
            Nous utilisons les technologies les plus adaptées à vos besoins
        </h3>
        <p class="text-fs-8 md:text-center">
            Nous adaptons notre stratégie de développement à vos besoins et votre budget.
            Nous n’utilisons que des solutions de références du marché, tant en développement sur-mesure
            que pour les solutions clé-en-main.
        </p>
        <div class="py-8 md:py-32">
            <h3 class="text-fs-5">Backend</h3>
            <div class="grid grid-cols-3 md:grid-cols-5 gap-8">
                <div class="card">
                    <img src="{{ asset('images/icons/php.svg') }}" alt="PHP Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/nodejs.svg') }}" alt="NODEJS Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/python.svg') }}" alt="PYTHON Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/laravel.svg') }}" alt="LARAVEL Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/django.svg') }}" alt="DJANGO Icon">
                </div>
            </div>
        </div>

        <div>
            <h3 class="text-fs-5">Fontend</h3>
            <div class="grid grid-cols-3 md:grid-cols-5 gap-8">
                <div class="card">
                    <img src="{{ asset('images/icons/reactjs.svg') }}" alt="REACT Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/vuejs.svg') }}" alt="VUEJS Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/nextjs.svg') }}" alt="NEXTJS Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/angular.svg') }}" alt="ANGULAR Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/typescript.svg') }}" alt="TYPESCRIPT Icon">
                </div>
            </div>
        </div>
        <div class="py-8 md:py-32">
            <h3 class="text-fs-5">Base de données</h3>
            <div class="grid grid-cols-3 md:grid-cols-4 gap-8">
                <div class="card">
                    <img src="{{ asset('images/icons/postgresql.svg') }}" alt="POSTGREQL Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/mysql.svg') }}" alt="MYSQL Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/mongodb.svg') }}" alt="MONGODB Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/mariadb.svg') }}" alt="MARIADB Icon">
                </div>
            </div>
        </div>
        <div>
            <h3 class="text-fs-5">Plateforme</h3>
            <div class="grid grid-cols-3 gap-8">
                <div class="card">
                    <img src="{{ asset('images/icons/amazon-ar21.svg') }}" alt="AWS Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/google-cloud.svg') }}" alt="GOOGLE CLOUD Icon">
                </div>

                <div class="card">
                    <img src="{{ asset('images/icons/digitalocean.svg') }}" alt="DIGITALOCEAN Icon">
                </div>

            </div>
        </div>
    </div>
    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>
</section>
