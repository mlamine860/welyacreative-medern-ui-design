<section class="content py-16 md:py-32" id="content">
    <div class="container pb-8 md:pb-32">
        <h2 class="text-fs-3 text-center">Le développement de nos applications repose sur 4 facteurs de succès</h2>
        <div class="flex flex-col gap-4 md:flex-row justify-between py-8">
            <div>
                <h4 class="text-fs-6 text-secondary">Performance</h4>
                <ul>
                    <li>Fluidité d’utilisation</li>
                    <li>Fiabilité dans la durée</li>
                </ul>
            </div>

            <div>
                <h4 class="text-fs-6 text-secondary">Sobriété</h4>
                <ul>
                    <li>Limitation de la consommation de batterie</li>
                    <li>Poids de l’application maîtrisé</li>
                </ul>
            </div>
        </div>
        <div class="flex flex-col gap-4 md:flex-row justify-between py-8 md:py-32">
            <div class="">
                <h4 class="text-fs-6 text-secondary">Disponibilité</h4>
                <ul>
                    <li>Disponibilité sous iOS et Android</li>
                    <li>Expérience utilisateur commune</li>
                    <li>Intégration des modes de paiements</li>
                </ul>
            </div>
            <div class="">
                <h4 class="text-fs-6 text-secondary">Ergonomie et design</h4>
                <ul>
                    <li>Optimisation de l’UX</li>
                    <li>Design sur-mesure</li>

                </ul>
            </div>

        </div>
        <h3 class="text-fs-5 text-secondary md:text-center mt-16">
            Nous utilisons les technologies les plus adaptées à vos besoins
        </h3>
        <p class="text-fs-8 md:text-center">
            Nous adaptons notre stratégie de développement à vos besoins et votre budget.
            Nous n’utilisons que des technologies de référence du marché en développement mobile.
        </p>
        <div class="py-8 md:py-32">
            <div class="grid grid-cols-3  gap-8">
                <div class="card">
                    <img src="{{ asset('images/icons/flutter.svg') }}" alt="PHP Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/reactjs.svg') }}" alt="NODEJS Icon">
                </div>
                <div class="card">
                    <img src="{{ asset('images/icons/nodejs.svg') }}" alt="NODEJS Icon">
                </div>

            </div>
        </div>
    </div>
    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>
</section>
