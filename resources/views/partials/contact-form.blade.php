<form class="relative" id="contact-use" x-data="contactForm" @submit="submitHandler">
    <div x-transition x-show="toast" x-ref="alertmessagebox"
        class="fixed overflow-hidden z-50 top-44 right-5 items-center w-full max-w-xl p-4 space-x-8 text-gray-100 bg-gradient-primary rounded-lg shadow-xl dark:text-gray-400 dark:divide-gray-700 space-x dark:bg-gray-800 hidden"
        role="alert">
        <div class="h-2 bg-blue-600 absolute left-0 bottom-0 transition-colors" :style="toastWidthStyle"></div>
        <svg aria-hidden="true" class="w-16 h-16  text-blue-600 dark:text-blue-500" focusable="false" data-prefix="fas"
            data-icon="paper-plane" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path fill="currentColor"
                d="M511.6 36.86l-64 415.1c-1.5 9.734-7.375 18.22-15.97 23.05c-4.844 2.719-10.27 4.097-15.68 4.097c-4.188 0-8.319-.8154-12.29-2.472l-122.6-51.1l-50.86 76.29C226.3 508.5 219.8 512 212.8 512C201.3 512 192 502.7 192 491.2v-96.18c0-7.115 2.372-14.03 6.742-19.64L416 96l-293.7 264.3L19.69 317.5C8.438 312.8 .8125 302.2 .0625 289.1s5.469-23.72 16.06-29.77l448-255.1c10.69-6.109 23.88-5.547 34 1.406S513.5 24.72 511.6 36.86z">
            </path>
        </svg>
        <div class="pl-4 text-fs-8 font-normal">
            Nous apprécions que vous nous contactiez. Un de nos collègues vous recontactera bientôt !
        </div>
    </div>
    <div class="flex flex-col md:flex-row gap-4">
        <x-forms.input error="errors.username" name="fields.username" label="Votre nom" placeholder="John Doe">
            <svg class="fill-body  mt-[5px]" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20"
                height="20">
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                    d="M4 22a8 8 0 1 1 16 0h-2a6 6 0 1 0-12 0H4zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6zm0-2c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4z" />
            </svg>
        </x-forms.input>
        <x-forms.input name="fields.email" error="errors.email" label="Votre email" placeholder="exemple@domain.com">
            <svg class="fill-body  mt-[5px]" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                height="24">
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                    d="M3 3h18a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm17 4.238l-7.928 7.1L4 7.216V19h16V7.238zM4.511 5l7.55 6.662L19.502 5H4.511z" />
            </svg>
        </x-forms.input>
    </div>
    <div class="flex flex-col md:flex-row gap-4">
        <x-forms.input name="fields.phone" error="errors.phone" label="Votre numéro" placeholder="+224 600 00 00 00">
            <svg class="fill-body  mt-[5px]" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                height="24">
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                    d="M9.366 10.682a10.556 10.556 0 0 0 3.952 3.952l.884-1.238a1 1 0 0 1 1.294-.296 11.422 11.422 0 0 0 4.583 1.364 1 1 0 0 1 .921.997v4.462a1 1 0 0 1-.898.995c-.53.055-1.064.082-1.602.082C9.94 21 3 14.06 3 5.5c0-.538.027-1.072.082-1.602A1 1 0 0 1 4.077 3h4.462a1 1 0 0 1 .997.921A11.422 11.422 0 0 0 10.9 8.504a1 1 0 0 1-.296 1.294l-1.238.884zm-2.522-.657l1.9-1.357A13.41 13.41 0 0 1 7.647 5H5.01c-.006.166-.009.333-.009.5C5 12.956 11.044 19 18.5 19c.167 0 .334-.003.5-.01v-2.637a13.41 13.41 0 0 1-3.668-1.097l-1.357 1.9a12.442 12.442 0 0 1-1.588-.75l-.058-.033a12.556 12.556 0 0 1-4.702-4.702l-.033-.058a12.442 12.442 0 0 1-.75-1.588z" />
            </svg>
        </x-forms.input>
        <x-forms.input name="fields.address" error="errors.address" label="Votre adresse" placeholder="Conakry, Rue 16">
            <svg class="fill-body  mt-[5px]" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                height="24">
                <path fill="none" d="M0 0h24v24H0z" />
                <path
                    d="M12 20.9l4.95-4.95a7 7 0 1 0-9.9 0L12 20.9zm0 2.828l-6.364-6.364a9 9 0 1 1 12.728 0L12 23.728zM12 13a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm0 2a4 4 0 1 1 0-8 4 4 0 0 1 0 8z" />
            </svg>
        </x-forms.input>
    </div>
    <x-forms.input name="fields.subject" error="errors.subject" label="Le sujet" placeholder="" />
    <x-forms.textarea name="fields.message" error="errors.message" label="Votre message" />
    <div class="flex justify-end mt-[15px]">

        <button type="submit" class="btn-primary">
            <template x-if="!loading">
                <span>Envoyez</span>
            </template>
            <template x-if="loading">
                <svg class="animate-spin h-8 w-8 mr-3 fill-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                    width="24" height="24">
                    <path fill="none" d="M0 0h24v24H0z" />
                    <path
                        d="M5.463 4.433A9.961 9.961 0 0 1 12 2c5.523 0 10 4.477 10 10 0 2.136-.67 4.116-1.81 5.74L17 12h3A8 8 0 0 0 6.46 6.228l-.997-1.795zm13.074 15.134A9.961 9.961 0 0 1 12 22C6.477 22 2 17.523 2 12c0-2.136.67-4.116 1.81-5.74L7 12H4a8 8 0 0 0 13.54 5.772l.997 1.795z" />
                </svg>
                Traitement...
            </template>
        </button>
    </div>

</form>
