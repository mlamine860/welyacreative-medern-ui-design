@foreach ($services as $service)
    <div class="shadow-xl hover:shadow-2xl group border rounded-lg transition">
        <a href="{{ route('services.detail', $service) }}" class="p-8">
            <div class="card-icon">
                <img src="{{ $service->icon }}" width="64" height="64" title="{{ $service->name }}"
                    alt="{{ $service->name }}">
            </div>
            <h3 class="text-ssp text-fs-7 font-700 text-center mb-[15px] text-secondary hover:text-primary">
                {{ $service->name }}</h3>
        </a>
    </div>
@endforeach
