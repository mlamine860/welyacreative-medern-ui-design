@push('scripts')
    <script>
        const categorySelect = document.getElementById('category-select')
        categorySelect.addEventListener('change', ({
            target
        }) => {
            const {
                value: catId
            } = target
            const url = "{{ route('blog') }}/" + (catId ? `category/${catId}` : "")
            location.replace(url)
        })
    </script>
@endpush

<x-app-layout>
    @section('title')
        | Blog
    @endsection
    @if ($posts->isEmpty())
    <section class="py-[120px]">
        <div class="container">
            <h3 class="text-center">Aucun article pour le moment</h3>
        </div>
    </section>
    @else
        <section class="pt-[120px]">
            <div class="container">
                <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                    <div class="flex flex-col gap-8">
                        <div class="flex gap-16">
                            <div class="text-gray-500 uppercase">{{ $latestPost->created_at->isoFormat('LL') }}</div>
                            <div class="text-gray-500 uppercase">Auteur {{ $latestPost->author->name }}</div>
                        </div>
                        <h1 class="text-fs-3 md:text-fs-2">{{ $latestPost->title }}</h1>
                        <a href="{{ route('posts.detail', $latestPost) }}" class="uppercase font-bold text-primary">Voire
                            plus</a>
                    </div>
                    <img src="{{ $latestPost->imageMedium }}" class="w-full" width="420" height="320"
                        alt="">
                </div>
            </div>
        </section>
        <section class="block-list py-32">
            <div class="container">
                <div class="flex items-center justify-end py-16">
                    <span class="mr-2">Catégorie:</span>
                    <select name="category" id="category-select" class="py-4 text-fs-8 rounded shadow">
                        <option value="">Toutes les catégories</option>
                        @foreach ($categories as $cat)
                            <option value="{{ $cat->id }}" {{ $catId === $cat->id ? 'selected' : '' }}>
                                {{ $cat->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
                    @foreach ($posts as $post)
                        <a href="{{ route('posts.detail', $post) }}" class="flex flex-col justify-between border">
                            <img src="{{ $post->imageMedium }}" class="h-[250px]" alt="">
                            <div class="px-6 pb-4 rounded">
                                <h4 class="text-fs-6 text-secondary my-4 hover:text-primary">{{ $post->title }}</h4>
                                <div class="flex justify-between py-2">
                                    <div class="text-gray-500 uppercase text-xl">
                                        {{ $post->created_at->isoFormat('LL') }}
                                    </div>
                                    <div class="text-gray-500 uppercase text-xl">Auteur {{ $post->author->name }}</div>
                                </div>
                                <p class="mb-8">
                                    {{ $post->excerpt }}
                                </p>
                                <div class="uppercase font-medium text-primary">Voire plus</div>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="py-16">
                    {{ $posts->links('vendor.pagination.tailwind') }}
                </div>
            </div>
        </section>
    @endif


    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>
    @include('partials.newsletter')
</x-app-layout>
