<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('partials.meta')
    @yield('meta_og')
    <title>
        WelyaCreative @yield('title')
    </title>
    <!--
    -google font link
     -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&family=Source+Sans+Pro:wght@600;700&display=swap"
        rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @stack('head')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7KCG15W6YV"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-7KCG15W6YV');
    </script>
</head>

<body class="relative w-full h-full text-[1.6rem] text-body bg-white">
    @include('partials.header')

    {{ $slot }}

    @include('partials.footer')

    <a href="#top" data-go-top
        class="invisible fixed text-white bottom-32 right-[15px] p-[10px] rounded-lg z-20 go-top">
        <svg class="fill-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="none" d="M0 0h24v24H0z" />
            <path d="M13 7.828V20h-2V7.828l-5.364 5.364-1.414-1.414L12 4l7.778 7.778-1.414 1.414L13 7.828z" />
        </svg>
    </a>
    <script src="https://www.google.com/recaptcha/api.js?render=6LdZ_kAlAAAAAJZGLocZYxHJY5J2y4F5j4iRxBfv"></script>
    @stack('scripts')
</body>

</html>
