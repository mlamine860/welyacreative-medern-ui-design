<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        WelyaCreative
        @isset($title)
        | {{title}}
        @endisset
    </title>
    <!--
    -google font link
     -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&family=Source+Sans+Pro:wght@600;700&display=swap"
        rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="w-[100%] h-[100%] overflow-auto text-[1.6rem] text-body">
    <section class="section">
        <div class="container">
            <div class="w-full md:w-[450px] mx-auto">
                <div class="grid place-content-center">
                    <x-logo width="60" height="60" />
                </div>
                @unless (request()->routeIs('verification.notice'))
                @if (session('status'))
                <div class="mb-4 bg-success text-slate-200 p-4 rounded">
                    {{ __(session('status')) }}
                </div>
                @endif
                @endunless
                @if (session('status') == 'verification-link-sent')
                <div class="mb-4 p-4 text-slate-200 bg-success">
                    Un nouveau lien de vérification a été envoyé à l'adresse e-mail que vous avez fournie lors de
                    l'inscription.
                </div>
                @endif
                {{ $slot }}
            </div>
        </div>
    </section>
</body>

</html>
