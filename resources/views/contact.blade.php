<x-app-layout>
    @section('title')
        | Contact
    @endsection

    <!-- Hero contact -->
    <section class="py-[120px] hero-contact bg-gradient-primary" id="hero-contact">
        <div class="container">
            <div class="grid place-content-center">
                <h1 class="hero-title">Prêt à gagner plus d'affaires?</h1>
                <p class="text-slate-200 font-500 text-fs-6">Appelez-nous au +224 622064449 ou contactez-nous dès
                    aujourd'hui</p>
            </div>
        </div>
    </section>

    <!-- Contact -->
    <section class="contact section">
        <div class="container">
            <h2 class="text-center">Contactez-nous</h2>
            <p class="text-fs-8 text-center mb-[30px]">Parlez-nous de votre projet.
            </p>
            <div class="card shadow-2xl">
                <div class="grid lg:grid-cols-2 gap-8">
                    <img src="{{ asset('images/contact-us.jpg') }}"
                        class="hidden lg:block w-full h-full rounded-lg md:object-contain lg:object-cover" alt="contact us">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </section>

    <!-- Socials and address    -->
    <section class="contact-social-and-address" id="contact-social-and-address">
        <div class="container">
            <h3 class="md:text-center mb-[30px]">
                Adresses & Réseaux sociaux
            </h3>
            <div class="flex flex-col md:flex-row  justify-between items-center gap-16">
                <div class="self-start">
                    <p class="text-fs-7 text-secondary mb-2">Sur les réseaux sociaux</p>
                    <div class="flex gap-4">
                        <a href="" class="social-link bg-primary hover:bg-white group">
                            <svg class="social-link-icon group-hover:fill-primary" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path
                                    d="M12 2C6.477 2 2 6.477 2 12c0 4.991 3.657 9.128 8.438 9.879V14.89h-2.54V12h2.54V9.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V12h2.773l-.443 2.89h-2.33v6.989C18.343 21.129 22 16.99 22 12c0-5.523-4.477-10-10-10z" />
                            </svg>
                        </a>
                        <a href="" class="social-link bg-primary hover:bg-white group">
                            <svg class="social-link-icon group-hover:fill-primary" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path
                                    d="M15.3 5.55a2.9 2.9 0 0 0-2.9 2.847l-.028 1.575a.6.6 0 0 1-.68.583l-1.561-.212c-2.054-.28-4.022-1.226-5.91-2.799-.598 3.31.57 5.603 3.383 7.372l1.747 1.098a.6.6 0 0 1 .034.993L7.793 18.17c.947.059 1.846.017 2.592-.131 4.718-.942 7.855-4.492 7.855-10.348 0-.478-1.012-2.141-2.94-2.141zm-4.9 2.81a4.9 4.9 0 0 1 8.385-3.355c.711-.005 1.316.175 2.669-.645-.335 1.64-.5 2.352-1.214 3.331 0 7.642-4.697 11.358-9.463 12.309-3.268.652-8.02-.419-9.382-1.841.694-.054 3.514-.357 5.144-1.55C5.16 15.7-.329 12.47 3.278 3.786c1.693 1.977 3.41 3.323 5.15 4.037 1.158.475 1.442.465 1.973.538z" />
                            </svg>
                        </a>
                        <a href="" class="social-link bg-primary hover:bg-white group">
                            <svg class="social-link-icon group-hover:fill-primary" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path
                                    d="M19.606 6.995c-.076-.298-.292-.523-.539-.592C18.63 6.28 16.5 6 12 6s-6.628.28-7.069.403c-.244.068-.46.293-.537.592C4.285 7.419 4 9.196 4 12s.285 4.58.394 5.006c.076.297.292.522.538.59C5.372 17.72 7.5 18 12 18s6.629-.28 7.069-.403c.244-.068.46-.293.537-.592C19.715 16.581 20 14.8 20 12s-.285-4.58-.394-5.005zm1.937-.497C22 8.28 22 12 22 12s0 3.72-.457 5.502c-.254.985-.997 1.76-1.938 2.022C17.896 20 12 20 12 20s-5.893 0-7.605-.476c-.945-.266-1.687-1.04-1.938-2.022C2 15.72 2 12 2 12s0-3.72.457-5.502c.254-.985.997-1.76 1.938-2.022C6.107 4 12 4 12 4s5.896 0 7.605.476c.945.266 1.687 1.04 1.938 2.022zM10 15.5v-7l6 3.5-6 3.5z" />
                            </svg>
                        </a>
                        <a href="" class="social-link bg-primary hover:bg-white group">
                            <svg class="social-link-icon group-hover:fill-primary" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path
                                    d="M4 3h16a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm1 2v14h14V5H5zm2.5 4a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm-1 1h2v7.5h-2V10zm5.5.43c.584-.565 1.266-.93 2-.93 2.071 0 3.5 1.679 3.5 3.75v4.25h-2v-4.25a1.75 1.75 0 0 0-3.5 0v4.25h-2V10h2v.43z" />
                            </svg>
                        </a>
                    </div>
                </div>
                <!-- Addresses -->
                <div class="flex flex-col self-start">
                    <p class="text-fs-7 text-secondary mb-2">Contactez nous</p>
                    <div class="flex gap-4 flex-col text-fs-8 mb-[10px]">
                        <div class="flex items-center gap-4">
                            <span class="social-link bg-primary hover:bg-white group">
                                <svg class="social-link-icon group-hover:fill-primary"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                                    height="24">
                                    <path fill="none" d="M0 0h24v24H0z" />
                                    <path
                                        d="M9.366 10.682a10.556 10.556 0 0 0 3.952 3.952l.884-1.238a1 1 0 0 1 1.294-.296 11.422 11.422 0 0 0 4.583 1.364 1 1 0 0 1 .921.997v4.462a1 1 0 0 1-.898.995c-.53.055-1.064.082-1.602.082C9.94 21 3 14.06 3 5.5c0-.538.027-1.072.082-1.602A1 1 0 0 1 4.077 3h4.462a1 1 0 0 1 .997.921A11.422 11.422 0 0 0 10.9 8.504a1 1 0 0 1-.296 1.294l-1.238.884zm-2.522-.657l1.9-1.357A13.41 13.41 0 0 1 7.647 5H5.01c-.006.166-.009.333-.009.5C5 12.956 11.044 19 18.5 19c.167 0 .334-.003.5-.01v-2.637a13.41 13.41 0 0 1-3.668-1.097l-1.357 1.9a12.442 12.442 0 0 1-1.588-.75l-.058-.033a12.556 12.556 0 0 1-4.702-4.702l-.033-.058a12.442 12.442 0 0 1-.75-1.588z" />
                                </svg>
                            </span>
                            <div class="flex flex-col gap-1">
                                <a href="">622 06 44 49</a>
                                <a href="">622 06 44 49</a>
                            </div>
                        </div>
                    </div>
                    <!-- Emails -->
                    <div class="flex gap-4 flex-col text-fs-8 mb-[10px]">
                        <div class="flex items-center gap-4">
                            <span class="social-link bg-primary hover:bg-white group">
                                <svg class="social-link-icon group-hover:fill-primary"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                                    height="24">
                                    <path fill="none" d="M0 0h24v24H0z" />
                                    <path
                                        d="M3 3h18a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm17 4.238l-7.928 7.1L4 7.216V19h16V7.238zM4.511 5l7.55 6.662L19.502 5H4.511z" />
                                </svg>
                            </span>
                            <div class="flex flex-col gap-1">
                                <a href="">contact@welyacreative.com</a>
                                <a href="">admin@welyacreative.com</a>
                            </div>
                        </div>
                    </div>

                    <!-- Addresses -->
                    <div class="flex gap-4 flex-col text-fs-8">
                        <div class="flex items-center gap-4">
                            <span class="social-link bg-primary hover:bg-white group ">
                                <svg class="social-link-icon group-hover:fill-primary"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                                    height="24">
                                    <path fill="none" d="M0 0h24v24H0z" />
                                    <path
                                        d="M18.364 17.364L12 23.728l-6.364-6.364a9 9 0 1 1 12.728 0zM12 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm0-2a2 2 0 1 1 0-4 2 2 0 0 1 0 4z" />
                                </svg>
                            </span>
                            <div class="flex flex-col gap-1">
                                <a href="">Sonfonia Foula Madina</a>
                                <a href="">Boké, Fria</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    @include('partials.newsletter')
</x-app-layout>
