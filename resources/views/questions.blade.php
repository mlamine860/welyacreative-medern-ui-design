<x-app-layout>
    @section('title')
        | FAQ GÉNÉRALE
    @endsection
    <section class="py-[120px] bg-gradient-primary" id="hero-faq">
        <div class="container">
            <div class="grid place-content-center">
                <p class="text-white">FAQ GÉNÉRALE.</p>
                <h1 class="hero-title">Des réponses à vos questions</h1>
            </div>
        </div>
    </section>
    <section class="faq py-16 md:py-32" id="faq">
        <div class="container">
            <div>
                @foreach ($questions as $q)
                    <div x-data="{ open: false }" class="mb-6 border rounded-t-lg">
                        <div x-on:click="open = !open"
                            class="p-2 cursor-pointer bg-primary/10 rounded-t-lg flex justify-between hover:bg-primary/40"
                            :class="open ? 'bg-primary/40' : ''">
                            <h4 class="text-fs-6  text-secondary">{{ $q->question }}</h4>
                            <div>
                                <svg x-show="open" class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path
                                        d="M11.9997 10.8284L7.04996 15.7782L5.63574 14.364L11.9997 8L18.3637 14.364L16.9495 15.7782L11.9997 10.8284Z">
                                    </path>
                                </svg>
                                <svg x-show="!open" class="w-8 h-8" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 24 24">
                                    <path
                                        d="M11.9997 13.1714L16.9495 8.22168L18.3637 9.63589L11.9997 15.9999L5.63574 9.63589L7.04996 8.22168L11.9997 13.1714Z">
                                    </path>
                                </svg>
                            </div>
                        </div>
                        <div class="px-2 py-4" x-show="open" x-transition>
                            <p>{{ $q->answer }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="bg-gradient-primary py-16">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </section>
    @include('partials.newsletter')
</x-app-layout>
