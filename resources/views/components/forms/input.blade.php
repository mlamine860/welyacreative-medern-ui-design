@props(['type' => 'text', 'label', 'name', 'placeholder' => '', 'error' => ''])
<div class="mb-[10px]">
    <label for="name" class="text-fs-8">{{ $label }} <span class="text-red-400">*</span></label>
    <div
        class="flex items-center bg-gray-200 px-[10px] py-[5px] rounded-xl {{ $errors->has($name) ? 'border border-darkRed' : '' }}">
        {{ $slot }}
        <input x-model="{{ $name }}" type="{{ $type }}" name="{{ $name }}" id="{{ $name }}" value="{{old($name, '')}}"
            placeholder="{{ $placeholder }}"
            class="w-full bg-transparent text-fs-8 border-0 placeholder:text-fs-8 focus:outline-none focus:ring-0">
    </div>
    <p x-show="{{ $error }}" x-text="{{$error}}" class="text-darkRed"></p>
</div>
