@props(['label', 'name', 'placeholder' => '', 'error' => ''])
<div class="mb-[10px]">
    <label for="name" class="text-fs-8">{{ $label }} <span class="text-red-400">*</span></label>
    <div class="flex bg-gray-200 px-[10px] py-[5px] rounded-xl">
        <textarea x-model="{{$name}}" name="message" id="message" cols="30" rows="10"
            class="w-full bg-transparent text-fs-8 border-0 placeholder:text-fs-8 focus:outline-none focus:ring-0"></textarea>
    </div>
    <p x-show="{{ $error }}" x-text="{{$error}}" class="text-darkRed"></p>
</div>
