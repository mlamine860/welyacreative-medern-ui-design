<x-app-layout>
    @section('title')
        | Apropos de nous
    @endsection
    <!-- Hero about us  -->
    <section class="bg-hero-pattern py-[120px] mt-[60px] hero-contact" id="hero-contact">
        <div class="container">
            <div class="flex items-center justify-center">
                <h1 class="text-fs-3 md:text-fs-1 hero-title">Ce que nous faisons</h1>
            </div>
        </div>
    </section>

    <!-- About Us -->
    <section class="contact section">
        <div class="container">
            <div class="grid  md:grid-cols-2 gap-[30px] items-center">
                <h2 class="text-fs-3 md:text-[50px] font-700 leading-[1.5]">NOTRE ÉQUIPE EST VOTRE ÉQUIPE</h2>
                <p class="text-fs-5 leading-[1.6] title-underline">
                    Nous sommes un groupe d'experts d'esprits divers qui associent stratégie, technologie et passion
                    créative pour concevoir
                    la meilleure expérience pour votre marque.
                </p>
            </div>
        </div>
    </section>

    <!-- About Us Image full -->
    <section class="my-[60px] section about-us-full  bg-no-repeat bg-cover bg-center w-full h-[534px]"
        style="background-image: url('{{ asset('images/about-us-full.jpeg') }}')">
    </section>

    <section class="contact section">
        <div class="container">
            <div class="grid  md:grid-cols-3 gap-[10px] items-start">
                <h2 class="text-fs-1 leading-[1.2]">À PROPOS DE NOUS</h2>
                <div class="col-span-2">
                    <p class="text-fs-7 leading-[1.6] mb-[10px]">
                        WelyaCreative fait partie des acteurs de la transition numérique en Guinée. Notre agence Web &
                        Digital propose une
                        approche 360°, orientée autour des besoins de notre clientèle, de l’analyse des données et de
                        l’expérience utilisateur.
                        Nous appliquons des méthodologies éprouvées à l’ensemble de nos services, soit la conception, le
                        design UX/UI et le
                        développement de MVPs, sites Internet, e-commerce, marketplaces et applications mobiles ;
                        l’hébergement et la
                        maintenance de services digitaux.
                    </p>
                    <p class="text-fs-7 leading-[1.6]">
                        Depuis + de 5 ans, notre agence digitale perfectionne sa méthode pour concevoir des expériences
                        qui
                        suscitent
                        l’engagement de vos utilisateurs et génèrent des résultats pour votre entreprise. Notre mission
                        ?
                        Façonner l'UX pour
                        humaniser vos produits et services.
                    </p>
                </div>
            </div>
            <div class="py-[50px]">
                <h3 class="text-center">Ce qui nous differencie des autres?</h3>
                <p class="text-fs-7 mb-[5px]">Depuis 2019, nous croisons nos compétences en conseil stratégique,
                    développement
                    technique, design
                    d’interface et
                    conception centrée utilisateur pour produire des supports web ultra-efficaces. Notre recette ? Faire
                    collaborer des
                    humains bourrés de talents, prêts à relever les défis les plus fous (ou presque) au service de l'UX
                    et du RSI.</p>
                <p class="text-fs-7">Nous concevons des expériences digitales uniques qui engagent vos utilisateurs,
                    vous démarquent de la
                    concurrence et
                    prennent soin de votre RSI(Retour Sur Investissement). Création ou refonte de site internet,
                    application mobile, outils
                    métiers, web app, Création des contenues medias, Marketing digital et le Comminuty management : nos
                    experts en UX
                    Design, stratégie digitale, design d'interfaces et développement technique conçoivent votre projet
                    en (re)plaçant
                    l’humain au centre de la conception.</p>
            </div>
        </div>
    </section>

    <!-- Let's talk -->
    <section class="lets-talk">
        <div class="container grid place-content-center">
            <h1>Let’s talk</h1>
            <p class=""><a href="mailto:contact@welyacreative.com">hey@welyacreative.com</a></p>
        </div>
    </section>

    @include('partials.newsletter')


</x-app-layout>
