<x-app-layout>
    @push('head')
        <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
    @endpush
    @push('scripts')
        <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
        <script>
            let swiper = new Swiper(".swiper", {
                lazy: true,
                pagination: {
                    el: ".swiper-pagination",
                    hide: true,
                    clickable: true
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
                keyboard: {
                    enabled: true,
                },
                loop: true,
                autoplay: true,
            });
        </script>
    @endpush
    <!-- Hero -->
    <section class="hero sm:min-h-[600px] md:min-h-[700px] lg:min-h-[800px] md:grid place-items-center" id="hero">
        <div class="container md:grid grid-cols-2 items-center gap-[30px]">
            <div class="mb-[50px]">
                <p class="text-primary font-ssp text-600 text-[20px] mb-[15px]">Nous sommes une agence web en Guinée</p>
                <h1 class="hero-title">Solutions
                    Web
                    Personnalisées
                </h1>
                <p class="text-white text-[16px] mb-[30px]">Libérez le potentiel de votre entreprise en
                    ligne avec notre
                    agence
                    web innovante et créative</p>
                <a href="{{ route('pages.contact') }}" class="btn-primary inline-block">En savoire plus</a>
            </div>
            <figure class="hero-banner">
                <img src="{{ asset('images/hero-service.png') }}" width="694" height="529" loading="lazy"
                    alt="hero-banner" class="w-full h-auto rounded-full motion-safe:animate-pulse ">
            </figure>
        </div>
    </section>
    <!-- About -->
    <section class="section" id="about">
        <div class="container lg:grid lg:grid-cols-2 gap-[40px]">
            <figure class="mb-[30px]">
                <img src="{{ asset('images/about-us.jpeg') }}" width="700" height="532" loading="lazy"
                    alt="about banner" class="w-full h-auto rounded-[40px]">
            </figure>
            <div class="">
                <h2 class="mb-[40px] leading-[1.2] title-underline">Qu'est-ce qu'on fait ?
                </h2>
                <p class="text-fs-8 mb-[10px]">Notre agence web est fière de proposer des solutions de marketing
                    digital, de web
                    design, de
                    graphique design, de
                    développement mobile, et de développement web de qualité supérieure à des prix abordables.</p>
                <p class="text-fs-8 mb-[10px]">Nous comprenons que chaque entreprise a des besoins uniques en ligne,
                    c'est pourquoi nous travaillons
                    en étroite
                    collaboration avec nos clients pour créer des sites web personnalisés qui répondent à leurs besoins
                    spécifiques, tout en
                    offrant des stratégies de marketing digital éprouvées pour améliorer leur visibilité en ligne.</p>
                <p class="text-fs-8 mb-[10px]">Que vous cherchiez à lancer un site web de commerce électronique, à
                    améliorer votre taux de
                    conversion ou à renforcer
                    votre présence sur les réseaux sociaux, notre équipe est là pour vous aider à atteindre vos
                    objectifs.</p>
                <div class="flex flex-col gap-8 md:flex-row items-center  py-[25px]">
                    <a href="" class="btn-primary">En savoire plus</a>
                    <div class="flex gap-4 text-primary">
                        <svg class="fill-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                            height="24">
                            <path fill="none" d="M0 0h24v24H0z" />
                            <path
                                d="M17 15.245v6.872a.5.5 0 0 1-.757.429L12 20l-4.243 2.546a.5.5 0 0 1-.757-.43v-6.87a8 8 0 1 1 10 0zM12 15a6 6 0 1 0 0-12 6 6 0 0 0 0 12zm0-2a4 4 0 1 1 0-8 4 4 0 0 1 0 8z" />
                        </svg>
                        <span class="text-fs-6">5 ans d'éxperiences</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Companies -->
    <section class="companies bg-secondary section" id="companies">
        <div class="container">
            <h2 class="font-ssp text-fs-3  text-center text-white leading-none mb-[40px]">Marques avec lesquelles nous
                avons travaillé</h2>
            <div class="grid grid-cols-3 md:grid-cols-7 gap-8">
                <img src="{{ asset('images/companies/africa-water.png') }}" class="rounded-md" alt="africa-water">
                <img src="{{ asset('images/companies/sgs.png') }}" class="rounded-md" alt="sgs">
                <img src="{{ asset('images/companies/darnei-kah-mein.png') }}" class="rounded-md" alt="darnei-kah-mein">
                <img src="{{ asset('images/companies/cis.png') }}" class="rounded-md" alt="cis">
                <img src="{{ asset('images/companies/humnan-bird.png') }}" class="rounded-md" alt="humnan-bird">
                <img src="{{ asset('images/companies/fa-global-logistic.png') }}" class="rounded-md"
                    alt="fa-global-logistic">
                <img src="{{ asset('images/companies/windfall.png') }}" class="rounded-md" alt="windfall">
            </div>
        </div>
    </section>
    <!-- Services -->
    <section class="section services" id="services">
        <div class="container">
            <div class="flex item-center justify-center mb-[20px]">
                <h2 class="text-ssp text-fs-3 font-700 text-center text-secondary">Notre spécialisation
                </h2>
            </div>
            <div class="text-center md:w-2/3 m-auto mb-[40px]">
                <p class="text-fs-7">Tirez le meilleur parti de la réduction des coûts
                    d'exploitation
                    de votre
                    équipe pour l'ensemble du produit, ce qui crée
                    des expériences UI/UX incroyables.</p>
            </div>
            <div class="grid md:grid-cols-2 lg:grid-cols-4 py-[40px] gap-4">
                @include('partials.services', ['services' => $featuredServices])
            </div>
        </div>
    </section>
    <!-- Teck stack -->
    <section class="services section">
        <div class="container">
            <div class="flex item-center justify-center mb-[40px]">
                <h2 class="text-ssp text-secondary text-fs-3 font-700 text-center">Notre Tech Stack</h2>
            </div>
            <p class="text-fs-8 mb-[40px] md:w-1/2 m-auto">Nous utilisons des technologies de pointe pour donner vie a
                vos projets .</p>
            <div class="grid grid-cols-3 gap-4 items-center md:grid-cols-6 mt-[20px]">
                <img src="{{ asset('images/icons/nodejs.svg') }}" alt="Nodejs" title="NodeJs">
                <img src="{{ asset('images/icons/php.svg') }}" alt="Php" title="Php">
                <img src="{{ asset('images/icons/flutter.svg') }}" alt="Flutter" title="Flutter">
                <img src="{{ asset('images/icons/java.svg') }}" alt="Java" title="Java">
                <img src="{{ asset('images/icons/python.svg') }}" alt="Python">
                <img src="{{ asset('images/icons/laravel.svg') }}" alt="Laravel" title="Python">
                <img src="{{ asset('images/icons/django.svg') }}" alt="Django" title="Django">
                <img src="{{ asset('images/icons/reactjs.svg') }}" alt="React" title="React">
                <img src="{{ asset('images/icons/typescript.svg') }}" alt="TypeScript" title="TypeScript">
                <img src="{{ asset('images/icons/mongodb.svg') }}" alt="Mongodb" title="Mongodb">
                <img src="{{ asset('images/icons/mysql.svg') }}" alt="Mysql" title="Mysql">
                <img src="{{ asset('images/icons/firebase.svg') }}" alt="Firebase" title="Firebase">
            </div>
        </div>
    </section>
    <!-- Testimonials -->
    <section class="testimonial section">
        <div class="container">
            <h2 class="mb-[35px] text-center text-fs-3 font-700">Ce que disent nos clients !
            </h2>
            <div class="grid md:grid-cols-2 lg:grid-cols-3 gap-8 relative">
                @foreach ($testimonials as $testimony)
                    <div class="shadow-lg rounded border hover:shadow-xl transition-all">
                        <div class="p-8 ">
                            <div class="flex gap-8 items-center mb-8">
                                <img src="{{ $testimony->avatar }}" class="w-16 h-16 rounded-full"
                                    alt="{{ $testimony->username }}">
                                <div class="flex flex-col items-start">
                                    <div class="text-fs-6 text-primary/70">{{ $testimony->username }}</div>
                                    <div class="text-gray-500 text-fs-9">{{ $testimony->job }}</div>
                                </div>
                            </div>
                            <p class="text-gray-5 text-fs-9 leading-relaxed text-center"><span
                                    class="text-fs-6">&ldquo;
                                </span>{{ $testimony->message }}<span class="text-fs-6">&rdquo;
                                </span></p>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Projects -->
    @if ($projects->isNotEmpty())
        <section class="projects py-32" id="portfolio">
            <div class="container">
                <h2 class="text-center text-fs-3 font-700 mb-16">Nos Réalisations</h2>
                <div class="swiper h-[620px]">
                    <div class="swiper-wrapper">
                        @foreach ($projects as $project)
                            <a href="{{ route('projects.detail', $project) }}"
                                class="block overflow-hidden swiper-slide slide-{{ $project->id }}">
                                <img src="{{ $project->imageLarge }}"
                                    class="swiper-lazy w-full h-[450px] object-cover rounded  hover:scale-110 ease-in duration-300 "
                                    alt="{{ $project->name }}">
                                <div class="p-8">
                                    <h3 class="text-fs-5">{{ $project->name }}</h3>
                                    <div class="py-6 flex gap-2">
                                        @foreach ($project->tags as $tag)
                                            <span
                                                class="px-2 py-1 flex items-center text-white bg-primary rounded-full text-fs-11">{{ $tag->name }}</span>
                                        @endforeach
                                    </div>
                                    <p class="text-fs-8">{{ $project->description }}</p>
                                </div>
                            </a>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                {{-- </div> --}}
            </div>
        </section>
    @endif
    <!-- Blog news -->
    <section class="blog section" id="blog">
        <div class="container">
            <div class="flex item-center justify-center mb-[60px]">
                <h2 class="text-ssp leading-tight text-fs-3 font-700 text-secondary text-center">Notre
                    blog et nos
                    actualités</h2>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
                @foreach ($posts as $post)
                    <a href="{{ route('posts.detail', $post) }}" class="shadow-lg rounded-lg border">
                        <div class="overflow-hidden">
                            <img src="{{ $post->imageMedium }}"
                                class="h-[250px] w-full object-cover  hover:scale-110 ease-in duration-300 rounded-t"
                                alt="{{ $post->name }}">
                        </div>
                        <div class="p-6 flex flex-col justify-between">
                            <h3 class="hover:text-primary transition ease-in  leading-tight">
                                {{ $post->title }}</h3>
                            <p class="text-fs-8 mb-[15px]">{{ $post->excerpt }}</p>
                            <div class="grid grid-cols-2 gap-8">
                                <div class="flex gap-4 justify-start items-center">
                                    <svg class="fill-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        width="24" height="24">
                                        <path fill="none" d="M0 0h24v24H0z" />
                                        <path
                                            d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm1-8h4v2h-6V7h2v5z" />
                                    </svg>
                                    <span class="text-gray-500 uppercase text-xl">
                                        {{ $post->created_at->isoFormat('LL') }}</span>
                                </div>
                                <div class="flex gap-4 justify-end items-center cursor-pointer">
                                    <span class="text-xl text-gray-500">15</span>
                                    <svg class="fill-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                        width="24" height="24">
                                        <path fill="none" d="M0 0h24v24H0z" />
                                        <path
                                            d="M10 3h4a8 8 0 1 1 0 16v3.5c-5-2-12-5-12-11.5a8 8 0 0 1 8-8zm2 14h2a6 6 0 1 0 0-12h-4a6 6 0 0 0-6 6c0 3.61 2.462 5.966 8 8.48V17z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        </div>
    </section>
    <!-- FQA -->
    <section class="faq py-[120px] bg-secondary/5">
        <div class="container">
            <div class="text-center">
                <h2 class="text-ssp leading-tight text-fs-3 font-700 text-secondary text-center">Questions fréquemment
                    posées</h3>
            </div>
            <div class="mt-16">
                @foreach ($questions as $q)
                    <div x-data="{ open: false }" class="mb-6 border border-gray-300 rounded-t-lg">
                        <div x-on:click="open = !open"
                            class="p-2 cursor-pointer bg-primary/10 rounded-t-lg flex justify-between hover:bg-primary/40"
                            :class="open ? 'bg-primary/40' : ''">
                            <h4 class="text-fs-6  text-secondary">{{ $q->question }}</h4>
                            <div>
                                <svg x-show="open" class="w-8 h-8" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 24 24">
                                    <path
                                        d="M11.9997 10.8284L7.04996 15.7782L5.63574 14.364L11.9997 8L18.3637 14.364L16.9495 15.7782L11.9997 10.8284Z">
                                    </path>
                                </svg>
                                <svg x-show="!open" class="w-8 h-8" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 24 24">
                                    <path
                                        d="M11.9997 13.1714L16.9495 8.22168L18.3637 9.63589L11.9997 15.9999L5.63574 9.63589L7.04996 8.22168L11.9997 13.1714Z">
                                    </path>
                                </svg>
                            </div>
                        </div>
                        <div class="px-2 py-4" x-show="open" x-transition>
                            <p>{{ $q->answer }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Contact -->
    <section class="contact section" id="contact">
        <div class="container">
            <h2 class="text-ssp text-fs-3 font-700 text-secondary text-center mb-[15px]">Contactez-nous</h2>
            <p class="text-fs-8 text-center mb-[30px]">Parlez-nous de votre projet.
            </p>
            <div class="card shadow-2xl">
                <div class="grid md:grid-cols-2 gap-[32px]">
                    <img src="{{ asset('images/contact-us.jpg') }}"
                        class="hidden md:block w-full h-full rounded-lg object-cover" alt="contact us">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </section>
    <!-- Team -->
    <section class="team py-[120px]">
        <div class="container">
            <h3 class="text-fs-2 text-center">Notre team</h3>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-8 pt-16">
                @foreach ($teams as $member)
                    <div class="px-8 shadow-lg rounded-lg flex flex-col lg:flex-row gap-4 items-center">
                        <img src="{{ $member->avatar }}"
                            class="w-40 h-40 rounded-full" width="240"
                            height="120" alt="">
                        <div class="p-8 text-center lg:text-left">
                            <h3 class="text-fs-7 font-medium">{{ $member->name }}</h3>
                            <div class="text-fs-9 text-gray-500">{{ $member->profile->role }}</div>
                            <p class="text-gray-600 text-fs-9 py-4">{{ $member->profile->bio }}
                            </p>
                            <div class="flex gap-4 justify-end">
                                <a href="">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 fill-gray-600"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M12.001 2C6.47813 2 2.00098 6.47715 2.00098 12C2.00098 16.9913 5.65783 21.1283 10.4385 21.8785V14.8906H7.89941V12H10.4385V9.79688C10.4385 7.29063 11.9314 5.90625 14.2156 5.90625C15.3097 5.90625 16.4541 6.10156 16.4541 6.10156V8.5625H15.1931C13.9509 8.5625 13.5635 9.33334 13.5635 10.1242V12H16.3369L15.8936 14.8906H13.5635V21.8785C18.3441 21.1283 22.001 16.9913 22.001 12C22.001 6.47715 17.5238 2 12.001 2Z">
                                        </path>
                                    </svg>
                                </a>
                                <a href="">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 fill-gray-600"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M22.2125 5.65605C21.4491 5.99375 20.6395 6.21555 19.8106 6.31411C20.6839 5.79132 21.3374 4.9689 21.6493 4.00005C20.8287 4.48761 19.9305 4.83077 18.9938 5.01461C18.2031 4.17106 17.098 3.69303 15.9418 3.69434C13.6326 3.69434 11.7597 5.56661 11.7597 7.87683C11.7597 8.20458 11.7973 8.52242 11.8676 8.82909C8.39047 8.65404 5.31007 6.99005 3.24678 4.45941C2.87529 5.09767 2.68005 5.82318 2.68104 6.56167C2.68104 8.01259 3.4196 9.29324 4.54149 10.043C3.87737 10.022 3.22788 9.84264 2.64718 9.51973C2.64654 9.5373 2.64654 9.55487 2.64654 9.57148C2.64654 11.5984 4.08819 13.2892 6.00199 13.6731C5.6428 13.7703 5.27232 13.8194 4.90022 13.8191C4.62997 13.8191 4.36771 13.7942 4.11279 13.7453C4.64531 15.4065 6.18886 16.6159 8.0196 16.6491C6.53813 17.8118 4.70869 18.4426 2.82543 18.4399C2.49212 18.4402 2.15909 18.4205 1.82812 18.3811C3.74004 19.6102 5.96552 20.2625 8.23842 20.2601C15.9316 20.2601 20.138 13.8875 20.138 8.36111C20.138 8.1803 20.1336 7.99886 20.1256 7.81997C20.9443 7.22845 21.651 6.49567 22.2125 5.65605Z">
                                        </path>
                                    </svg>
                                </a>
                                <a href="">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 fill-gray-600"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M18.3362 18.339H15.6707V14.1622C15.6707 13.1662 15.6505 11.8845 14.2817 11.8845C12.892 11.8845 12.6797 12.9683 12.6797 14.0887V18.339H10.0142V9.75H12.5747V10.9207H12.6092C12.967 10.2457 13.837 9.53325 15.1367 9.53325C17.8375 9.53325 18.337 11.3108 18.337 13.6245V18.339H18.3362ZM7.00373 8.57475C6.14573 8.57475 5.45648 7.88025 5.45648 7.026C5.45648 6.1725 6.14648 5.47875 7.00373 5.47875C7.85873 5.47875 8.55173 6.1725 8.55173 7.026C8.55173 7.88025 7.85798 8.57475 7.00373 8.57475ZM8.34023 18.339H5.66723V9.75H8.34023V18.339ZM19.6697 3H4.32923C3.59498 3 3.00098 3.5805 3.00098 4.29675V19.7033C3.00098 20.4202 3.59498 21 4.32923 21H19.6675C20.401 21 21.001 20.4202 21.001 19.7033V4.29675C21.001 3.5805 20.401 3 19.6675 3H19.6697Z">
                                        </path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('partials.newsletter')


</x-app-layout>
