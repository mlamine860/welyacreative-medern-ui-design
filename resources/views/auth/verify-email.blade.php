<x-guest-layout>
    <div class="card">
        <div class="mb-4 text-body">
            {{ __("Merci pour l'enregistrement! Avant de commencer, pourriez-vous vérifier votre adresse e-mail en
            cliquant sur le
            lien que nous
            venons de vous envoyé par e-mail ? Si vous n'avez pas reçu l'e-mail, nous vous en enverrons un autre
            avec plaisir.") }}
        </div>
        <div class="mt-4 flex items-center justify-between">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div class="py-4">
                    <x-primary-button>
                        {{ __('Renvoyer l\'e-mail de vérification') }}
                    </x-primary-button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="underline  text-primary">
                    {{ __('Deconnexion') }}
                </button>
            </form>
        </div>

    </div>
</x-guest-layout>
