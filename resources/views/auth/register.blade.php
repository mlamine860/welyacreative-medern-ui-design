<x-guest-layout>
    <div class="card">
        <h3 class="text-center font-700">Inscription</h3>
        <form action="{{ route('register') }}" method="POST">
            @csrf
            <x-forms.input-text type="text" name="name" label="Votre nom" placeholder="Joe Doe"
                error="{{ $errors->first('name') }}">
                <svg class="fill-body" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                    height="24">
                    <path fill="none" d="M0 0h24v24H0z" />
                    <path d="M4 22a8 8 0 1 1 16 0H4zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6 6 2.685 6 6-2.685 6-6 6z" />
                </svg>
            </x-forms.input-text>
            <x-forms.input-text type="email" name="email" label="Votre email" error="{{ $errors->first('email') }}"
                placeholder="Exemple@domain.com">
                <svg aria-hidden="true" class="fill-body mr-3" height="24px" fill="currentColor" viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z">
                    </path>
                    <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                </svg>
            </x-forms.input-text>
            <x-forms.input-text type="password" name="password" label="Mot de passe"
                error="{{ $errors->first('password') }}" placeholder="">
                </x-forms.input>
                <x-forms.input type="password" name="password_confirmation"
                    error="{{ $errors->first('password_confirm') }}" label="Confirmation de mot de passe" placeholder="">
            </x-forms.input-text>
            <div class="flex justify-end mt-[15px]">
                <button class="btn-primary">S'inscrire</button>
            </div>
            <div class="mt-[15px]">
                Vous avez déjà un compte? <a href="{{ route('login') }}" class="text-primary">Se connecter</a>
            </div>
        </form>

</x-guest-layout>
