<x-guest-layout>
    <div class="card">
        <h3>Réinitialisation du mot de passe</h3>
        <form action="{{route('password.update')}}" method="POST">
            @csrf
            <input type="hidden" name="token" value="{{request()->route('token')}}">
            <x-forms.input type="email" name="email" label="Votre email" value="{{request()->get('email')}}"
                placeholder="Exemple@domain.com">
                <svg aria-hidden="true" class="fill-body mr-3" height="24px" fill="currentColor" viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z">
                    </path>
                    <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                </svg>
            </x-forms.input>
            <x-forms.input type="password" name="password" label="Mot de passe" placeholder="">
            </x-forms.input>
            <x-forms.input type="password" name="password_confirmation" label="Confirmation de mot de passe"
                placeholder="">
            </x-forms.input>
            <div class="flex justify-end mt-[15px]">
                <button class="btn-primary">Reinitialiser le mot de passe</button>
            </div>
        </form>
    </div>
</x-guest-layout>
