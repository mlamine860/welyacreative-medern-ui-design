<x-guest-layout>
    <div class="card">
        <h3>Demande de réinitialisation du mot de passe</h3>
        <form action="{{route('password.email')}}" method="POST">
            @csrf
            <x-forms.input type="email" name="email" label="Votre email" placeholder="Exemple@domain.com">
                <svg aria-hidden="true" class="fill-body mr-3" height="24px" fill="currentColor" viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z">
                    </path>
                    <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                </svg>
            </x-forms.input>
            <div class="flex justify-end mt-[15px]">
                <button class="btn-primary">Envoyer</button>
            </div>
        </form>
    </div>
</x-guest-layout>
