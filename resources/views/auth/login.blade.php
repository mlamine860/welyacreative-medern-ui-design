<x-guest-layout>
    <div class="card">
        <div class="p-4">
            <h3 class="text-center font-700">Connéxion</h3>
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <x-forms.input-text type="email" name="email" label="Votre email" error="{{$errors->first('email')}}" placeholder="exemple@domain.com">
                    <svg aria-hidden="true" class="fill-body mr-3" height="24px" fill="currentColor"
                        viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z">
                        </path>
                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                    </svg>
                </x-forms.input-text>
                <x-forms.input-text type="password" name="password" label="Mot de passe" error="{{$errors->first('password')}}" placeholder="">
                </x-forms.input-text>
                <div class="py-[15px] flex gap-4 justify-between">
                    <label for="remember" class="text-fs-8">
                        <input type="checkbox" name="remember" id="remember"
                            class="w-6 h-6 text-primary rounded-lg focus:ring-darkRed border-body">
                        <span class="ml-1 text-fs-10">Se souvenire de moi</span>
                    </label>
                    <a href="{{ route('password.request') }}" class="text-fs-10">Mot de passe oublié ?</a>
                </div>
                <div class="flex justify-end mt-[15px]">
                    <button class="btn-primary py-[8px] px-[16px] text-fs-9">Connéxion</button>
                </div>
                <div class="mt-[15px] text-fs-9">
                    Vous n'avez pas encore de compte? <a href="{{ route('register') }}"
                        class="text-primary">S'inscrire</a>
                </div>
            </form>
        </div>
    </div>
</x-guest-layout>
