@section('meta_og')
    <meta property="og:url" content="{{ route('posts.detail', $post) }}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $post->title }}" />
    <meta property="og:description" content="{{ $post->excerpt }}" />
    <meta property="og:image" content="{{ $post->image }}" />
@endsection
@section('title')
    | {{ $post->title }}
@endsection
<x-app-layout>
    <section class="pt-[120px]">
        <div class="container">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                <div class="flex flex-col gap-8">
                    <div>{{ $post->created_at->isoFormat('LL') }}</div>
                    <h1 class="text-fs-4 md:text-fs-2">{{ $post->title }}</h1>
                </div>
                <img src="{{ $post->imageMedium }}" class="w-full" width="420" height="320" alt="">
            </div>
        </div>
    </section>
    <section class="block-list pb-32">
        <div class="container">
            <div class="py-8 md:py-8">
                <div class="text-secondary text-fs-7 font-bold mb-2">Partagez</div>
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_facebook" rel="nofollow noopener" target="_blank"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_linkedin"></a>
                </div>
            </div>
            <div class="raw-content max-w-7xl m-auto">
                {!! $post->content !!}
            </div>
        </div>
    </section>

    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>
    @include('partials.newsletter')

    <script>
        var a2a_config = a2a_config || {};
        a2a_config.locale = "fr";
    </script>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
</x-app-layout>
