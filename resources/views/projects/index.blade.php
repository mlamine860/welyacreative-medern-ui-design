<x-app-layout>
    @section('title')
        | Portefolio
    @endsection

    <header class="pt-32">
        <div class="container">
            <h1 class="text-fs-3  md:text-fs-1 text-center">Notre portefolio</h1>
        </div>
    </header>
    <!-- Portfolio -->
    <section class="portfolio section">
        <div class="container">
            <div class="flex gap-2 flex-wrap pb-8 md:justify-end">
                @foreach ($tags as $tag)
                    <a
                      href="{{route('portfolio.tag', $tag->name)}}"   class="px-2 py-1 flex items-center text-white bg-primary rounded-full text-fs-11">{{ $tag->name }}</a>
                @endforeach
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
                @foreach ($projects as $project)
                    <a href="{{ route('projects.detail', $project) }}">
                        <div class="shadow-lg rounded-lg">
                            <div class="overflow-hidden">
                                <img src="{{ $project->imageMedium }}"
                                    class="w-full h-[250px] object-cover  hover:scale-110 ease-in duration-300 "
                                    alt="{{ $project->name }}">
                            </div>
                            <div class="p-6 flex flex-col justify-between">
                                <h3 class="hover:text-primary transition ease-in  leading-tight">
                                    {{ $project->name }}</h3>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

        </div>
    </section>
    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>

   @include('partials.newsletter')
</x-app-layout>
