<x-app-layout>
    @section('title')
        | {{ $project->name }}
    @endsection
    @push('head')
        <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
    @endpush
    @push('scripts')
        <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
        <script>
            let swiper = new Swiper(".swiper", {
                lazy: true,
                effect: "cube",
                cubeEffect: {
                    slideShadows: true,
                },
                pagination: {
                    el: ".swiper-pagination",
                    hide: true,
                    clickable: true
                },
                keyboard: {
                    enabled: true,
                },
                loop: true,
                autoplay: true,
            });
        </script>
    @endpush
    <header
        class="py-32 h-[90vh] bg-no-repeat bg-cover relative
    after:absolute after:content-[''] after:inset-0 after:bg-black after:opacity-50 z-0 grid place-content-center "
        style="background-image: url('{{ $project->image }}')">
        <div class="container relative z-10 grid  place-items-center">
            <div class="md:max-w-4xl">
                <h1 class="text-white text-fs-3 md:text-fs-1">{{ $project->name }}</h1>
                <div class="flex gap-2 py-6">
                    @foreach ($project->tags as $tag)
                        <span
                            class="px-2 py-1 flex items-center bg-white rounded-full text-fs-11">{{ $tag->name }}</span>
                    @endforeach
                </div>
                <p class="text-white">{{ $project->description }}</p>
            </div>
        </div>
    </header>
    <section class="my-32">
        <div class="container">
            <div class="flex flex-col md:flex-row md:gap-8 items-start">
                <div class="raw-content md:w-1/2">
                    {!! $project->content !!}
                </div>
                <div class="swiper max-w-3xl">
                    <div class="swiper-wrapper">
                        @foreach ($project->media as $media)
                            <img src="{{ $media->imageMedium }}"
                                class="swiper-slide slide-{{ $media->id }} rounded-lg" alt="">
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>

        </div>
    </section>
    <div class="bg-gradient-primary py-32 mb-16 md:mb-32">
        <div class="container">
            <div class="flex flex-col md:flex-row gap-16">
                <div>
                    <h3 class="text-fs-2 text-white">Démarrer un projet</h3>
                    <p class="text-gray-400">Avec une équipe complète d'experts à votre disposition, <br> tout est
                        possible</p>
                </div>
                <div class="card flex-grow">
                    @include('partials.contact-form')
                </div>
            </div>
        </div>
    </div>

    <!-- Newsletter -->
    <div class="container pt-[60px] mb-4">
        <div class="card shadow-4xl">
            <h3 class="text-ssp text-fs-4 text-secondary font-700">Suscrivez au newsletter</h3>
            <p class="text-fs-8 mb-[15px]">Vous voulez que nous vous envoyions occasionnellement des e-mails
                avec des nouvelles de WelyaCreative ?</p>
            <form action="">
                <div class="mb-[10px]">
                    <div class="flex bg-gray-200 px-[10px] py-[1px] rounded-xl">
                        <input type="email" name="email" id="email" placeholder="exemple@domain.com"
                            class="self-center w-full bg-transparent text-fs-8 border-0 placeholder:text-fs-8 focus:outline-none focus:ring-0">
                        <button class="btn-primary">Souscrire</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
