/** @type {import('tailwindcss').Config} */

module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.{js,jsx,css}",
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
    ],
    theme: {
        container: {
            center: true,
            padding: {
                DEFAULT: '2rem',
                sm: '2rem',
                lg: '4rem',
                xl: '5rem',
                '2xl': '6rem',
            },
        },
        extend: {
            colors:
            {
                primary: '#8d4ee9',
                secondary: '#060047',
                pink: '#E90064',
                darkRed: '#B3005E',
                body: '#4f4b79',
                success: '#208835',
                gray: {
                    100: '#f7fafc',
                    200: '#edf2f7',
                    300: '#e2e8f0',
                    400: '#cbd5e0',
                    500: '#a0aec0',
                    600: '#718096',
                    700: '#4a5568',
                    800: '#2d3748',
                    900: '#1a202c'
                  },
            },
            fontSize: {
                "fs-1": "5.2rem",
                "fs-2": "3.8rem",
                "fs-3": "3.2rem",
                "fs-4": "2.5rem",
                "fs-5": "2.4rem",
                "fs-6": "2rem",
                "fs-7": "1.8rem",
                "fs-8": "1.6rem",
                "fs-9": "1.5rem",
                "fs-10": "1.4rem",
                "fs-11": "1.3rem",
                "fs-12": "1.2rem",
                "fs-13": "1rem",
            },
            fontFamily: {
                'sans': ['Poppins', 'sans-serif'],
                'ssp': ['Source Sans Pro', 'sans-serif'],

            },
            fontWeight: {
                '500': '500',
                '600': '600',
                '700': '700'
            },
            backgroundImage: {
                'hero-pattern': "linear-gradient(90deg,#060047 0, #762095 51%, #060048)",
                'gradient-primary': 'linear-gradient(90deg,#FF5F9E ,#060048);'
            },
            boxShadow: {
                'primary': '0 10px 25px -15px #FF5F9E',
              }
        },

    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
