import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import react from '@vitejs/plugin-react';
import mjml from 'vite-plugin-mjml'

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/css/admin.css', 'resources/js/app.js', 'resources/js/admin.jsx', 'resources/js/media.jsx'],
            refresh: true,
        }),
        mjml({
			input: 'resources/mail',
			output: 'resources/views/emails',
			extension: '.blade.php',
		}),
        react()
    ],
});
