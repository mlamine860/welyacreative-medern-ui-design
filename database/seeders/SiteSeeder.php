<?php

namespace Database\Seeders;

use App\Models\Site;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Site::truncate();
        Site::create([
            'name' => 'WelyaCreative',
            'phone' => '+224 622 06 44 49',
            'url' => 'https://welyacreative.com',
            'address' => 'address',
            'email' => 'contact@welyacreative.com',
            'logo_url' => 'logo',
            'image_url' => 'image',
            'tagline' => 'Libérez le potentiel de votre entreprise en ligne avec notre agence web innovante et créative',
            'description' => 'Nous sommes une agence de communication digitale à conakry ayant pour mission de vous accompagner sur vos projets digitaux.',
        ]);
    }
}
