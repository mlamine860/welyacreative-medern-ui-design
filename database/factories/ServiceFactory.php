<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Service>
 */
class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->sentence(3),
            'slug' => fake()->slug(),
            'icon_url' => fake()->imageUrl(250, 250),
            'image_url' => fake()->imageUrl(),
            'content' => fake()->sentence(),
            'description' => fake()->paragraph(),
            'user_id' => 1,
        ];
    }
}
