<?php

use Illuminate\Support\Facades\Route;



// Route::post('/login', [AuthController::class, 'login']);

// Route::middleware(['auth:sanctum', 'verified', 'admin'])->group(function () {
//     Route::resource('users', UserController::class);
//     Route::resource('services', ServiceController::class);
// });

use App\Http\Controllers\Api\NewsletterController;

Route::post('/subscribe', [NewsletterController::class, 'subscribe']);
Route::delete('/unsubscribe{email}', [NewsletterController::class, 'subscribe']);
