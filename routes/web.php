<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\QuestionController;

Route::get('/', HomeController::class)->name('welcome');
Route::get('/services', [ServiceController::class, 'index'])->name('services.list');
Route::get('/services/{service:slug}', [ServiceController::class, 'show'])->name('services.detail');

Route::get('/portfolio', [ProjectController::class, 'index'])->name('portfolio');
Route::get('/portfolio/tags/{tag:name}', [ProjectController::class, 'index'])->name('portfolio.tag');
Route::get('/portfolio/{project:name}', [ProjectController::class, 'show'])->name('projects.detail');

Route::get('/blog', [BlogController::class, 'index'])->name(('posts.list'));
Route::get('/blog/category/{slug}', [BlogController::class, 'index'])->name(('posts.category'));
Route::get('/blog/{post:slug}', [BlogController::class, 'detail'])->name(('posts.detail'));

Route::get('/contact', function () {
    return view('contact');
})->name(('pages.contact'));


Route::get('/about-us', function () {
    return view('about-us');
})->name(('about-us'));

Route::get('/faq', [QuestionController::class, 'index'])->name(('faq'));

Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name(('privacy-policy'));

Route::get('/blog', [BlogController::class, 'index'])->name(('blog'));

Route::post('/message', MessageController::class);




require __DIR__ . DIRECTORY_SEPARATOR . 'admin.php';
