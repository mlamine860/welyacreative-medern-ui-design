<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Api\PageController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\SiteController;
use App\Http\Controllers\Api\TaskController;
use App\Http\Controllers\Api\TeamController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\MediaController;
use App\Http\Controllers\Api\UploadController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\ServiceController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\QuestionController;
use App\Http\Controllers\Api\TestimonyController;
use App\Http\Controllers\Api\NewsletterController;

Route::get('/admin/login', function () {
    return view('admin');
});

Route::prefix('admin/')
    ->middleware(['auth', 'admin'])
    ->group(function () {
        Route::get('', AdminController::class)->name('admin');
        Route::resource('services', ServiceController::class);
        Route::resource('messages', MessageController::class);
        Route::resource('testimonies', TestimonyController::class);
        Route::resource('questions', QuestionController::class);
        Route::resource('categories', CategoryController::class);
        Route::resource('posts', PostController::class);
        Route::resource('projects', ProjectController::class);
        Route::resource('tags', TagController::class);

        Route::post('/pages/uploads', [PageController::class, 'uploadImage'])->name('pages.upload.image');
        Route::resource('pages', PageController::class);

        Route::get('users/profile', [UserController::class, 'profile'])->name('users.profile');
        Route::put('users/password/update', [UserController::class, 'passwordUpdate'])->name('users.password.update');
        Route::put('users/profile', [UserController::class, 'profileUpdate'])->name('users.profile.update');
        Route::put('users/profile/avatar', [UserController::class, 'uploadAvatar'])->name('users.upload.avatar');
        Route::resource('users', UserController::class);
        Route::resource('teams', TeamController::class);
        Route::resource('newsletters', NewsletterController::class)->only(['index', 'destroy']);

        Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index');
        Route::post('/tasks', [TaskController::class, 'process'])->name('tasks.process');



        Route::get('/site', [SiteController::class, 'edit'])->name('site.edit');
        Route::put('/site', [SiteController::class, 'update'])->name('site.update');
        Route::post('/logout', [AdminController::class, 'logout'])->name('admin.logout');

        Route::post('filepond-process', [UploadController::class, 'filepondProcess'])->name('filepond-process');
        Route::delete('filepond-revert', [UploadController::class, 'filepondRevert'])->name('filepond-revert');
        Route::delete('filepond-remove', [UploadController::class, 'filepondRemove'])->name('filepond-remove');
        Route::post('uploads', [UploadController::class, 'uploads'])->name('uploads.image');
        Route::get('media', [MediaController::class, 'index'])->name('media.index');
        Route::get('media/list', [MediaController::class, 'list'])->name('media.list');
        Route::delete('media/{media}', [MediaController::class, 'destroy'])->name('media.destroy');
    });
